module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', '5e368af34a7a9904a583fccdb6b4eea8'),
  },
});
