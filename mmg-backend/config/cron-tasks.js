const {getIssuedDateFromDatetime} = require('../utils/datetime')
const {getDetail} = require('../utils/axieTracker')

module.exports = {
    '0 0 */4 * * *': async ({strapi}) => {
        const handleUpdateOrInsertSlp = async (newData) => {
            const isExist = await strapi.query('api::slp.slp').findOne({
                where: {
                    issuedDate: newData.issuedDate,
                    accountId: newData.accountId
                }
            })
            if (isExist) {
                await strapi.query('api::slp.slp').update({
                    where: {
                        id: isExist.id
                    },
                    data: newData
                })
            } else {
                await strapi.query('api::slp.slp').create({
                    data: newData
                })
            }
        }
        const handleUpdateOrInsertLeaderboard = async (newData) => {
            if (!newData.leaderboard) return
            const isExist = await strapi.query('api::leaderboard.leaderboard').findOne({
                where: {
                    issuedDate: newData.issuedDate,
                    accountId: newData.accountId
                }
            })
            if (isExist) {
                await strapi.query('api::leaderboard.leaderboard').update({
                    where: {
                        id: isExist.id
                    },
                    data: newData
                })
            } else {
                await strapi.query('api::leaderboard.leaderboard').create({
                    data: newData
                })
            }
        }
        const transformData = (data, accountId, scholarId) => ({
            total: data.slp.total,
            average: data.slp.average,
            claimableTotal: data.slp.claimableTotal,
            lastClaimedItemAt: data.slp.lastClaimedItemAt * 1e3,
            rawTotal: data.slp.rawTotal,
            rawClaimableTotal: data.slp.rawClaimableTotal,
            todaySoFar: data.slp.todaySoFar,
            yesterdaySLP: data.slp.yesterdaySLP,

            winRate: data.leaderboard?.winRate,
            winTotal: data.leaderboard?.winTotal,
            drawTotal: data.leaderboard?.drawTotal,
            loseTotal: data.leaderboard?.loseTotal,
            elo: data.leaderboard?.elo,
            rank: data.leaderboard?.rank,

            name: data.leaderboard?.name,
            issuedDate: getIssuedDateFromDatetime(new Date()),
            accountId,
            scholarId,
        })
        const accounts = await strapi.query('api::account.account').findMany({
            where: {
                status: {
                    statusCode: 'active'
                }
            },
            populate: ['scholar', 'status']
        })
        await Promise.all(accounts.map(async acc => {
            const resp = await getDetail(acc.wallet)
            const transformed = transformData(resp, acc.id, acc.scholar?.id)
            await handleUpdateOrInsertSlp(transformed)
            await handleUpdateOrInsertLeaderboard(transformed)
        })).catch(e => {
            console.log(e)
        })
    }

}