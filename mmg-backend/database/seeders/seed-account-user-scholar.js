const csv = require('csv-parser')
const fs = require('fs')
const path = require('path')
const results = []

async function launch (strapi) {
  const role = await strapi.query('plugin::users-permissions.role').findOne({
    where: {
      type: 'scholar',
    }
  })
  const guilds = await strapi.query('api::guild.guild').findMany()
  fs.createReadStream(path.resolve(__dirname, 'gameguide-users.csv'))
  .pipe(csv())
  .on('data', (data) => results.push(data))
  .on('end', async () => {
    await Promise.all(results.map(async u => {
      const email = u.email?.toLowerCase()
      const userWithSameEmail = await strapi
      .query('plugin::users-permissions.user')
      .findOne({ where: { email } })
      if (userWithSameEmail) return

      const user = await strapi
      .query('plugin::users-permissions.user')
      .create({ data: {
          username: u.username.toLowerCase(),
          email,
          password: u.password,
          role: role.id,
          confirmed: true,
          provider: 'local',
        }, populate: ['role'] })
      const guild = guilds.find(_ => _.name === u.guild_leader)
      if (guild) {
        return strapi
        .service('api::scholar.scholar')
        .create({ data: {
            guild: guild.id,
            users_permissions_user: user.id
          }})
      }
    }))
    const accounts = []
    const game = await strapi.query('api::game.game').findOne()
    fs.createReadStream(path.resolve(__dirname, 'gameguide_accounts.csv'))
    .pipe(csv())
    .on('data', (data) => accounts.push(data))
    .on('end', async () => {
      accounts.map(async acc => {
        const email = acc['﻿email']
        const user = await strapi.query('plugin::users-permissions.user').findOne({
          where: {
            username: acc.map_scholar?.toLowerCase()
          }
        })
        if (!user) return
        const scholar = await strapi.query('api::scholar.scholar').findOne({
          where: {
            users_permissions_user: user.id
          }
        })
        strapi
        .service('api::account.account')
        .create({ data: {
            email,
            password: acc.password,
            wallet: acc.account_wallet,
            build: acc.build,
            game: game.id,
            scholar: scholar.id
          }})
      })
    })
  })
}
module.exports = launch