'use strict';
const _ = require('lodash')
const {getSlpDetailForLast60Days} = require("../../../../utils/axieTracker");
/**
 *  account controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

const removeUnusedAttribute = (ctx) => {
  const {scholar, guild, build, status, game, transfer_status} = ctx.request.body.data
  if(_.isEmpty(scholar)) delete ctx.request.body.data['scholar']
  if(_.isEmpty(guild)) delete ctx.request.body.data['guild']
  if(_.isEmpty(build)) delete ctx.request.body.data['build']
  if(_.isEmpty(status)) delete ctx.request.body.data['status']
  if(_.isEmpty(game)) delete ctx.request.body.data['game']
  if(_.isEmpty(transfer_status)) delete ctx.request.body.data['transfer_status']
  return ctx
}

module.exports = createCoreController('api::account.account', ({ strapi }) =>  ({
  async createAccountByLeader(ctx) {
    const isAllowed = await strapi.service('api::guild.guild')
      .find({
        filters: {
          id: ctx.request.body.data.guild?.id,
          guild_leader: {
            users_permissions_user: ctx.state.user.id
          }
        }
      }).then(_ => _.results.length > 0)
    if (!isAllowed) {
      return ctx.badRequest('Please provide correct guild!', {
        guild: 'Incorrect'
      })
    }
    return this.create(ctx);
  },
  async create(ctx) {
    return super.create(removeUnusedAttribute(ctx));
  },

  async update(ctx) {
    return super.update(removeUnusedAttribute(ctx));
  },

  async findByScholar(ctx) {
    const { results, pagination } = await strapi.service('api::account.account').findByScholar(ctx.query, ctx.state.user.id)
    const sanitizedResults = await this.sanitizeOutput(results, ctx)

    return this.transformResponse(sanitizedResults, { pagination })
  },

  async syncSlpByAccountID(ctx) {
    const { accountId } = ctx.params
    if(accountId > 0) {
      const account = await strapi.query('api::account.account').findOne(
          {
            where: {
              id: accountId
            },
            populate: ['scholar']
          }
      )
      try {
        const resp = await getSlpDetailForLast60Days(account.wallet)
        await strapi.service('api::account.account').handleUpdateAccountDailySlp(resp, account.id, account.scholar?.id || -1)
            .then(_ => console.log("Updated"))
        ctx.body = []
      } catch (e){
        ctx.badRequest(e)
      }
    }
  },

  async findOneByScholar (ctx) {
    const { id } = ctx.params;
    const { query } = ctx;

    const scholar = await strapi.query('api::scholar.scholar').findOne({
      where: {
        users_permissions_user: ctx.state.user.id
      }
    })

    const entity = await strapi.service('api::account.account').findOne(
      id,
      {
        ...query,
        populate: query.populate ? (query.populate !== '*' ? [
          ...query.populate,
          'guild'
        ] : '*') : ['guild'],
        scholar: scholar.id
      })
    const sanitizedEntity = await this.sanitizeOutput(entity, ctx)
    return this.transformResponse(sanitizedEntity)
  },
  async findByLeader(ctx) {
    const { results, pagination } = await strapi.service('api::account.account').findByLeader(ctx.query, ctx.state.user.id)
    const sanitizedResults = await this.sanitizeOutput(results, ctx)

    return this.transformResponse(sanitizedResults, { pagination })
  },
  async findOneByLeader (ctx) {
    const { id } = ctx.params;
    // Checking user role
    return await strapi.service('api::account.account').findOneWithAccountId(id)
  },
  async updateByLeader(ctx) {
    const { id } = ctx.params;

    const entity = await strapi.service('api::account.account').findOne(
        id,
        {
          populate: ['guild.guild_leader.users_permissions_user']
        })
    if (!entity || entity.guild.guild_leader.users_permissions_user.id !== ctx.state.user.id) {
      return ctx.notFound()
    }
    const newCtx = removeUnusedAttribute(ctx)
    return strapi.service('api::account.account').update(id, newCtx.request.body)
  },

  myRole(ctx) {
    const user = ctx.state.user

    if (!user) {
      return ctx.unauthorized()
    }

    ctx.body = user.role
  },
  async getRoleByType(ctx) {
    const { role } = ctx.params;

    const resRole = await strapi
        .query('plugin::users-permissions.role')
        .findOne({where: {type: role}});

    if (!resRole) {
      return ctx.badRequest('Not Found!')
    }
    ctx.body = resRole;
  },
  async getAllAccounts (ctx) {

    const res = await strapi.query('api::account.account').findMany(
        {
          select: ['email', 'wallet'],
          populate: {
            guild: true
          }
        }
    );
    ctx.body = res || [];
  },

  async syncTodaySlpByAccountId (ctx) {
    const { accountId } = ctx.params;
    return await strapi.service('api::account.account').syncTodaySlpByAccountId(ctx, accountId)
  }
}))
