module.exports = {
  routes: [
    {
      method: 'POST',
      path: '/accounts-by-leader',
      handler: 'account.createAccountByLeader',
    }, {
      method: 'GET',
      path: '/accounts-by-scholar',
      handler: 'account.findByScholar',
    }, {
      method: 'GET',
      path: '/accounts-by-scholar/:id',
      handler: 'account.findOneByScholar',
    }, {
      method: 'GET',
      path: '/accounts-by-leader',
      handler: 'account.findByLeader',
    }, {
      method: 'GET',
      path: '/accounts-by-leader/:id',
      handler: 'account.findOneByLeader',
    },{
      method: 'PUT',
      path: '/accounts-by-leader/:id',
      handler: 'account.updateByLeader',
    }, {
      method: 'GET',
      path: '/my-role',
      handler: 'account.myRole',
    }, {
      method: 'GET',
      path: '/role-by-type/:role',
      handler: 'account.getRoleByType',
    }, {
      method: 'GET',
      path: '/all-accounts',
      handler: 'account.getAllAccounts',
    }, {
      method: 'GET',
      path: '/sync-slp-by-accountID/:accountId',
      handler: 'account.syncSlpByAccountID',
    }, {
      method: 'POST',
      path: '/sync-today-slp-by-accountID/:accountId',
      handler: 'account.syncTodaySlpByAccountId',
    }
  ]
}
