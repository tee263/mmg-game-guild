'use strict';

const {getEndDate, getOneDayInMilliseconds, getOffsetTimezone, getIssuedDateFromDatetime} = require('../../../../utils/datetime')
const {getStartDate} = require('../../../../utils/datetime')
const {getDays} = require('../../../../utils/datetime')
const {transformSlpRecordData, getDetail} = require("../../../../utils/axieTracker");
const {createCoreService} = require('@strapi/strapi').factories

const transformData = (data, accountId, scholarId) => ({
    total: data.slp.total,
    average: data.slp.average,
    claimableTotal: data.slp.claimableTotal,
    lastClaimedItemAt: data.slp.lastClaimedItemAt * 1e3,
    rawTotal: data.slp.rawTotal,
    rawClaimableTotal: data.slp.rawClaimableTotal,
    todaySoFar: data.slp.todaySoFar,
    yesterdaySLP: data.slp.yesterdaySLP,

    winRate: data.leaderboard?.winRate,
    winTotal: data.leaderboard?.winTotal,
    drawTotal: data.leaderboard?.drawTotal,
    loseTotal: data.leaderboard?.loseTotal,
    elo: data.leaderboard?.elo,
    rank: data.leaderboard?.rank,

    name: data.leaderboard?.name,
    issuedDate: getIssuedDateFromDatetime(new Date()),
    accountId,
    scholarId,
})

module.exports = createCoreService('api::account.account', ({strapi}) => ({
        async find(...args) {
            const {results, pagination} = await super.find(...args)

            await Promise.all(results.map(async result => {
                const [slp, leaderboard] = await Promise.all([
                    strapi.service('api::slp.slp').find({
                        filters: {
                            accountId: result.id
                        },
                        sort: {
                            createdAt: 'desc'
                        }
                    }).then(_ => _.results[0]),
                    strapi.service('api::leaderboard.leaderboard').find({
                        filters: {
                            accountId: result.id
                        },
                        sort: {
                            createdAt: 'desc'
                        }
                    }).then(_ => _.results[0])
                ])
                result.slp = slp
                result.leaderboard = leaderboard
            }))

            return {results, pagination}
        },
        async findByScholar(_query, scholarId) {
            const query = {..._query}
            const scholars = await strapi.query('api::scholar.scholar').findMany({
                where: {
                    users_permissions_user: scholarId
                }
            })
            if (!query.filters)
                query.filters = {}
            query.filters.scholar = scholars.map(_ => _.id)
            if (query.populate !== '*')
                if (!query.populate)
                    query.populate = []
            query.populate.push('guild')

            return super.find(query)
        },
        async findByLeader(query, leaderId, guildIds = null) {
            const guildFilters = guildIds ? {filters: {id: {$in: guildIds}}} : {}
            const guilds = await strapi.query('api::guild-leader.guild-leader')
                .findOne({
                    ...guildFilters,
                    where: {
                        users_permissions_user: leaderId
                    },
                    populate: ['guilds']
                })
                .then(_ => _?.guilds)

            const queryGuild = {
                guild: guilds ? guilds.map(_ => _.id) : [-1]
            }
            const _query = {
                ...query,
                filters: {
                    ...query.filters,
                    ...queryGuild
                },
                populate: query.populate ? (query.populate !== '*' ? [
                    ...query.populate,
                    'guild'
                ] : '*') : ['guild']
            }

            return super.find(_query)
        },
        async getSlpsInDayRange(ctx) {
            const {id} = ctx.params
            const {startTime, endTime} = ctx.query
            if (!startTime || !endTime) return

            const days = getDays(startTime, endTime)
            return Promise.all(days.map(d => strapi.query('api::slp.slp').findOne({
                select: 'todaySoFar',
                where: {
                    createdAt: {
                        $gte: getStartDate(d),
                        $lte: getEndDate(d)
                    },
                    accountId: id
                },
                orderBy: {createdAt: 'desc'}
            }).then(_ => _?.todaySoFar)))
        },

        async findOneWithAccountId(accountId) {
            const entity = await strapi.query('api::account.account').findOne({
                where: {id: accountId},
                populate: ['build.project', 'scholar', 'status', 'guild.group']
            })
            return {data: entity, meta: {}}
        },

        async syncTodaySlpByAccountId(ctx ,accountId) {
            try {
                const accountData = await strapi.query('api::account.account').findOne({
                    where: {id: accountId},
                    populate: ['build.project', 'scholar', 'status', 'guild.group']
                })

                if(accountData) {
                    const resp = await getDetail(accountData?.wallet)
                    const transformed = transformData(resp, accountData.id, accountData?.scholar?.id)

                    const isExist = await strapi.query('api::slp.slp').findOne({
                        where: {
                            issuedDate: transformed.issuedDate,
                            accountId: transformed.accountId
                        }
                    })
                    if (isExist) {
                        await strapi.query('api::slp.slp').update({
                            where: {
                                id: isExist.id
                            },
                            data: transformed
                        })
                    } else {
                        await strapi.query('api::slp.slp').create({
                            data: transformed
                        })
                    }
                    return isExist;
                } else {
                    return ctx.badRequest("Entity Not Found!", { })
                }
            } catch (e) {
                return ctx.badRequest("Entity Not Found!", { })
            }
        },

        async handleUpdateAccountDailySlp(dailyData, accountId, scholarId) {

            if ((dailyData.daily).length > 0) {
                const dataArr = dailyData.daily
                const newDataMap = new Map(
                    dataArr.map(object => {
                        return [(new Date(object.date) - (getOffsetTimezone() * 60 * 60 * 1000)), object.slp];
                    })
                )
                const belowFence = new Date(dataArr[0].date).getTime() - (getOffsetTimezone() * 60 * 60 * 1000)
                const upperFence = new Date(dataArr[dataArr.length - 1].date).getTime() - (getOffsetTimezone() * 60 * 60 * 1000)
                const sourceFromDb = await strapi.db.query('api::slp.slp').findMany({
                    where: {
                        accountId: accountId,
                        issuedDate: {
                            $gte: belowFence,
                            $lte: upperFence,
                        }
                    }
                });
                const targetSource = []
                const alreadyExistKey = []
                if (sourceFromDb) {
                    sourceFromDb.map(item => {
                        const source = new Date(item.issuedDate)
                        const newItemData = newDataMap.get(source.getTime())
                        const preDateData = newDataMap.get(source.getTime() - getOneDayInMilliseconds())
                        const dataToUpdate = item
                        if (newItemData) {
                            dataToUpdate.total = newItemData.total
                            dataToUpdate.rawTotal = newItemData.rawTotal
                            dataToUpdate.claimableTotal = newItemData.claimableTotal
                            dataToUpdate.rawClaimableTotal = newItemData.rawClaimableTotal
                            if (preDateData) {
                                dataToUpdate.todaySoFar = newItemData.rawTotal - preDateData.rawTotal
                            }
                            dataToUpdate.updatedAt = new Date()
                            targetSource.push(dataToUpdate)
                        }
                        alreadyExistKey.push(source.getTime())
                    })
                }
                for (const value of targetSource) {
                    await strapi.query('api::slp.slp').update({
                        where: {
                            id: value.id
                        },
                        data: value
                    }).then(value => console.log(value))
                }
                for (const item of newDataMap) {
                    if(!(alreadyExistKey.includes(item[0]))) {
                        const value = item[1]
                        const preDateData = newDataMap.get(item[0] - getOneDayInMilliseconds())
                        const pre2DaysData = newDataMap.get(item[0] - getOneDayInMilliseconds() - getOneDayInMilliseconds())
                        if(preDateData && pre2DaysData) {
                            value.todaySoFar = value.rawTotal - preDateData.rawTotal
                            value.yesterdaySLP = preDateData.rawTotal - pre2DaysData.rawTotal
                        }
                        const newData = transformSlpRecordData(value, accountId, scholarId, item[0])
                        if(scholarId === -1) delete newData['scholarId']
                        const isExist = await strapi.query('api::slp.slp').findOne({
                            where: {
                                issuedDate: newData.issuedDate,
                                accountId: newData.accountId
                            }
                        })
                        if (!isExist) {
                            await strapi.query('api::slp.slp').create({
                                data: newData
                            })
                        }
                    }
                }
            }
        }
    }
))
