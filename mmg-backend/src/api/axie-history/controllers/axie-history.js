'use strict';

/**
 *  axie-history controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::axie-history.axie-history');
