'use strict';

/**
 * axie-history router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::axie-history.axie-history');
