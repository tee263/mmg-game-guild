const { getService } = require('@strapi/plugin-users-permissions/server/utils')

module.exports = {
  async beforeDelete ({ params }) {
    const detail = await strapi.service('api::guild-leader.guild-leader').findOne(params.where.id, {
      populate: ['users_permissions_user']
    })
    await getService('user').remove({
      id: detail.users_permissions_user.id
    });
  },
}