'use strict';

const { validateCreateUserBody } = require(
  '@strapi/plugin-users-permissions/server/controllers/validation/user')
const { getService } = require('@strapi/plugin-users-permissions/server/utils')
const utils = require('@strapi/utils')
const bcrypt = require('bcryptjs');
const { ApplicationError } = utils.errors
/**
 *  guild-leader controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::guild-leader.guild-leader', ({ strapi }) =>  ({
  async create(ctx) {
    const { email } = ctx.request.body.data;
    const username = email
    ctx.request.body.data.username = username
    await validateCreateUserBody(ctx.request.body.data);

    const userWithSameUsername = await strapi
      .query('plugin::users-permissions.user')
      .findOne({ where: { username: username.toLowerCase() } });
    if (userWithSameUsername) {
      if (!email) throw new ApplicationError('Username already taken');
    }

    const userWithSameEmail = await strapi
    .query('plugin::users-permissions.user')
    .findOne({ where: { email: email.toLowerCase() } })
    if (userWithSameEmail) {
      throw new ApplicationError('Email already taken')
    }

    const user = {
      ...ctx.request.body.data,
      email: email.toLowerCase(),
      confirmed: true,
      provider: 'local',
    };

    const defaultRole = await strapi
      .query('plugin::users-permissions.role')
      .findOne({ where: { type: 'leader' } })
    user.role = defaultRole.id;

    const data = await getService('user').add(user)

    ctx.request.body.data.users_permissions_user = {
      id: data.id
    }
    return strapi
      .service('api::guild-leader.guild-leader')
      .create({ data: ctx.request.body.data })
  },
  async update(ctx) {
    const { password } = ctx.request.body.data
    if (password) {
      const guildLeader = await strapi.query('api::guild-leader.guild-leader')
        .findOne({ 
          where: { id: ctx.params.id },
          populate: ['users_permissions_user']
        });
      if (guildLeader) {
        guildLeader.users_permissions_user.password = await this.beforeUpdate(password)
        await strapi.query('plugin::users-permissions.user').update({
          where: {
            id: guildLeader.users_permissions_user.id
          },
          data: guildLeader.users_permissions_user
        })
      }
    }
    const response = await super.update(ctx);
    return response;
  },
  async beforeUpdate(password) {
    return bcrypt.hash(password,10);
  },
}));
