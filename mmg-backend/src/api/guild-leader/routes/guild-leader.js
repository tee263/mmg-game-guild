'use strict';

/**
 * guild-leader router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::guild-leader.guild-leader');
