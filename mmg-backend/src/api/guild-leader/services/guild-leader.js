'use strict';

/**
 * guild-leader service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::guild-leader.guild-leader');
