'use strict';

/**
 *  guild controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::guild.guild', ({ strapi }) =>  ({
  async findByLeader(ctx) {
    const { results, pagination } = await strapi.service('api::guild.guild').findByLeader(ctx.query, ctx.state.user.id)
    const sanitizedResults = await this.sanitizeOutput(results, ctx)

    return this.transformResponse(sanitizedResults, { pagination })
  },
  async findOneByLeader (ctx) {
    const { id } = ctx.params;

    const entity = await strapi.service('api::guild.guild').findOne(
      id,
      {
        populate: ['guild_leader.users_permissions_user']
      })
    if (!entity || entity.guild_leader.users_permissions_user.id !== ctx.state.user.id) {
      return ctx.notFound()
    }
    return super.findOne(ctx)
  }
}))
