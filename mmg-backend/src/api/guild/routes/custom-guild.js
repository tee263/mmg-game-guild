module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/guilds-by-leader',
      handler: 'guild.findByLeader',
    }, {
      method: 'GET',
      path: '/guilds-by-leader/:id',
      handler: 'guild.findOneByLeader',
    },
  ]
}
