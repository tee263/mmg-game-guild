'use strict';

/**
 * guild service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::guild.guild', ({strapi}) => ({
  async findByLeader(query, leaderId) {
    const guilds = await strapi.query('api::guild-leader.guild-leader')
    .findOne({
      where: {
        users_permissions_user: leaderId
      },
      populate: ['guilds']
    })
    .then(_ => _?.guilds)

    const queryGuild = {
      id: guilds ? guilds.map(_ => _.id) : [-1]
    }
    const _query = {
      ...query,
      filters: {
        ...query.filters,
        ...queryGuild
      }
    }

    return super.find(_query)
  }
}));
