'use strict';

/**
 *  interview-question controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::interview-question.interview-question');
