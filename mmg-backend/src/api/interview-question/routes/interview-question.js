'use strict';

/**
 * interview-question router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::interview-question.interview-question');
