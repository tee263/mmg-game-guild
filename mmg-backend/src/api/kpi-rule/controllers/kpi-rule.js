'use strict';

/**
 *  kpi-rule controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::kpi-rule.kpi-rule');
