'use strict';

/**
 * kpi-rule router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::kpi-rule.kpi-rule');
