'use strict';

/**
 * kpi-rule service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::kpi-rule.kpi-rule');
