'use strict';

const axios = require("axios");
const Web3 = require("web3");
/**
 *  payment-transaction controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

const axiosInstance = axios.create({
    baseURL: 'https://explorer-api.roninchain.com/tx',
    timeout: 10000,
    headers: {
        'origin': "https://explorer-api.roninchain.com/tx"
    }
})

axiosInstance.interceptors.response.use(function (response) {
    return response.data
})

module.exports = createCoreController('api::payment-transaction.payment-transaction', ({strapi}) => ({
    async getTransactionInfoByTxHash(ctx) {
        const web3 = new Web3()

        const {txCode} = ctx.request.body
        const result = await axiosInstance.get(`/${txCode}`)
        const logs = result.logs || []
        const convertedLogs = []
        logs.map(value => {
            convertedLogs.push({
                _from: "ronin:" + (value?.topics[1]).slice(26),
                _to: "ronin:" + (value?.topics[2]).slice(26),
                _value: web3.utils.toBN("0x" + value?.data || "").toNumber(),
            })
        })
        result.enCryptLogs = convertedLogs

        ctx.body = result;
    },
    async getAllpaymentsTransaction (ctx) {

        const res = await strapi.query('api::payment-transaction.payment-transaction').findMany(
            {
                select: ['value', 'from', 'to']
            }
        );
        ctx.body = res || [];
    },
    async saveTrackingTransaction (ctx) {
        const {txHash} = ctx.request.body.data
        const txHashPresent = await strapi.query('api::tracked-transaction.tracked-transaction').findOne({
            select: ['txHash'],
            where: { txHash: txHash },
        })
        if(txHashPresent) {
            return ctx.badRequest(null, 'Transaction already exists');
        } else {
            return await strapi.service('api::payment-transaction.payment-transaction').saveTrackingTransaction(ctx)
        }
    }
}));
