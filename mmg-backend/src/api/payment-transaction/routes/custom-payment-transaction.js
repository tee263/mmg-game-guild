module.exports = {
  routes: [
    {
      method: 'POST',
      path: '/get-info-by-txHash',
      handler: 'payment-transaction.getTransactionInfoByTxHash',
    },
    {
      method: 'POST',
      path: '/save-transaction',
      handler: 'payment-transaction.saveTrackingTransaction',
    },
    {
      method: 'GET',
      path: '/all-payments-transaction',
      handler: 'payment-transaction.getAllpaymentsTransaction',
    }
  ]
}
