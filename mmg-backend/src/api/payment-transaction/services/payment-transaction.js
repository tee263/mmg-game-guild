'use strict';

/**
 * payment-transaction service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::payment-transaction.payment-transaction', ({ strapi }) =>  ({
    async saveTrackingTransaction(ctx) {
        const {txHash, paymentTransaction} = ctx.request.body.data
            if(txHash || paymentTransaction?.length > 0) {
                const txHashPresent = await strapi.service('api::tracked-transaction.tracked-transaction').create({ data: {txHash: txHash} });
                if(txHashPresent) {
                    const dataToSave = []
                    paymentTransaction.map(item => dataToSave.push({...item, tracked_transaction: txHashPresent?.id}))
                    dataToSave.map(i => strapi.service('api::payment-transaction.payment-transaction').create({data: i}))
                    return txHashPresent;
                }
            } else {
                return ctx.badRequest(null, 'Invalid Request');
            }
    }
}));
