const Payout = require('../../../../services/Payout')

module.exports = {
  async beforeCreate (event) {
    const { params } = event
    const payout = new Payout({
      accountId: params.data.account,
      periodStart: params.data.periodStart,
      periodEnd: params.data.periodEnd
    })
    const payoutDetail = await payout.launch()
    params.data.detail = payoutDetail
    params.data.amount = payoutDetail.total
  },
  async afterCreate(event) {
    const { result } = event
    const detail = await strapi.service('api::payout-account.payout-account').findOne(result.id, {
      populate: ['payout.payout_accounts.account']
    })
    const currentAccountIds = detail.payout.payout_accounts.map(_ => _.account.id)
    strapi.service('api::payout.payout').update(detail.payout.id, {
      data: {
        accounts: currentAccountIds
      }
    })
  },
  async beforeUpdate(event) {
    const { params } = event
    const detail = await strapi.service('api::payout-account.payout-account').findOne(params.where.id, {
      populate: ['account']
    })
    const payout = new Payout({
      accountId: detail.account.id,
      periodStart: params.data.periodStart,
      periodEnd: params.data.periodEnd
    })
    const payoutDetail = await payout.launch()
    params.data.detail = payoutDetail
    params.data.amount = payoutDetail?.total || 0
  }
}