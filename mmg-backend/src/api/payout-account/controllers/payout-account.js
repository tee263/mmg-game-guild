'use strict';

/**
 *  payout-account controller
 */

const {createCoreController} = require('@strapi/strapi').factories;

module.exports = createCoreController('api::payout-account.payout-account', ({strapi}) => ({
    async findByLeader(ctx) {
        const {results, pagination} = await strapi.service('api::payout-account.payout-account').find(
            ctx.request.query
        )
        return this.transformResponse(results, {pagination})
    },
    async findMyPayout(ctx) {
        const {results: accounts} = await strapi
            .service('api::account.account')
            .findByScholar({}, ctx.state.user.id)
        if (!ctx.request.query.filters)
            ctx.request.query.filters = {}
        if (!ctx.request.query.filters.account)
            ctx.request.query.filters.account = {}
        ctx.request.query.filters.account.id = accounts.map(_ => _.id)
        const {results, pagination} = await strapi
            .service('api::payout-account.payout-account')
            .find(ctx.request.query)
        return this.transformResponse(results, {pagination})
    },
    async create(ctx) {
        const {payout} = ctx.request.body.data
        await strapi.service('api::payout.payout').safeModify(ctx, payout)
        ctx.request.body.data.state = 'waitForScholar'
        const response = await strapi.service('api::payout-account.payout-account')
            .create({
                data: ctx.request.body.data
            })

        const sanitizedEntity = await this.sanitizeOutput(response, ctx)
        return this.transformResponse(sanitizedEntity)
    },
    async update(ctx) {
        ctx.request.query = {
            populate: ['payout']
        }
        const detail = await super.findOne(ctx)
        const {payout} = detail.data.attributes
        await strapi.service('api::payout.payout').safeModify(ctx, payout.data.id)

        delete ctx.request.body.data.state
        return super.update(ctx)
    },
    async delete(ctx) {
        const detail = await strapi.query('api::payout-account.payout-account')
            .findOne({
                where: {
                    id: ctx.request.params.id
                },
                populate: ['payout.accounts', 'account']
            })
        const {payout} = detail
        if (payout.state === 'confirmed') {
            return ctx.badRequest('Payout Account was confirmed', {
                state: 'Payout Account was confirmed'
            })
        }
        await strapi.service('api::payout.payout').safeModify(ctx, payout.id)
        /**
         * Have to wait validate payout before remove payout-account
         */

        /**
         * Update account list of the payout group
         * when user remove payout-account
         * TODO move this function to lifecycle after/before delete
         */
        const payoutAccounts = await strapi.query('api::payout-account.payout-account')
            .findMany({
                where: {
                    payout: payout.id
                },
                populate: ['account']
            })
        await strapi.query('api::payout.payout')
            .update({
                where: {
                    id: payout.id
                },
                data: {
                    accounts: payoutAccounts.map(_ => _.account.id).filter(id => id !== detail.account.id)
                }
            })

        return super.delete(ctx);
    },
    async leaderReconfirm(ctx) {
        ctx.request.body.data = {
            state: 'waitForScholar'
        }
        return super.update(ctx)
    },
    async scholarConfirm(ctx) {
        const {result, scholarNote} = ctx.request.body.data
        const {data: {attributes: {state}}} = await super.findOne(ctx)
        if (state !== 'waitForScholar') {
            return ctx.badRequest('Payout Account can\'t modify right now!')
        }
        let body = {}
        if (result) {
            body = {
                state: 'waitForManager',
                scholarNote: ''
            }
            // TODO disable edit slp amount in that range
        } else {
            body = {
                state: 'rejectedByScholar',
                scholarNote
            }
        }
        ctx.request.body.data = body
        return super.update(ctx)
    },
    async managerConfirm(ctx) {
        const {result} = ctx.request.body.data
        const {data: {attributes: {state}}} = await super.findOne(ctx)
        if (state !== 'waitForManager') {
            return ctx.badRequest('Payout Account can\'t modify right now!')
        }
        let body = {}
        if (result) {
            body = {
                state: 'confirmed'
            }
        } else {
            body = {
                state: 'rejectedByManager'
            }
        }
        ctx.request.body.data = body
        return super.update(ctx)
    },
    async getPaymentsByPayoutId(ctx) {
        const {payoutId} = ctx.request.params
        const results = await strapi.service('api::payout-account.payout-account').getPaymentsByPayoutId(payoutId)

        return {
            data: results,
            meta: {
                pagination: {
                    pageSize: 1000,
                    pageCount: (((results.length / 1000) % 2) > 0 ? Math.floor(results.length / 1000) + 1 : results.length / 1000),
                    total: results.length
                }
            }
        }
    },
    async getPaymentsByLeaderId(ctx) {
        const {leaderId} = ctx.request.params
        const validAccounts = await strapi.service('api::account.account').findByLeader({}, leaderId)

        const _query = {
            ...ctx.query,
            filters: {
                ...ctx.query.filters,
                account: {id: {$in: validAccounts.results.map(_ => _.id)}}
            },
            populate: ['payout', 'account.build']
        }

        let {results, pagination} = await strapi.service('api::payout-account.payout-account').find(_query)

        return this.transformResponse(results, {pagination})
    },

    async getAllPayoutsRecord (ctx) {
        await strapi.service('api::payout-account.payout-account').getAllPayoutsRecord(ctx)
    },

}));
