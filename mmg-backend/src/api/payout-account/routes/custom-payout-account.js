module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/payout-accounts-by-leader',
      handler: 'payout-account.findByLeader',
    }, {
      method: 'PUT',
      path: '/payout-accounts-by-leader/:id',
      handler: 'payout-account.update',
    }, {
      method: 'GET',
      path: '/payout-accounts-by-leader/:id',
      handler: 'payout-account.findOne',
    }, {
      method: 'GET',
      path: '/payout-accounts/my',
      handler: 'payout-account.findMyPayout',
    }, {
      method: 'GET',
      path: '/payout-accounts/:id',
      handler: 'payout-account.findOne',
    }, {
      method: 'POST',
      path: '/payout-accounts/:id/scholar-confirm',
      handler: 'payout-account.scholarConfirm',
    }, {
      method: 'POST',
      path: '/payout-accounts/:id/leader-reconfirm',
      handler: 'payout-account.leaderReconfirm',
    }, {
      method: 'POST',
      path: '/payout-accounts/:id/manager-confirm',
      handler: 'payout-account.managerConfirm',
    }, {
      method: 'GET',
      path: '/payout-accounts-by-payout-id/:payoutId',
      handler: 'payout-account.getPaymentsByPayoutId',
    }, {
      method: 'GET',
      path: '/payments-by-leader/:leaderId',
      handler: 'payout-account.getPaymentsByLeaderId',
    }, {
      method: 'GET',
      path: '/all-payout-accounts',
      handler: 'payout-account.getAllPayoutsRecord',
    }
  ]
}
