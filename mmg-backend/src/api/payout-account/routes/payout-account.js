'use strict';

/**
 * payout-account router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::payout-account.payout-account');
