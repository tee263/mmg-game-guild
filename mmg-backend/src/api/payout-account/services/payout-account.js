'use strict';

/**
 * payout-account service.
 */

const {createCoreService} = require('@strapi/strapi').factories;

module.exports = createCoreService('api::payout-account.payout-account', ({strapi}) => ({
    async getPaymentsByPayoutId(payoutId) {

        const payout = await strapi.query('api::payout-account.payout-account').findMany({
            where: {
                payout: payoutId
            },
            populate: ['payout', 'account.build']
        })

        return payout || []
    },
    async getAllPayoutsRecord(ctx) {

        const res = await strapi.query('api::payout-account.payout-account').findMany({
            select: '*',
            populate: {
                payout: true, account: {
                    populate: {
                        build: true,
                        scholar: {
                            populate: {
                                status: true
                            }
                        }
                    }
                }
            },
        });
        ctx.body = res || [];
    },
}));
