'use strict';

/**
 *  payout-confirm controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::payout-confirm.payout-confirm');
