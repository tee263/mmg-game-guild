'use strict';

/**
 * payout-confirm router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::payout-confirm.payout-confirm');
