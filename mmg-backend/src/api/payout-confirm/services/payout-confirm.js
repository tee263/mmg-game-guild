'use strict';

/**
 * payout-confirm service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::payout-confirm.payout-confirm');
