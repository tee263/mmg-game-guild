'use strict';

/**
 *  payout controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::payout.payout', ({ strapi }) =>  ({

  async create(ctx) {
    ctx.request.body.data.createdUser = {
      id: ctx.state.user.id
    }
    const response = await super.create(ctx);
    const result = response.data.attributes
    const selectAllAccounts = ctx.request.body.data.selectAllAccounts;
    let accountIds = ctx.request.body.data.accounts
    if (selectAllAccounts) {
      //const accounts = await strapi.entityService.findMany('api::account.account', {})
      const accounts = await strapi.query('api::account.account').findMany(
          {
            populate: {
              guild: true,
              status: true
            }
          }
      );

      accountIds = accounts.filter(item => item?.status?.statusCode === "active").map(account => account.id)
    }
    await Promise.all(accountIds.map(async accountId => (
      strapi.service('api::payout-account.payout-account').create({
        data: {
          periodStart: result.periodStart,
          periodEnd: result.periodEnd,
          payout: response.data.id,
          account: accountId
        }
      })
    )))
    return response;
  },

  async update(ctx) {
    await strapi.service('api::payout.payout').safeModify(ctx, ctx.params.id)
    delete ctx.request.body.data.isPublished

    const response = await super.update(ctx);

    return response;
  },
  async confirm(ctx) {
    await strapi.service('api::payout.payout').safeModify(ctx, ctx.params.id)

    ctx.request.body.data = {
      isPublished: true
    }
    const [response] = await Promise.all([
      super.update(ctx),
      strapi.service('api::payout-confirm.payout-confirm').create({
        data: {
          payout: ctx.params.id
        }
      })
    ])

    return response;
  },
  async delete(ctx) {
    await strapi.service('api::payout.payout').safeModify(ctx, ctx.params.id)

    const response = await super.delete(ctx);

    return response;
  }
}));
