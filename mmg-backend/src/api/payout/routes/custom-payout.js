module.exports = {
  routes: [
    {
      method: 'POST',
      path: '/payouts/:id/confirm',
      handler: 'payout.confirm',
    }, {
      method: 'GET',
      path: '/payouts-by-leader/:id',
      handler: 'payout.findOne'
    }, {
      method: 'PUT',
      path: '/payouts-by-leader/:id',
      handler: 'payout.update'
    }
  ]
}
