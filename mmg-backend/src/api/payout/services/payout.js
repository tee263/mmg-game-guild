'use strict';

/**
 * payout service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::payout.payout', ({ strapi }) =>  ({
  async safeModify(ctx, payoutId) {
    const detail = await strapi.service('api::payout.payout').findOne(payoutId)
    if (detail.isPublished) {
      ctx.badRequest('Payout was published')
      throw Error()
    }
  }
}));
