'use strict';

const { validateCreateUserBody } = require(
  '@strapi/plugin-users-permissions/server/controllers/validation/user')
const utils = require('@strapi/utils')
const { getService } = require('@strapi/plugin-users-permissions/server/utils')
const { ApplicationError } = utils.errors
/**
 *  scholar controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::scholar.scholar', ({ strapi }) =>  ({
  async create(ctx) {
    const advanced = await strapi
    .store({ type: 'plugin', name: 'users-permissions', key: 'advanced' })
    .get();

    await validateCreateUserBody(ctx.request.body.data);

    const { email, username } = ctx.request.body.data;

    const userWithSameUsername = await strapi
      .query('plugin::users-permissions.user')
      .findOne({ where: { username: username.toLowerCase() } });
    if (userWithSameUsername) {
      if (!email) throw new ApplicationError('Username already taken');
    }

    const userWithSameEmail = await strapi
      .query('plugin::users-permissions.user')
      .findOne({ where: { email: email.toLowerCase() } });
    if (userWithSameEmail) {
      throw new ApplicationError('Email already taken');
    }

    const user = {
      ...ctx.request.body.data,
      email: email.toLowerCase(),
      confirmed: true,
      provider: 'local',
    };

    const data = await getService('user').add(user);

    ctx.request.body.data.users_permissions_user = data.id
    return strapi
      .service('api::scholar.scholar')
      .create({ data: ctx.request.body.data })
  },
  async findByLeader(ctx) {
    const { results, pagination } = await strapi.service('api::scholar.scholar').findByLeader(ctx.query, ctx.state.user.id)

    return this.transformResponse(results, { pagination })
  },
  async findOneByLeader (ctx) {
    const { params, query, state: { user }} = ctx
    const entity = await strapi.service('api::scholar.scholar').findOneByLeader(params.id, query, user.id)
    if (!entity) return ctx.notFound()

    return this.transformResponse(entity)
  },

  async updateIncludeSysUser (ctx) {
    // const { users_permissions_user } = ctx.request.body.data
    // if (users_permissions_user) {
    //   delete ctx.request.body.data['users_permissions_user']
    //   await strapi.service('api::scholar.scholar').updateSysUser(users_permissions_user.id, users_permissions_user)
    // }
    const entity = await strapi.service('api::scholar.scholar').update(ctx.request.params?.id, ctx.request.body)
    return {entity}
  },

  async deActiveScholar (ctx) {
    const { id } = ctx.params
    const entity = await strapi.service('api::scholar.scholar').findOne(id, {
      populate: { users_permissions_user: true, status: true }
    })
    if (!entity) return ctx.notFound()
    if(entity.users_permissions_user){
      entity.users_permissions_user.confirmed = false
      await strapi.service('api::scholar.scholar').updateSysUser(entity.users_permissions_user.id, entity.users_permissions_user);
    }
    const status = await strapi.query('api::status.status').findOne({
      where: {
        statusCode: 'in-active'
      }
    });
    entity.status = {id: status?.id}
    return await strapi.service('api::scholar.scholar').updateScholarStatus(id, entity)
  },

  async getAllScholars (ctx) {

    const res = await strapi.query('plugin::users-permissions.user').findMany(
        {
          select: ['username', 'email'],
          populate: {
            scholar: true,
            role: true
          }
        }
    );

    ctx.body = res || [];
  }
}));
