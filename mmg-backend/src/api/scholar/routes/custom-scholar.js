module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/scholars-by-leader',
      handler: 'scholar.findByLeader',
    }, {
      method: 'GET',
      path: '/scholars-by-leader/:id',
      handler: 'scholar.findOneByLeader',
    }, {
      method: 'GET',
      path: '/all-scholars',
      handler: 'scholar.getAllScholars',
    }, {
      method: 'PUT',
      path: '/scholars-by-leader/:id',
      handler: 'scholar.updateIncludeSysUser',
    }, {
      method: 'PUT',
      path: '/scholars/:id',
      handler: 'scholar.updateIncludeSysUser',
    }, {
      method: 'DELETE',
      path: '/scholars-by-leader/:id',
      handler: 'scholar.deActiveScholar',
    }, {
      method: 'DELETE',
      path: '/scholars/:id',
      handler: 'scholar.deActiveScholar',
    }
  ]
}
