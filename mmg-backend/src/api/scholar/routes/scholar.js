'use strict';

/**
 * scholar router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::scholar.scholar');
