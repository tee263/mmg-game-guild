'use strict';

const {createCoreService} = require('@strapi/strapi').factories

module.exports = createCoreService('api::scholar.scholar', ({strapi}) => ({
    async find(...args) {
        const {results, pagination} = await super.find(...args)

        await Promise.all(results.map(async result => {
            const [slp, leaderboard] = await Promise.all([
                strapi.service('api::slp.slp').find({
                    filters: {
                        scholarId: result.id
                    },
                    sort: {
                        createdAt: 'desc'
                    }
                }).then(_ => _.results[0]),
                strapi.service('api::leaderboard.leaderboard').find({
                    filters: {
                        accountId: result.id
                    },
                    sort: {
                        createdAt: 'desc'
                    }
                }).then(_ => _.results[0])
            ])
            result.slp = slp
            result.leaderboard = leaderboard
        }))

        return {results, pagination}
    },
    async findByLeader(query = {}, leaderId, guildIds = null) {

        const guildFilters = guildIds ? {filters: {id: {$in: guildIds}}} : {}
        const guilds = await strapi.query('api::guild-leader.guild-leader')
            .findOne({
                ...guildFilters,
                where: {
                    users_permissions_user: leaderId
                },
                populate: ['guilds']
            })
            .then(_ => _?.guilds)

        const queryGuild = {
            guild: guilds ? guilds.map(_ => _.id) : [-1]
        }

        const _query = {
            ...query,
            filters: {
                ...query.filters,
                ...queryGuild
            },
            populate: query.populate ? (query.populate !== '*' ? [
                ...query.populate,
                'guild'
            ] : '*') : ['guild']
        }

        return strapi.service('api::scholar.scholar').find(_query)
    },
    async findOneByLeader(id, params, leaderId) {
        // const entity = await strapi.service('api::scholar.scholar').findOne(
        //   id,
        //   {
        //     populate: ['guild.guild_leader.users_permissions_user']
        //   })
        // if (!entity || entity.guild.guild_leader.users_permissions_user.id !== leaderId) {
        //   return false
        // }
        return super.findOne(id, params)
    },

    async updateSysUser(id, data) {
        return await strapi.query('plugin::users-permissions.user').update({
            where: {
                id: id
            },
            data: data
        })
    },
    async updateScholarStatus(id, data) {
        return await strapi.query('api::scholar.scholar').update({
            where: {
                id: id
            },
            data: data
        })
    }
}))
