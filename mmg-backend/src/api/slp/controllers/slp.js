'use strict';

const { pick } = require('lodash');
const { calcPagination } = require('../../../../utils/calcPagination');
const Payout = require('../../../services/Payout');

/**
 *  slp controller
 */

const {createCoreController} = require('@strapi/strapi').factories;

module.exports = createCoreController('api::slp.slp', ({strapi}) => ({
    async findByLeader(ctx) {
        const userId = ctx.state.user.id
        if (!ctx.query?.filters?.periodStart?.$eq || !ctx.query?.filters?.periodEnd?.$eq) {
            return { data: [], meta: { pagination: { total: 0, page: 1, pageSize: 0, pageCount: 1 } } }
        }
        const periodStart = new Date(ctx.query.filters.periodStart?.$eq)
        const periodEnd = new Date(ctx.query.filters.periodEnd?.$eq)
        delete ctx.query.filters.periodStart
        delete ctx.query.filters.periodEnd

        let guildIds = null;
        if (ctx.query?.filters?.guildId) {
            const guildId = ctx.query.filters.guildId?.$eq
            if (guildId) guildIds = [guildId]
            else guildIds = ctx.query.filters.guildId?.$in
            delete ctx.query.filters.guildId
        }

        const accountsRes = await strapi.service('api::account.account').findByLeader({
            pagination: ctx.query.pagination,
            filters: {
                ...ctx.query.filters,
                build: { id: { $notNull: true } },
                status: { statusCode: {$eq: "active"}}
            },
            populate: ['scholar', 'build', 'status'],
        }, userId, guildIds)

        const payout = new Payout({ accounts: accountsRes.results, periodStart, periodEnd })
        const payoutsDetail = await payout.launch();      

        const res = accountsRes.results.map((account) => ({
            accountId: account.id,
            account: pick(account, ['id', 'email', 'build', 'scholar', 'status']),
            detail: payoutsDetail?.[account.id],
        }))
        return { data: res, meta: { pagination: accountsRes.pagination } }
    },
    async findByScholar(ctx) {
        const scholarId = ctx.state.user.id
        const accounts = await strapi.service('api::account.account').findByScholar({
            filters: {},
            populate: []
        }, scholarId)

        const data = await strapi.query('api::slp.slp').findMany({
            where: {
                accountId: {
                    $in: accounts.results.map(_ => _.id)
                }
            },
            orderBy: {
                issuedDate: 'desc'
            },
            limit: accounts.results.length
        });

        return {
            data: data,
            meta: {
                pagination: {
                    pageSize: 15,
                    pageCount: (((accounts.results.length / 15) % 2) > 0 ? Math.floor(accounts.results.length / 15) + 1 : accounts.results.length / 15),
                    total: accounts.results.length
                }
            }
        }
    },
    async findOneByScholar(ctx) {
        const accounts = await strapi.service('api::account.account').findByScholar({
            filters: {},
            populate: []
        }, ctx.state.user.id)
        const slp = await super.findOne(ctx)
        if (!accounts.results.find(_ => _.id === slp.data.attributes.accountId)) {
            return ctx.notFound()
        }
        return slp
    },
    async updateByScholar(ctx) {
        const accounts = await strapi.service('api::account.account').findByScholar({
            filters: {},
            populate: []
        }, ctx.state.user.id)
        const slp = await super.findOne(ctx)
        if (!accounts.results.find(_ => _.id === slp.data.attributes.accountId)) {
            return ctx.notFound()
        }
        if (!slp.data.attributes.isScholarEditable) {
            return ctx.badRequest('Not allowed!')
        }
        return this.update(ctx)
    },

    async findByManager(ctx) {
        let guilds = null
        let filtersGuild = {}

        if (!ctx.query?.filters?.periodStart?.$eq || !ctx.query?.filters?.periodEnd?.$eq) {
            return { data: [], meta: { pagination: { total: 0, page: 1, pageSize: 0, pageCount: 1 } } }
        }
        const periodStart = new Date(ctx.query.filters.periodStart?.$eq)
        const periodEnd = new Date(ctx.query.filters.periodEnd?.$eq)
        delete ctx.query.filters.periodStart
        delete ctx.query.filters.periodEnd

        if (ctx.query?.filters?.groupId) {
            filtersGuild.group = { id: ctx.query.filters.groupId }
            delete ctx.query.filters.groupId
        }

        if (ctx.query?.filters?.guildId) {
            filtersGuild.id = ctx.query.filters.guildId
            delete ctx.query.filters.guildId
        }

        if (ctx.query?.filters?.gameId) {
            filtersGuild.game = { id: ctx.query.filters.gameId }
            delete ctx.query.filters.gameId
        }
        if (Object.keys(filtersGuild).length > 0) {
            guilds = await strapi.entityService.findMany('api::guild.guild', {
                filters: filtersGuild,
            })
        }
        const filtersAccounts = guilds ? { guild: guilds?.map(_ => _.id) } : {}
        const accountsRes = await strapi.service('api::account.account').find({
            pagination: ctx.query.pagination,
            filters: {
                ...filtersAccounts,
                build: { id: { $notNull: true } },
                status: { statusCode: {$eq: "active"}}
            },
            populate: ['scholar', 'build', 'status'],
        })
        const accounts = accountsRes.results;
        if (!ctx.query.filters) ctx.query.filters = {}
        const payout = new Payout({ accounts, periodStart, periodEnd })
        const payoutsDetail = await payout.launch();      

        const res = accounts.map((account) => ({
            accountId: account.id,
            account: pick(account, ['id', 'email', 'build', 'scholar', 'status']),
            detail: payoutsDetail?.[account.id],
        }))
        return { data: res, meta: { pagination: accountsRes.pagination } }
    },
    async update(ctx) {
        // TODO validate can this item modify?
        const detail = await strapi.service('api::slp.slp').findOne(ctx.params.id)
        const newData = {
            todaySoFar: ctx.request.body.data.todaySoFar
        }
        ctx.request.body.data = newData
        const [response] = await Promise.all([
            super.update(ctx),
            strapi.service('api::log.log').create({
                data: {
                    username: ctx.state.user.username,
                    method: 'UPDATE',
                    endpoint: ctx.request.originalUrl,
                    oldData: detail,
                    newData
                }
            })
        ])

        return response;
    }
}));
