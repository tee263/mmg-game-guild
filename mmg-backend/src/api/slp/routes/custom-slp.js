module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/slp-by-leader',
      handler: 'slp.findByLeader',
    }, {
      method: 'GET',
      path: '/slp-by-leader/:id',
      handler: 'slp.findOne',
    }, {
      method: 'PUT',
      path: '/slp-by-leader/:id',
      handler: 'slp.update',
    }, {
      method: 'GET',
      path: '/slp-by-scholar',
      handler: 'slp.findByScholar',
    }, {
      method: 'GET',
      path: '/slp-by-scholar/:id',
      handler: 'slp.findOneByScholar',
    }, {
      method: 'PUT',
      path: '/slp-by-scholar/:id',
      handler: 'slp.updateByScholar',
    },
    {
      method: 'GET',
      path: '/slp-by-manager',
      handler: 'slp.findByManager',
    }
  ]
}
