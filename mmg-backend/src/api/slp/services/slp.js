'use strict';

/**
 * slp service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::slp.slp');
