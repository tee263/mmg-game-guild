'use strict';

/**
 *  tracked-transaction controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::tracked-transaction.tracked-transaction');
