'use strict';

/**
 * tracked-transaction router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::tracked-transaction.tracked-transaction');
