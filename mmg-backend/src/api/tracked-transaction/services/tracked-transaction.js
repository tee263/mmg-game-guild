'use strict';

/**
 * tracked-transaction service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::tracked-transaction.tracked-transaction');
