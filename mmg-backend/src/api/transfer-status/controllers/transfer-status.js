'use strict';

/**
 *  transfer-status controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::transfer-status.transfer-status');
