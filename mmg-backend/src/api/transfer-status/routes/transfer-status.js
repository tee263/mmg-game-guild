'use strict';

/**
 * transfer-status router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::transfer-status.transfer-status');
