'use strict';

/**
 * transfer-status service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::transfer-status.transfer-status');
