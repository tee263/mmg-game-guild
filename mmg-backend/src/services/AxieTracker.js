const axios = require('axios')
const api = axios.create({
  baseURL: 'https://api.axie.management/v1',
  timeout: 10000
})
api.interceptors.response.use(function (response) {
  return response.data
})
module.exports = {
  getDetail: (address) => {
    const _add = address.replace('ronin:', '0x')
    return api.get(`/overview/${_add}`)
  }
}