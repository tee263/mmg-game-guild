const { getDays, getIssuedDateFromDatetime, getEndDate, getStartDate, getDaysInRangeWithAccount} = require('../../utils/datetime')
const _maxBy = require('lodash/maxBy')
const { uniqBy, sortBy } = require('lodash')
const addDays = require('date-fns/addDays')

module.exports = class Payout {
  constructor (options) {
    // Provide accounts or accountIds only
    const { accounts, accountId, periodStart, periodEnd } = options
    if (this.accounts && this.accountId) throw new Error('Provide accounts or accountIds only')
    this.accounts = accounts || []
    this.accountId = accountId
    this.accountIds = accountId ? [accountId] : accounts.map(_ => _.id)
    this.periodStart = periodStart
    this.periodEnd = periodEnd
    this.detail = {}
    this.accountIds.forEach((accountId) => {
      this.detail[accountId] = {
        slps: {},
        kpiRulesRange: [],
        total: 0,
        rawTotal: 0,
        claimableTotal: 0,
        days: [],
      }
    })
  }
  async launch() {
    if (this.accountIds.length === 0 || !this.periodStart || !this.periodEnd) return {}
    this.getDayRange()
    await this.getSlpInDayRange()
    await this.getKpiInDayRange()
    await this.getTotal()
    await this.calTotal()
    if (this.accountId) return this.detail[this.accountId]
    return this.detail
  }

  async getDayRange () {
    const days = getDays(this.periodStart, this.periodEnd)
    this.detail.days = days
    this.accountIds.forEach((accountId) => {
      this.detail[accountId].days = this.detail.days
    })
    return days
  }

  async getSlpInDayRange () {
    return Promise.all(
      this.detail.days.map(d => strapi.query('api::slp.slp').findMany({
      select: ['todaySoFar', 'accountId'],
      where: {
        issuedDate: {
          $gte: getStartDate(d),
          $lte: getEndDate(d)
        },
        accountId: {
          $in: this.accountIds
        }
      },
      orderBy: { issuedDate: 'desc' }
    }).then(_ => {
      _.forEach(slp => {
        this.detail[slp.accountId].slps[d] = slp?.todaySoFar
      })
      return _
    })))
  }

  async getTotal () {
    return Promise.all(this.accountIds.map((accountId) => {
      return strapi.query('api::slp.slp').findOne({
        where: {
          issuedDate: {
            $lte: getEndDate(this.periodEnd)
          },
          accountId: accountId,
          rawTotal: {
            $gt: -1
          }
        },
        orderBy: { issuedDate: 'desc' }
      }).then((data) => {
        this.detail[accountId].rawTotal = data?.total || 0
        this.detail[accountId].claimableTotal = data?.claimableTotal || 0
      })
    }))
  }

  // SLP that scholar will get
  async calTotal() {
    return Promise.all(this.accountIds.map(async (accountId) => {
      const accountForAction = this.accounts.find(value => value.id === accountId)
      const { slps, kpiRulesRange } = this.detail[accountId]
      kpiRulesRange.forEach((kpiRule, index) => {
        const days = getDaysInRangeWithAccount(kpiRule.start, kpiRule.end, accountForAction)
        const totalInRange = days.reduce((acc, day) => {
          const slp = slps[day]
          if (!slp) return acc
          return acc + slp
        }, 0)
        // const avgInRange = Number((totalInRange / days.reduce((r, v) => {
        //   if (slps[v] > 0) return r + 1
        //   return r
        // }, 0)).toFixed(10));
        const avgInRange = Number((totalInRange / days.length).toFixed(10));
        kpiRulesRange[index].totalSlp = totalInRange
        kpiRulesRange[index].avgSlp = avgInRange
        const validKpis = kpiRulesRange[index].kpiRules.filter((rule) => rule.value <= avgInRange)
        const bestKpi = _maxBy(validKpis, 'percent')
        kpiRulesRange[index].maxPercent = bestKpi?.percent || 0
        // Using toFixed because in some cases, the result is not accurate. For example, 58*0.2 = 11.600000000000001 instead of 11.6
        kpiRulesRange[index].payable = Number((kpiRulesRange[index].maxPercent * totalInRange).toFixed(10))
      })
      this.detail[accountId].total = kpiRulesRange.reduce((acc, rule) => {
        return acc + rule.payable
      }, 0)

      return;
    }))
  }

  async getKpiRuleInRange (build) {
    /* Get all kpi in day range
    Example: periodStart = 03/04, periodEnd = 05/04
    KPI Rules: [
      { id: 1, start: 31/03, end: null }
      { id: 2, start: 30/03, end: 02/04 },
      { id: 3, start: 01/04, end: 03/04 },
      { id: 4, start: 02/04, end: 04/04 },
      { id: 5, start: 04/04, end: 06/04 },
      { id: 6, start: 05/04, end: 07/04 },
      { id: 7, start: 06/04, end: 08/04 },
      { id: 8, start: 04/04, end: null },
      { id: 9, start: 04/04, end: 04/04 },
    ]
    */
    const kpis = await strapi.query('api::kpi-rule.kpi-rule').findMany({
      where: {
        $or: [
          { build, startDate: { $lte: getEndDate(this.periodStart) }, endDate: { $gte: getStartDate(this.periodStart) } }, // => id: 3,4
          { build, startDate: { $lte: getEndDate(this.periodEnd) }, endDate: { $gte: getStartDate(this.periodEnd) } }, // => id: 5,6
          { build, startDate: { $gte: getStartDate(this.periodStart) }, endDate: { $lte: getEndDate(this.periodEnd) } },// id: 9
          { build, startDate: { $lte: getEndDate(this.periodEnd) }, endDate: { $null: true }  }, // id: 1,8
        ]
      },
    })
    return { kpis, build }
  }

  async getKpiInDayRange() {
    if (this.accountId) {
      const account = await strapi.query('api::account.account').findOne({
        where: { id: this.accountId },
        populate: ['build']
      })
      this.accounts = [account]
    }
  
    const builds = uniqBy(this.accounts.filter(_ => _.build?.id).map(_ => _.build), 'id')
    if (builds.length === 0) return

    const kpiRules = await Promise.all(builds.map(build => this.getKpiRuleInRange(build)))
    
    const kpiRulesObj = kpiRules.reduce((obj, kpiRule) => {
      obj[kpiRule.build.id] = kpiRule.kpis
      return obj
    }, {})

    const accountsObj = this.accounts.reduce((obj, account) => {
      obj[account.id] = account
      return obj
    }, {})


    const firstDaySlpMinedOfAccount = await Promise.all(this.accountIds.map((accountId) => {
      return strapi.query('api::slp.slp').findOne({
        where: {
          accountId: accountId,
          todaySoFar: {
            $gt: 0
          }
        },
        orderBy: { issuedDate: 'asc' },
      })
    }))

    const firstDaySlpMinedObj = firstDaySlpMinedOfAccount.reduce((obj, item) => {
      if (item) obj[item.accountId] = item
      return obj
    }, {})

    this.accountIds.forEach((accountId) => {
      const kpiRulesOfAccount = kpiRulesObj[accountsObj[accountId]?.build?.id]

      const firstDaySlpMined = firstDaySlpMinedObj[accountId]
      if (firstDaySlpMined) {
        const startDate = new Date(this.accounts[0].validStartDate || "1970-1-1")
        const endDate = addDays(startDate, 2)
        const specialKpiRule = { 
          id: 0, 
          percent: 1,
          value: 0,
          startDate: getIssuedDateFromDatetime(startDate),
          endDate: getIssuedDateFromDatetime(endDate)
        }
        kpiRulesOfAccount.push(specialKpiRule)
      }

      const kpiRulesByDay = this.detail[accountId].days.map((dayString) => {
        const day = new Date(dayString)
        const kpiRules = kpiRulesOfAccount.filter(_ => {
          const start = new Date(_.startDate || 0)
          const end = _.endDate ? new Date(_.endDate) : new Date()
          return start <= day && end >= day
        })
        return {
          day: dayString,
          compareStr: sortBy(kpiRules, ['id']).map(_ => _.id).join(','),
          kpiRules,
        }
      })
      const kpiRulesRange = []
      let temp = {}
      kpiRulesByDay.forEach((_, index) => {
        if (!temp.start) { 
          temp.start = _.day
          temp.end = _.day
          temp.compareStr = _.compareStr,
          temp.kpiRules = _.kpiRules
        }
        if (temp.compareStr === _.compareStr) temp.end = _.day
        else {
          delete temp.compareStr
          kpiRulesRange.push(temp)
          temp = {
            start: _.day,
            end: _.day,
            compareStr: _.compareStr,
            kpiRules: _.kpiRules
          }
        }
        if (index === kpiRulesByDay.length - 1) {
          delete temp.compareStr
          kpiRulesRange.push(temp)
        }
      })
      this.detail[accountId].kpiRulesRange = kpiRulesRange
    })
  }
}
