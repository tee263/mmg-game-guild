const fs = require('fs');
const { createStrapiInstance } = require('../helpers/strapi');

beforeAll(async () => {
  await createStrapiInstance();
});

afterAll(async () => {
  const dbSettings = strapi.config.get('database.connections.default.settings')

  await strapi.destroy()

  if (dbSettings && dbSettings.filename) {
    const tmpDbFile = `${__dirname}/../${dbSettings.filename}`
    if (fs.existsSync(tmpDbFile)) {
      fs.unlinkSync(tmpDbFile)
    }
  }
})

xit('strapi is defined', async () => {
  const accounts = await strapi.service('api::account.account').getSlpsInDayRange({
    params: {
      id: 1285
    },
    query: {
      startTime: new Date('2022-02-16'),
      endTime: new Date('2022-02-22'),
    }
  })
  console.log(accounts)
  expect(strapi).toBeDefined()
});
