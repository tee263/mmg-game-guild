const fs = require('fs');
const { createStrapiInstance } = require('../helpers/strapi');

beforeAll(async () => {
  await createStrapiInstance();
});

afterAll(async () => {
  const dbSettings = strapi.config.get('database.connections.default.settings')

  await strapi.destroy()

  if (dbSettings && dbSettings.filename) {
    const tmpDbFile = `${__dirname}/../${dbSettings.filename}`
    if (fs.existsSync(tmpDbFile)) {
      fs.unlinkSync(tmpDbFile)
    }
  }
})

it('strapi is defined', async () => {
  expect(strapi).toBeDefined()
})

it('get performance', async() => {
  // TODO get detail of payout
  const result = await payment.launch()
  console.log(result)
})
