
const axios = require('axios')
const api = axios.create({
  baseURL: 'https://game-api.axie.technology',
  timeout: 10000
})
api.interceptors.response.use(function (response) {
  return response.data
})
module.exports = {
  getDetail: (address) => {
    return api.get(`/api/v1/${address}`)
  }
}
