const axios = require('axios')
const {getIssuedDateFromDatetime} = require("./datetime");

const axiosInstance = axios.create({
  baseURL: 'https://api.axie.management/v1',
  timeout: 10000,
  headers: {
    'origin': "https://tracker.axie.management"
  }
})

axiosInstance.interceptors.response.use(function (response) {
  return response.data
})

module.exports = {
  getDetail: (address) => {
    const _add = address.replace('ronin:', '0x')
    return axiosInstance.get(`/overview/${_add}`)
  },

  getSlpDetailForLast60Days: (address) => {
    const _add = address.replace('ronin:', '0x')
    return axiosInstance.get(`/daily/${_add}`)
  },

  transformSlpRecordData: (data, accountId, scholarId, date) => ({
    total: data?.total,
    claimableTotal: data?.claimableTotal,
    rawTotal: data?.rawTotal,
    rawClaimableTotal: data?.rawClaimableTotal,
    todaySoFar: data?.todaySoFar || 0,
    yesterdaySLP: data?.yesterdaySLP || 0,

    issuedDate: getIssuedDateFromDatetime(date),
    accountId,
    scholarId,
  })
}