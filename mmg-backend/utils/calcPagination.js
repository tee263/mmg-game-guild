module.exports.calcPagination = (params) => {
  const page = parseInt(params?.page) || 1
  const pageSize = parseInt(params?.pageSize) || 10
  const start = (page - 1 < 0 ? 0 : page - 1) * pageSize
  const end = pageSize * page
  return { start, end, page, pageSize }
}
