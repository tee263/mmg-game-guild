const format = require('date-fns/format')
const addDays = require('date-fns/addDays')

module.exports.getIssuedDateFromDatetime = (datetime) => {
  return format(datetime, 'yyyy-MM-dd')
}

module.exports.getDays = (start, end) => {
  let point = new Date(start)
  const results = []
  while (point <= new Date(end)) {
    results.push(this.getIssuedDateFromDatetime(point))
    point = addDays(point, 1)
  }
  return results
}

module.exports.getDaysInRangeWithAccount = (start, end, account) => {
  let point = new Date(start)
  const results = []
  while (point <= new Date(end)) {
    if((point.getTime() >= new Date(account?.validStartDate || "1970-1-1").getTime()) &&  (point.getTime() <= new Date(account?.validEndDate || "2025-1-1").getTime())) {
      results.push(this.getIssuedDateFromDatetime(point))
    }
    point = addDays(point, 1)
  }
  return results
}

module.exports.getStartDate = date => {
  let result = new Date(date)
  result = new Date(result.setHours(0))
  result = new Date(result.setMinutes(0))
  result = new Date(result.setSeconds(0))
  return result
}

module.exports.getEndDate = date => {
  let result = new Date(date)
  result = new Date(result.setHours(23))
  result = new Date(result.setMinutes(59))
  result = new Date(result.setSeconds(59))
  return result
}

module.exports.toUTC = datetime => {
  const myDate = (typeof datetime === 'number')
      ? new Date(datetime)
      : datetime;

  if (!myDate || (typeof myDate.getTime !== 'function')) {
    return 0;
  }

  const getUTC = myDate.getTime();
  const offset = myDate.getTimezoneOffset() * 60000;
  return getUTC - offset;
}

module.exports.fromUTC = datetime => {
  const myDate = (typeof datetime === 'number')
      ? new Date(datetime)
      : datetime;

  if (!myDate || (typeof myDate.getTime !== 'function')) {
    return 0;
  }

  const getUTC = myDate.getTime();
  const offset = myDate.getTimezoneOffset() * 60000; // It's in minutes so convert to ms
  return getUTC + offset; // UTC + OFFSET
}

module.exports.getDateKeyFromStrDate = datetime => {
  const myDate = (typeof datetime === 'number')
      ? new Date(datetime)
      : datetime;

  if (!myDate || (typeof myDate.getTime !== 'function')) {
    return 0;
  }

  const getUTC = myDate.getTime();
  const offset = myDate.getTimezoneOffset() * 60000; // It's in minutes so convert to ms
  return getUTC + offset; // UTC + OFFSET
}

module.exports.getOneDayInMilliseconds = () => {
  return 24 * 60 * 60 * 1000;
}

module.exports.getOffsetTimezone = () => {
  return Math.abs(new Date().getTimezoneOffset()/60);
}
