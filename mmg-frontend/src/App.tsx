import { Refine } from "@pankod/refine-core"
import {
  notificationProvider,
  Layout,
} from "@pankod/refine-antd"
import { Dashboard } from "./pages/dashboard"
import { DataProvider } from "@pankod/refine-strapi-v4"
import routerProvider from "@pankod/refine-react-router"
import authProvider, {axiosInstance} from './authProvider'
import resources from './router'
import "@pankod/refine-antd/dist/styles.min.css"
import {Title} from "./components/sider"
import accessControlProvider from "./accessControlProvider"
import { LoginPage } from './pages/login'

const { REACT_APP_API_URL } = process.env

const App: React.FC = () => {
  return (
    <Refine
      dataProvider={DataProvider(REACT_APP_API_URL + "/api", axiosInstance)}
      authProvider={authProvider}
      routerProvider={routerProvider}
      notificationProvider={notificationProvider}
      LoginPage={LoginPage}
      DashboardPage={Dashboard}
      Layout={Layout}
      resources={resources}
      accessControlProvider={accessControlProvider}
      Title={({ collapsed }) => <Title collapsed={collapsed} />}
    />
  )
}

export default App