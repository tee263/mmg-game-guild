import authProvider from "./authProvider"
import {AccessControlProvider} from "@pankod/refine-core"

export default<AccessControlProvider> {
  can: async ({ action, params, resource }) => {
    const role = await authProvider.getPermissions()
    if (action === 'list') {
      if (role === 'bod') {
        return { can:
            [
              'dashboard', 'builds', 'games', 'scholars', 'guild-leaders', 'guilds',
              'accounts', 'payout-confirms', 'accounts'
            ].indexOf(resource) > -1
        }
      } else if (role === 'manager') {
        return { can:
            [
              'dashboard', 'builds', 'groups', 'scholars', 'guild-leaders', 'guilds',
              'payouts', 'payments', 'payment-transaction', 'accounts', 'roles', 'role-by-type', 'slp-by-manager'
            ].indexOf(resource) > -1
        }
      } else if (role === 'leader') {
        return { can:
            [
              'dashboard', 'builds', 'slp-by-leader', 'accounts-by-leader', 'scholars-by-leader',
              'guilds-by-leader', 'payouts-by-leader', 'payments-by-leader'
            ].indexOf(resource) > -1
        }
      } else if (role === 'accountant') {
        return { can:
            [
              'dashboard', 'payouts', 'payments', 'payment-transaction'
            ].indexOf(resource) > -1
        }
      } else if (role === 'guest') {
        return { can:
            [
              'dashboard', 'builds', 'groups', 'scholars', 'guild-leaders', 'guilds',
              'payouts', 'payments', 'accounts', 'roles', 'role-by-type', 'slp-by-manager'
            ].indexOf(resource) > -1
        }
      }
      else if (role === 'scholar') {
        return { can:
            [
              'dashboard', 'accounts-by-scholar', 'scholar/payout-accounts'
            ].indexOf(resource) > -1
        }
      }else {
        return { can: false }
      }
    }
    return { can: true }
  }
}