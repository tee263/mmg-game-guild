import {AuthProvider} from "@pankod/refine-core"
import {AuthHelper} from '@pankod/refine-strapi-v4'
import axios from "axios"

export const API_URL = process.env.REACT_APP_API_URL
export const axiosInstance = axios.create()

const TOKEN_KEY = "strapi-jwt-token"
const ROLE_KEY = "role"
const USER_ID = "userId"
const strapiAuthHelper = AuthHelper(API_URL + "/api")
const myRoleUrl = `${API_URL}/api/my-role`

const authProvider: AuthProvider = {
  login: async ({ username, password }) => {
    const { data, status } = await strapiAuthHelper.login(
      username,
      password,
    )
    if (status === 200) {
      localStorage.setItem(TOKEN_KEY, data.jwt)

      // set header axios instance
      axiosInstance.defaults.headers = {
        Authorization: `Bearer ${data.jwt}`,
      }

      const role = await axiosInstance.get(myRoleUrl).then(_ => _.data.type)
      localStorage.setItem(ROLE_KEY, role)

      return Promise.resolve()
    }
    return Promise.reject()
  },
  logout: () => {
    localStorage.removeItem(TOKEN_KEY)
    return Promise.resolve()
  },
  checkError: () => Promise.resolve(),
  checkAuth: () => {
    const token = localStorage.getItem(TOKEN_KEY)
    if (token) {
      axiosInstance.defaults.headers = {
        Authorization: `Bearer ${token}`,
      }
      return Promise.resolve()
    }

    return Promise.reject()
  },
  getPermissions: () => Promise.resolve(localStorage.getItem(ROLE_KEY)),
  getUserIdentity: async () => {
    const token = localStorage.getItem(TOKEN_KEY)
    if (!token) {
      return Promise.reject()
    }

    const { data, status } = await strapiAuthHelper.me(token)
    if (status === 200) {
      const { id, username, email } = data
      const role = await axiosInstance.get(myRoleUrl).then(_ => _.data.type)
      localStorage.setItem(ROLE_KEY, role)
      localStorage.setItem(USER_ID, id.toString())
      return Promise.resolve({
        name: username,
        id,
        username,
        email,
      })
    }

    return Promise.reject()
  },
}

export default authProvider