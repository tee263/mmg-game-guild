import routerProvider from "@pankod/refine-react-router"

const { Link } = routerProvider

interface TitleProps {
  collapsed: boolean
}

export const Title: React.FC<TitleProps> = (props) => {
  return (
    <Link to="/">
      {props.collapsed ? (
        <img
          src="/coincu-fav.svg"
          alt="Refine"
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            padding: "12px 24px"
          }}
        />
      ) : (
        <img
          src="/coincu.png"
          alt="Refine"
          style={{
            width: "200px",
            padding: "12px 24px"
          }}
        />
      )}
    </Link>
  )
}
