
export interface IAccountFilterVariables {
  id: string;
  game: string;
  guild: string;
  build: string;
}