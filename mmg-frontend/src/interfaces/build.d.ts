import {IProject} from "./index"

export interface IBuild {
  id: string;
  name: string;
  kpi_rules: IKpiRule[];
  project: IProject;
  tier: string;
}

export interface IKpiRule {
  id: string;
  Tier: string;
  percent: number;
  value: number;
  startDate: string;
  endDate: string;
}