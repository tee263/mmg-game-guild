import {IGame} from "./index";

export interface IGameFilterVariables extends IGame{
  populate: boolean | string[];
}
