import {IRole} from "./role"
import {IScholar} from "./scholar"

export interface IAuthUser {
    id: string;
    username: string;
    email: string;
    role: { data: IRole };
    status: string;
    confirmed: boolean;
}

export interface IRegisUser {
    id: string;
    username: string;
    email: string;
    identifyId: string;
    personalWallet: string;
    phoneNumber: string;
    role: { data: IRole };
    guild: {data: IGuild};
    password: string;
    confirmed: boolean;
}

export interface ILabel {
    id: string;
    title: string;
    color: string;
}

export interface IPriority {
    id: string;
    title: string;
}

export interface IStatus {
    id: string;
    title: string;
    statusCode: string;
}

export interface IProject {
    id: string;
    name: string;
    mainToken: string;
    subToken: string;
    officialSite: string;
    contract: string;
    builds: IBuild[]
}

export interface IGame {
    id: number;
    title: string;
    projectName: string;
    token: string;
    status: IStatus;
    publishedAt: Date;
    createdByUser: IAuthUser;
    updatedByUser: IAuthUser;
}

export interface IGroup {
    id: number;
    groupName: string;
    project: IProject;
    status: IStatus;
    publishedAt: Date;
    guilds: IGuild[];
}

export interface IGuild {
    id: string;
    name: string;
    group: IGroup;
    created_at:string;
}

export interface IGuildLeader {
    id: string;
    name: string;
    group: IGroup;
    guilds: IGuild[];
    users_permissions_user: IAuthUser;
    createdAt: string;
}

export interface ITask {
    id: string;
    title: string;
    description: string;
    start_time: string;
    end_time: string;
    label: string;
    priority: string;
    status: string;
    users: string;
}

export interface ITaskFilterVariables {
    title: string;
    label: string;
    priority: string;
    users: string;
    status: string;
    start_time: [Dayjs, Dayjs];
    end_time: [Dayjs, Dayjs];
}
export interface IGameFilterVariables {
    groupName: string;
    status: string;
}

export interface IAccount {
    id: number;
    email: string;
    password: string;
    wallet: string;
    paidAt: string;
    build: IBuild;
    guild: IGuild;
    transfer_status: ITransferStatus;
    validStartDate: string;
    validEndDate: string;
    status: IStatus;
    scholar: IScholar;
}

export interface IAccountFilterVariables {
    email: string;
    wallet: string;
    build: string;
    axie_transfer: string;
    status: string;
    group: string;
    guild: string;
}

export interface IUserFilterVaribles {
    status: string;
}

export interface ITransferStatus {
    id: string;
    status: string;
}

export interface ISlp {
    id: string;
    todaySoFar: string;
    accountId: string;
    scholarId: string;
}