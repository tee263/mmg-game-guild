import {IAccount} from "./index"
import {IPayout} from "./payout"

export type IPayoutAccountState = 'waitForScholar' | 'rejectedByScholar' | 'waitForManager' | 'rejectedByManager' | 'confirmed';

export interface IPayoutAccount {
  id: string;
  periodStart: string;
  periodEnd: string;
  account: IAccount;
  amount: number;
  payout: IPayout;
  detail: any;
  state: IPayoutAccountState;
  scholarNote: string;
}

export interface IPaymentRecord {
  id: string;
  scholarId: string;
  scholarName: string;
  scholarStatus: string;
  scholarWallet: string;
  needPayment: number;
  paid: number;
  remaining: number;
  amount: number;
  account: IAccount;
}

export interface ITxHashData {
  status: number;
  blockNumber: string;
  timestamp: number;
  from: string;
  to: string;
  value: number;
}

export interface ITxHash {
  id: number;
  txHash: string;
}
