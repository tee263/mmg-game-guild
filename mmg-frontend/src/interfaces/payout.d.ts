import {IAccount} from "./index"

export interface IPayout {
  id: string;
  name: string;
  isPublished: boolean;
  periodStart: string;
  periodEnd:string;
  accounts: IAccount[];
  payout_confirm: IPayoutConfirm;
  selectAllAccounts?: boolean;
}

export interface IPayoutConfirm {
  id: string;
  payout: IPayout;
  isConfirmed: boolean;
}
