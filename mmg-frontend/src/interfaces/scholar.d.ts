import {IAccount, IAuthUser, IGuild, IStatus} from "./index"

export interface IScholar {
  id: string;
  createdAt: string;
  username: string;
  identifyId: string;
  personalWallet: string;
  phoneNumber: string;
  accounts: IAccount[];
  status: IStatus;
  users_permissions_user: IAuthUser;
}

export interface IScholarFilterVariables {
  gmail: string;
  currency: string;
  account: string;
  guild: string;
}
