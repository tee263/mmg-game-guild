
export interface ISlp {
  updatedAt: string
  id: string;
  name: string;
  accountId: string;
  scholarId: string;
  claimableTotal: string;
  issuedDate: string;
  todaySoFar: number;
}
