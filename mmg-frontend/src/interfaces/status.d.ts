import {IGame} from "./index";

export interface IStatusFilterVariables extends IGame{
  populate: boolean | string[];
}