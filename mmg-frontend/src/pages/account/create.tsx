import {
  useForm,
  Create,
} from "@pankod/refine-antd"
import {IAccount} from "interfaces"
import FormAccount from "./form"
import {IResourceComponentsProps} from "@pankod/refine-core"

export const CreateAccount: React.FC<IResourceComponentsProps> = () => {
  const { formProps, saveButtonProps } = useForm<IAccount>()

  return (
    <Create saveButtonProps={saveButtonProps}>
      <FormAccount formProps={formProps} mode={"create"}/>
    </Create>
  )
}
