import {
  IResourceComponentsProps,
} from "@pankod/refine-core"
import { IAccount } from "interfaces"
import FormAccount from "./form"
import {Edit, useForm} from "@pankod/refine-antd"

export const EditAccount: React.FC<IResourceComponentsProps> = () => {
  const { formProps, saveButtonProps, queryResult } = useForm<IAccount>({
    resource: 'accounts',
    metaData: { populate: ['scholar', 'status', 'guild.group', 'build.project'] }
  })

  const scholarId = queryResult?.data?.data.scholar ? queryResult?.data?.data.scholar.id : -1

  return (
    <Edit saveButtonProps={saveButtonProps}>
      <FormAccount formProps={formProps} scholarId={scholarId} mode={"edit"}/>
    </Edit>
  )
}
