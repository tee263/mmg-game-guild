import React, { useState } from "react"
import {Button, Col, Form, FormProps, Icons, Input, Row, Select, Switch, useSelect,} from "@pankod/refine-antd"
import {IGroup, IGuild, IStatus} from "interfaces"

export const Filter: React.FC<{ formProps: FormProps }> = ({ formProps }) => {
  const [selectedGroup, setSelectedGroup] = useState<any>(null)
  const { selectProps: groupProps } = useSelect<IGroup>({
    resource: "groups",
    optionLabel: 'groupName'
  })

  const {selectProps: statusProps} = useSelect<IStatus>({
    resource: "statuses",
    optionValue: "statusCode"
  })

  const { selectProps: guildProps } = useSelect<IGuild>({
    resource: "guilds",
    optionLabel: 'name',
    filters: selectedGroup ? [{
      field: 'group][id]]',
      operator: 'eq',
      value: selectedGroup,
    }] : [],
    metaData: {
      populate: ['users_permissions_user', 'group']
    }
  })

  return (
    <Form layout="vertical" {...formProps}>
      <Row gutter={16}>
        <Col lg={4} xs={24}>
          <Form.Item label="Search" name="email">
            <Input placeholder="Email" prefix={<Icons.SearchOutlined />} />
          </Form.Item>
        </Col>
        <Col lg={4} xs={24}>
          <Form.Item label="Wallet" name="wallet">
            <Input placeholder="Wallet" prefix={<Icons.SearchOutlined />} />
          </Form.Item>
        </Col>
        <Col lg={4} xs={24}>
          <Form.Item label="Group" name="group">
            <Select
              placeholder='group'
              {...groupProps}
              onChange={(value) => {
                setSelectedGroup(value)
                formProps.form?.resetFields(['guild'])
              }}
            />
          </Form.Item>
        </Col>
        <Col lg={4} xs={24}>
          <Form.Item label="Guild" name="guild">
            <Select
              placeholder='Guild'
              {...guildProps}
            />
          </Form.Item>
        </Col>
        <Col lg={4} xs={24}>
          <Form.Item label="Status" name="status">
            <Select
              placeholder='Status'
              {...statusProps}
            />
          </Form.Item>
        </Col>
        <Col lg={6} xs={24}>
          <Form.Item>
            <Button htmlType="submit" type="primary">
              Filter
            </Button>
          </Form.Item>
        </Col>
      </Row>
    </Form>
  )
}
