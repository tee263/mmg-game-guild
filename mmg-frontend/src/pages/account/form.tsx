import {Form, Input, Select, useSelect} from "@pankod/refine-antd"
import {DefaultOptionType} from "rc-select/lib/Select"
import {IAccount, IGroup, IGuild, IProject, IStatus} from "../../interfaces"
import {IBuild} from "../../interfaces/build"
import {IScholar} from "../../interfaces/scholar"
import {useOne} from "@pankod/refine-core"
import {useState} from "react"

const FormAccount: React.FC<any> = ({formProps, scholarId, mode}) => {
  // const {selectProps: selectTransferStatusProps} = useSelect<ITransferStatus>({
  //   resource: "transfer-statuses",
  //   optionLabel: "status"
  // })
  const [listGuildOtp, setListGuildOpt] = useState<DefaultOptionType[] | undefined>(undefined)
  const [listBuildOtp, setListBuildOpt] = useState<DefaultOptionType[] | undefined>(undefined)
  const {selectProps: statusProps} = useSelect<IStatus>({
    resource: "statuses",
  })

  const {selectProps: buildProps, queryResult: buildList} = useSelect<IBuild>({
    resource: "builds",
    optionLabel: "name",
    fetchSize: 100,
    metaData: {
      populate: ["project"]
    },
  })
  const {selectProps: gameProps, queryResult: projectList} = useSelect<IProject>({
    resource: "projects",
    optionLabel: "name",
    fetchSize: 100,
    metaData: {
      populate: ["builds"]
    },
  })

  const {selectProps: scholarProps, queryResult} = useSelect<IScholar>({
    resource: "scholars",
    optionLabel: "username",
    metaData: {
      populate: ["users_permissions_user, accounts"]
    },
    filters: [
      {
        field: "users_permissions_user][confirmed]",
        operator: "eq",
        value: true,
      }
    ],
    fetchSize: 100
  })

  const listScholar: DefaultOptionType[] | { label: string; value: string }[] | undefined =  []

  const owner = useOne<IScholar>({
    resource: "scholars",
    id: scholarId > 0 ? scholarId : -1,
    metaData: {populate: ['users_permissions_user']},
    queryOptions: {
      enabled: scholarId > 0
    }
  })

  if(owner?.data?.data) {
    const ownerLabel = owner?.data?.data.users_permissions_user ? owner?.data?.data.users_permissions_user.username : owner?.data?.data.username
    listScholar.push({label: ownerLabel || "", value: owner?.data?.data.id})
  }

  if(queryResult) {
    queryResult.data?.data.map(value => {
      listScholar.push({label: value.users_permissions_user ? value.users_permissions_user.username : "", value: value?.id})
    })
  }
  scholarProps.options = listScholar

  const {selectProps: guildProps, queryResult: guildList} = useSelect<IGuild>({
    resource: "guilds",
    optionLabel: "name",
    fetchSize: 100,
    metaData: { populate: "group"}
  })

  const {selectProps: groupProps, queryResult: groupList} = useSelect<IGroup>({
    resource: "groups",
    optionLabel: "groupName",
    fetchSize: 100,
    metaData: { populate: "guilds"}
  })
  if(listGuildOtp)
    guildProps.options = listGuildOtp
  if(listBuildOtp)
    buildProps.options = listBuildOtp

  return (
    <Form {...formProps} wrapperCol={{span: 12}} layout="vertical">
      <Form.Item label="Email" name="email" rules={[{required: true}]}>
        <Input/>
      </Form.Item>
      <Form.Item label="Password" name="password"
        rules={[
          {
            required: mode !== "edit",
            message: 'Please input your password!',
          },
        ]}
      >
        <Input/>
      </Form.Item>
      <Form.Item label="Wallet" name="wallet" rules={[{required: true, message: 'Please input your account wallet!'}]}>
        <Input/>
      </Form.Item>
      <Form.Item label="Start Date" name="validStartDate">
        <Input type={"date"}/>
      </Form.Item>
      <Form.Item label="End Date" name="validEndDate">
        <Input type={"date"}/>
      </Form.Item>
      <Form.Item label="Scholar" name={["scholar", "id"]}>
        <Select {...scholarProps}/>
      </Form.Item>
      <Form.Item
        label="Group"
        name={['guild', 'group', 'id']}
      >
        <Select {...groupProps} onChange={(group: any) => {
          const listGuildOption : DefaultOptionType[] | { label: string; value: string }[] | undefined =  []
          const guilds = groupList.data?.data.find(item => item.id === group)?.guilds
          if(guilds) {
            guilds.map(i => {
              listGuildOption.push({label: i.name, value: i.id})
            })
          }
          setListGuildOpt(listGuildOption)
          formProps.form?.setFields([
            {
              name: ["guild", "id"],
              value: listGuildOption[0].value
            }
          ])
        }}/>
      </Form.Item>
      <Form.Item<IAccount> label="Guild" name={["guild", "id"]}>
        <Select {...guildProps} onChange={(guild: any) => {
          const guildListDetail = guildList
          let guildDetail
          if(guildListDetail)
            guildDetail = guildListDetail.data?.data.find(opt => opt.id === guild)
          formProps.form?.setFields([
            {
              name: ['guild', 'group', 'id'],
              value: guildDetail?.group.id
            }
          ])
        }}/>
      </Form.Item>
      <Form.Item label="Game" name={["build", "project", "id"]}>
        <Select {...gameProps} onChange={(project: any) => {
          const listBuildOption : DefaultOptionType[] | { label: string; value: string }[] | undefined =  []
          const buildListByProject = projectList.data?.data.find(item => item.id === project)?.builds
          if(buildListByProject) {
            buildListByProject.map(i => {
              listBuildOption.push({label: i.name, value: i.id})
            })
          }
          setListBuildOpt(listBuildOption)
          formProps.form?.setFields([
            {
              name: ["build", "id"],
              value: listBuildOption[0].value
            }
          ])
        }}/>
      </Form.Item>
      <Form.Item label="Build" name={["build", "id"]}>
        <Select {...buildProps} onChange={(build: any) => {
          const buildListDetail = buildList
          let buildDetail
          if(buildListDetail)
            buildDetail = buildListDetail.data?.data.find(opt => opt.id === build)
          formProps.form?.setFields([
            {
              name: ["build", "project", "id"],
              value: buildDetail?.project.id
            }
          ])
        }}/>
      </Form.Item>
      

      {/*<Form.Item*/}
      {/*  label="Transfer"*/}
      {/*  name={["transfer_status", "id"]}*/}
      {/*>*/}
      {/*  <Select {...selectTransferStatusProps} />*/}
      {/*</Form.Item>*/}
      <Form.Item
        label="Status"
        name={['status', 'id']}
      >
        <Select {...statusProps} />
      </Form.Item>
    </Form>
  )
}

export default FormAccount
