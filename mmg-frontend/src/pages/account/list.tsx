import React, {useState} from "react"
import {
  Card,
  Col,
  CreateButton,
  EditButton,
  Form,
  Input,
  List,
  Row,
  Select,
  ShowButton,
  Space,
  Table,
  useSelect,
  useTable
} from "@pankod/refine-antd"

import {IAccount, IAccountFilterVariables, IGuild, IStatus,} from "interfaces"

import {Filter} from "./filter"
import {CrudFilters, HttpError, IResourceComponentsProps, useCreateMany, useCustom} from "@pankod/refine-core"
import {addressShortener} from "../../utility"
import readXlsxFile from "read-excel-file"
import {
  accountExcelCol,
  accountSchema as schema,
  API_URL,
  emailRegex,
  errorCol,
  schemaErrorCol,
  STATUS_ENUM
} from "../../utility/DataMappingUtils"
import {Button, Modal} from "antd"
import {openNotification} from "../../utility/NotificationUtils"
import {IBuild} from "../../interfaces/build"
import {axiosInstance} from "../../authProvider"

const ROLE_KEY = "role"

export const ListAccount: React.FC<IResourceComponentsProps> = () => {

  const allAccounts = useCustom<IAccount[]>({
    url: `${API_URL}/api/all-accounts`,
    method: "get",
    queryOptions: {enabled: false }
  })

  const [visible, setVisible] = useState(false)
  const [errorPopVisible, setErrorPopVisible] = useState(false)
  const [selectedGuild, setSelectedGuild] = React.useState(-1)
  const [excelDataAccounts, setExcelDataAccounts] = useState<any>([])
  const [schemaErorrs, setSchemaErorrs] = useState<any>([])
  const errorData: any[] = []
  const [allAccountsData, setAllAccountsData] = useState<any>([])
  const [allBuildsData, setAllBuildsData] = useState<any>([])
  const { selectProps: guildsProps } = useSelect<IGuild>({
    resource: "guilds",
    optionLabel: "name"
  })

  const {queryResult} = useSelect<IStatus>({
    resource: "statuses",
  })

  const { queryResult: buildList } = useSelect<IBuild>({
    resource: "builds",
    optionLabel: "name",
    fetchSize: 100,
    queryOptions: {enabled: false }
  })

  const { mutate }  = useCreateMany<IAccount>()

  excelDataAccounts.forEach((record : any, index: any) => {
    if(allAccountsData.length > 0) {
      const {email, wallet, build} = record

      if(!emailRegex.test(email)) {
        errorData.push({field: "Email", row: index + 2, value:"'" + email + "' => Invalid Email Address"})
      }

      if(allBuildsData.find((i: any) => i.id == build) == undefined) {
        errorData.push({field: "Build", row: index + 2, value:"'" + build + "' => Build ID Not Exist"})
      }

      allAccountsData.forEach((account: any) => {
        if(email === account.email) {
          errorData.push({field: "Email Account", row: index + 2, value:"'" + email + "' => Existed on System"})
        }
        if(wallet === account.wallet) {
          errorData.push({field: "Account Wallet", row: index + 2, value:"'" + wallet + "' => Existed on System"})
        }
      })
    }
  })


  const onClickImport = () => {
    if(selectedGuild === -1) {
      openNotification("Please Select guild before doing Import!", "", "error")
    } else if(errorData.length > 0 || schemaErorrs.length > 0) {
      setVisible(false)
      setErrorPopVisible(true)
    } else {
      const activeStatus = queryResult.data?.data.find(item => item.statusCode === STATUS_ENUM.ACTIVE)
      const dataToImport = excelDataAccounts.map((item: any) => {
        //const buildObject = {id: item.build}
        return {...item, guild: {id: selectedGuild}, build: {id: item.build}, status: {id: activeStatus?.id}}
      })
      mutate({
        resource: 'accounts',
        values: dataToImport
      })
      setExcelDataAccounts([])
      setSelectedGuild(-1)
      setSchemaErorrs([])
      setVisible(false)
    }
  }

  const { tableProps, searchFormProps } = useTable<
        IAccount,
        HttpError,
        IAccountFilterVariables
    >({
      onSearch: (params) => {
        const filters: CrudFilters = []
        let {
          email,
          wallet
        } = params
        const {
          status, build, guild, group
        } = params
        if(params.email != undefined) {
          email = encodeURIComponent(params.email)
        }
        if(params.wallet != undefined) {
          wallet = encodeURIComponent(params.wallet)
        }
        filters.push({
          field: "email",
          operator: "contains",
          value: email,
        }, {
          field: "status][statusCode]",
          operator: "eq",
          value: status || "active",
        }, {
          field: "wallet",
          operator: "containss",
          value: wallet,
        }, {
          field: "build",
          operator: "contains",
          value: build,
        }, {
          field: "guild][id]",
          operator: "eq",
          value: guild,
        }, {
          field: "guild][group][id]",
          operator: "eq",
          value: group,
        })
        return filters
      },
      initialFilter: [
        {
          field: "status][statusCode]",
          operator: "eq",
          value: "active",
        }
      ],
      metaData: { populate: ['status', 'guild.group.project', 'scholar.users_permissions_user'] }
    })

  return (

    <Row gutter={[16, 16]}>
      <Col xs={24}>
        <Card title="Account Filter">
          <Filter formProps={searchFormProps} />
        </Card>
      </Col>
      <Col xs={24}>
        <List pageHeaderProps={{extra: <div>
          <div className="ant-upload ant-upload-select ant-upload-select-text">
            <span tabIndex={0}
              className="ant-upload"
              role="button">
              <button disabled={localStorage.getItem(ROLE_KEY) === "guest"} type="button" className="ant-btn ant-btn-default" onClick={() =>{
                allAccounts.refetch().then(values => {
                  setAllAccountsData(values.data?.data)
                })
                setVisible(true)
              } }>
                <span role="img" aria-label="import" className="anticon anticon-import">
                  <svg viewBox="64 64 896 896"
                    focusable="false"
                    data-icon="import" width="1em"
                    height="1em"
                    fill="currentColor"
                    aria-hidden="true">
                    <path d="M888.3 757.4h-53.8c-4.2 0-7.7 3.5-7.7 7.7v61.8H197.1V197.1h629.8v61.8c0 4.2 3.5 7.7 7.7 7.7h53.8c4.2 0 7.7-3.4 7.7-7.7V158.7c0-17-13.7-30.7-30.7-30.7H158.7c-17 0-30.7 13.7-30.7 30.7v706.6c0 17 13.7 30.7 30.7 30.7h706.6c17 0 30.7-13.7 30.7-30.7V765.1c0-4.3-3.5-7.7-7.7-7.7zM902 476H588v-76c0-6.7-7.8-10.5-13-6.3l-141.9 112a8 8 0 000 12.6l141.9 112c5.3 4.2 13 .4 13-6.3v-76h314c4.4 0 8-3.6 8-8v-56c0-4.4-3.6-8-8-8z">
                    </path>
                  </svg>
                </span>
                <span>Import</span>
              </button>
            </span>
          </div>
            &nbsp;
          <CreateButton disabled={localStorage.getItem(ROLE_KEY) === "guest"}/>
        </div>}}>
          <Modal
            title="Import Account"
            centered
            visible={visible}
            destroyOnClose={true}
            okText={"Import"}
            onOk={() => onClickImport()}
            onCancel={() => {
              setExcelDataAccounts([])
              setSelectedGuild(-1)
              setSchemaErorrs([])
              setVisible(false)
            }}
            width={1300}
          >

            <Form  wrapperCol={{ span: 24 }} layout="vertical">
              <Button style={{position: 'absolute',top: '66px', right: '24px', zIndex: 1}} type={"primary"} onClick={() => {
                const a = document.createElement("a")
                a.href = "/files/Import_Account.xlsx"
                a.setAttribute("download", 'Import_account_template.xlsx')
                a.click()
              }}>Download Template</Button>
              <Form.Item rules={[{ required: true }]} label="Select Guild" name="guild"><Select {...guildsProps} onChange={
                (guild:any) => {
                  setSelectedGuild(parseInt(guild))
                }
              }/></Form.Item>
              <Form.Item rules={[{ required: true }]} label="File To Import">
                <Input type="file" accept=".xlsx" disabled={selectedGuild === -1} onChange={(event) => {
                  if (event.target.files) {
                    readXlsxFile(event.target.files[0], { schema }).then((rows) => {
                      setExcelDataAccounts(rows.rows)
                      setSchemaErorrs(rows.errors)
                      buildList.refetch().then(values => {
                        setAllBuildsData(values.data?.data)
                      })
                    })
                  }
                }
                }/>
              </Form.Item>
            </Form>
            <Table columns={accountExcelCol} dataSource={excelDataAccounts} rowKey={"email"}/>
          </Modal>
          <Modal
            title={<h3 style={{color: 'red'}}>Input File Invalid</h3>}
            centered
            visible={errorPopVisible}
            destroyOnClose={true}
            onOk={() =>{
              setExcelDataAccounts([])
              setSchemaErorrs([])
              setSelectedGuild(-1)
              setErrorPopVisible(false)
            }}
            onCancel={() => {
              setExcelDataAccounts([])
              setSchemaErorrs([])
              setSelectedGuild(-1)
              setErrorPopVisible(false)
            }}
            width={900}
          >
            {schemaErorrs.length > 0 ? <Table columns={schemaErrorCol} dataSource={schemaErorrs} rowKey={"row"}/> :  <Table columns={errorCol} dataSource={errorData} rowKey={"row"}/>}

            <h3 style={{color: 'red', textAlign: 'center'}}>Please correct file data before doing Import!</h3>
          </Modal>
          <Table {...tableProps} rowKey="id">
            <Table.Column dataIndex={['guild', 'group', 'project', 'name']} title="Game" />
            <Table.Column dataIndex={['guild', 'group', 'groupName']} title="Group" />
            <Table.Column dataIndex={['guild', 'name']} title="Guild" />
            <Table.Column dataIndex="email" title="Email" />
            <Table.Column
              dataIndex="wallet"
              title="Wallet"
              render={val => addressShortener(val)}
            />
            <Table.Column
              dataIndex={['scholar', 'users_permissions_user', 'username']}
              title="Scholar"
            />
            <Table.Column dataIndex={['status', 'title']} title="Status" />
            <Table.Column<IAccount>
              title="Actions"
              dataIndex="actions"
              render={(_, record): React.ReactNode => (
                <Space>
                  <ShowButton
                    disabled={localStorage.getItem(ROLE_KEY) === "guest"}
                    size="small"
                    recordItemId={record.id}
                    hideText
                    onMouseEnter={event => {
                      const myRoleUrl = `${API_URL}/api/sync-today-slp-by-accountID/${record.id}`
                      axiosInstance.post(myRoleUrl).then(_ => console.log("Done!!!" + _.toString()))
                    }}
                  />
                  <EditButton
                    disabled={localStorage.getItem(ROLE_KEY) === "guest"}
                    size="small"
                    recordItemId={record.id}
                    hideText
                  />
                </Space>
              )}
            />
          </Table>
        </List>
      </Col>
    </Row>
  )
}
