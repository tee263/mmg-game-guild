import React from "react"
import {Column} from '@ant-design/plots'
import {Typography} from "@pankod/refine-antd"
import {getDate, getDateNow} from "utility/datetime"
import {ISlp} from "../../../interfaces/slp"
import {getYYYYMMDDFrom, getListOfDateInRange} from "../../../utility/datetime"

const {Title} = Typography

interface ChartProps {
  data?: ISlp[];
}

export const PlotChart: React.FC<ChartProps> = ({data}) => {
  const mapsData = new Map([])

  const dateKeys = getListOfDateInRange(new Date().setDate(getDateNow().getDate() - 29), getDateNow())
  data?.map(item => {
    mapsData.set(getYYYYMMDDFrom(new Date(Date.parse(item.issuedDate.toString()))), item.todaySoFar || 0)
  })

  const _data = dateKeys?.map(key => ({
    day: getDate(key),
    value: mapsData.get(key) || 0
  })) ?? []

  const config = {
    data: _data,
    xField: 'day',
    yField: 'value',
    seriesField: '',
    xAxis: {
      label: {
        autoHide: true,
        autoRotate: true,
      },
    },
  }
  return (
    <div>
      <Title level={3} style={{color: 'white'}}>Daily Earning</Title>
      <Column {...config} />
    </div>
  )
}