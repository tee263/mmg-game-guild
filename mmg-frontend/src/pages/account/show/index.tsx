import {
  useList,
  useShow,
} from "@pankod/refine-core"
import { IAccount } from "interfaces"
import { ISlp } from "interfaces/slp"
import React, {useState} from "react"
import {RenderAccount} from "./render"
import {ILeaderboard} from "../../../interfaces/leaderboard"
import {getDateNow} from "../../../utility/datetime"

export const ShowAccount: React.FC = () => {
  const [startDate] = useState(new Date().setDate(getDateNow().getDate() - 30))
  const [issuedDate] = useState(new Date().getTime())
  
  const { queryResult } = useShow<IAccount>({
    resource: 'accounts',
    metaData: { populate: '*' }
  })
  const { data, isLoading } = queryResult
  const record = data?.data

  const earningResults = useList<ISlp>({
    resource: 'slps',
    config: {
      sort: [
        {
          field: 'createdAt',
          order: 'asc'
        }
      ],
      filters: [
        {
          field: 'accountId',
          operator: 'eq',
          value: record?.id
        },
        {
          field: 'issuedDate',
          operator: 'gte',
          value: startDate.valueOf()
        },
        {
          field: 'issuedDate',
          operator: 'lte',
          value: issuedDate
        }
      ],
      pagination: { pageSize: 100 }
    }
  })

  const leaderboardResults = useList<ILeaderboard>({
    resource: 'leaderboards',
    config: {
      sort: [
        {
          field: 'createdAt',
          order: 'desc'
        }
      ],
      filters: [
        {
          field: 'accountId',
          operator: 'eq',
          value: queryResult.data?.data.id
        }
      ],
      pagination: { pageSize: 30 }
    }
  })

  return (
    <RenderAccount
      isLoading={isLoading}
      accountData={record}
      chartData={earningResults.data?.data}
      leaderboardResults={leaderboardResults.data?.data}
    />
  )
}
