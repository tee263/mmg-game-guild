import React from "react"
import {Card, Col, EditButton, ListButton, RefreshButton, Row, Show, Typography} from "@pankod/refine-antd"
import _get from "lodash/get"
import {PlotChart} from "./earning-tracker"
import {ISlp} from "../../../interfaces/slp"
import {ILeaderboard} from "../../../interfaces/leaderboard"
import {useCustom} from "@pankod/refine-core"
import {API_URL} from "../../../utility/DataMappingUtils"
import {openNotification} from "../../../utility/NotificationUtils"

const {Title, Text} = Typography

type RenderAccountProps = {
    isLoading: boolean;
    accountData: any;
    chartData?: ISlp[];
    leaderboardResults?: ILeaderboard[]
}
const ROLE_KEY = "role"

export const RenderAccount: React.FC<RenderAccountProps> = (props) => {
  const {isLoading, accountData, chartData} = props
  const earningData = chartData?.find(i => new Date(Date.parse(i.issuedDate.toString())).getDate() === new Date().getDate())
  const TEXT_COLOR = 'white'
  const BG_COLOR = '#283646'
  const accountId = accountData?.id || -1

  const syncAccount = useCustom<any>({
    url: `${API_URL}/api/sync-slp-by-accountID/${accountId}`,
    method: "get",
    queryOptions: {enabled: false}
  })

  return (
    <Show isLoading={isLoading} pageHeaderProps={{
      extra:
                <div>
                  <ListButton/>
                    &nbsp;
                  <EditButton/>
                    &nbsp;
                  <RefreshButton contentEditable={true} onClick={(e) => {
                    syncAccount.refetch().then(value => {
                      if (!value.error) {
                        openNotification("Successfully!", "Please refresh the page to see the newest Data!", "success")
                      }
                    }
                    )
                  }
                  }>Sync SLP </RefreshButton>
                </div>
    }}>
      <Row gutter={[16, 14]}>
        <Col span={6}>
          <Card bordered={false} style={{backgroundColor: BG_COLOR}}>
            <Title level={5} style={{color: TEXT_COLOR}}>Email:</Title>
            <Text style={{color: TEXT_COLOR}}>{accountData?.email || "-"}</Text>

            <Title level={5} style={{color: TEXT_COLOR}}>Wallet:</Title>
            <Text style={{color: TEXT_COLOR}}>{accountData?.wallet || "-"}</Text>

            <Title level={5} style={{color: TEXT_COLOR}}>Build:</Title>
            <Text style={{color: TEXT_COLOR}}>{accountData?.build?.name || "-"}</Text>

            <Title level={5} style={{color: TEXT_COLOR}}>Game:</Title>
            <Text
              style={{color: TEXT_COLOR}}>{(accountData?.game as any)?.data?.attributes?.title || "-"}</Text>
          </Card>
        </Col>
        <Col span={18}>
          <Row gutter={[16, 14]}>
            <Col span={12}>
              <Card bordered={false} style={{backgroundColor: BG_COLOR, color: TEXT_COLOR}}>
                <div style={{marginBottom: 10}}>Unclaimed Amount</div>
                <strong>{_get(earningData, 'claimableTotal', 0)}</strong>
              </Card>
            </Col>
            <Col span={12}>
              <Card bordered={false} style={{backgroundColor: BG_COLOR, color: TEXT_COLOR}}>
                <div style={{marginBottom: 10}}>Raw Total</div>
                <strong>{_get(earningData, 'rawTotal', 0)}</strong>
              </Card>
            </Col>
            <Col span={12}>
              <Card bordered={false} style={{backgroundColor: BG_COLOR, color: TEXT_COLOR}}>
                <div style={{marginBottom: 10}}>Total</div>
                <strong>{_get(earningData, 'total', 0)}</strong>
              </Card>
            </Col>
            <Col span={12}>
              <Card bordered={false} style={{backgroundColor: BG_COLOR, color: TEXT_COLOR}}>
                <div style={{marginBottom: 10}}>Average</div>
                <strong>{_get(earningData, 'average', 0)}</strong>
              </Card>
            </Col>
          </Row>
        </Col>
        <Col span={24}>
          <Card bordered={false} style={{backgroundColor: BG_COLOR, color: TEXT_COLOR}}>
            <PlotChart
              data={chartData}
            />
          </Card>
        </Col>
        {/*<Col span={12}>*/}
        {/*  <Card bordered={false} style={{backgroundColor: BG_COLOR, color: TEXT_COLOR}}>*/}
        {/*    <RankingChart*/}
        {/*      data={leaderboardResults}*/}
        {/*    />*/}
        {/*  </Card>*/}
        {/*</Col>*/}
      </Row>
    </Show>
  )
}