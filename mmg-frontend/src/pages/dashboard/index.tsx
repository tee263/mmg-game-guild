import React from "react"
import { Row, Col, Card } from "@pankod/refine-antd"
import { ITask, ILabel, IPriority, IStatus, IAuthUser } from "interfaces"
import { TaskChart } from "components/task/pie"
import { groupBy } from "helper"
import { useList, useMany } from "@pankod/refine-core"

export const Dashboard = () => {
  const taskList: {data: ITask[]} = {data: []}
  // const taskList = useList<ITask>({
  //   resource: "tasks",
  // })

  const labelIds = taskList.data?.map((item:any) => item.label) ?? []
  const priorityIds = taskList.data?.map((item:any) => item.priority) ?? []
  const assignedIds = taskList.data?.map((item:any) => item.users) ?? []
  const statusIds = taskList.data?.map((item:any) => item.status) ?? []

  // const { data: labels } = useMany<ILabel>({
  //   resource: "label",
  //   ids: labelIds || [],
  // })
  //
  // const { data: priority } = useMany<IPriority>({
  //   resource: "priority",
  //   ids: priorityIds || [],
  // })
  //
  // const { data: assigned } = useMany<IAuthUser>({
  //   resource: "users",
  //   ids: assignedIds || [],
  // })
  //
  // const { data: status } = useMany<IStatus>({
  //   resource: "status",
  //   ids: statusIds || [],
  // })

  const labels: {data: ILabel[]} = {data: []}
  const priority: {data: IPriority[]} = {data: []}
  const status: {data: IStatus[]} = {data: []}
  const assigned: {data: IAuthUser[]} = {data: []}

  return (
    <Row gutter={[16, 16]}>
      <Col xl={8} lg={12} md={24} sm={24} xs={24}>
        <Card>
          <TaskChart
            data={
              labels?.data.map((i:any) => {
                return {
                  type: i.title,
                  value: groupBy(labelIds)[i.id],
                }
              }) ?? []
            }
          />
        </Card>
      </Col>
      <Col xl={8} lg={12} md={24} sm={24} xs={24}>
        <Card>
          <TaskChart
            data={
              priority?.data.map((i:any) => {
                return {
                  type: i.title,
                  value: groupBy(priorityIds)[i.id],
                }
              }) ?? []
            }
          />
        </Card>
      </Col>
      <Col xl={8} lg={12} md={24} sm={24} xs={24}>
        <Card>
          <TaskChart
            data={
              status?.data.map((i:any) => {
                return {
                  type: i.title,
                  value: groupBy(statusIds)[i.id],
                }
              }) ?? []
            }
          />
        </Card>
      </Col>
      <Col xl={8} lg={12} md={24} sm={24} xs={24}>
        <Card>
          <TaskChart
            data={
              assigned?.data.map((i:any) => {
                return {
                  type: i.email,
                  value: groupBy(assignedIds)[i.id],
                }
              }) ?? []
            }
          />
        </Card>
      </Col>
    </Row>
  )
}
