import React from "react"
import {IResourceComponentsProps} from "@pankod/refine-core"
import {IGroup, IProject, IStatus} from "interfaces"
import {Create, Form, Input, Select, useForm, useSelect} from "@pankod/refine-antd"

export const CreateGroup: React.FC<IResourceComponentsProps> = () => {
  const {formProps, saveButtonProps} = useForm<IGroup>()

  const {selectProps: statusSelectProps} = useSelect<IStatus>({
    resource: "statuses",
    optionLabel: "title"
  })

  const {selectProps: projectOptionsProps, queryResult} = useSelect<IProject>({
    resource: "projects",
    optionLabel: "name"
  })

  return (
    <Create saveButtonProps={saveButtonProps}>
      <Form {...formProps} wrapperCol={{span: 12}} layout="vertical">
        <Form.Item
          label="Group Name"
          name="groupName"
          rules={[{required: true}]}
        >
          <Input/>
        </Form.Item>

        <Form.Item label="Select Game" name="project" rules={[{required: true}]}>
          <Select {...projectOptionsProps} onChange={(value => {
            const detail = queryResult.data?.data.find(i => i.id.toString() == value.toString())
            formProps.form?.setFields([
              {
                name: "mainToken",
                value: detail?.mainToken
              },
              {
                name: "subToken",
                value: detail?.subToken
              }
            ])
          })}/>
        </Form.Item>

        <Form.Item label="Select Status" name="status" rules={[{required: true}]}>
          <Select {...statusSelectProps} />
        </Form.Item>

        <Form.Item label="Main Token" name={"mainToken"}>
          <Input disabled={true}/>
        </Form.Item>

        <Form.Item label="Sub Token" name={"subToken"}>
          <Input disabled={true}/>
        </Form.Item>
      </Form>
    </Create>
)
}
