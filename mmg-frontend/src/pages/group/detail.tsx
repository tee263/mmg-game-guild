import React from "react"
import {Show, Typography,} from "@pankod/refine-antd"
import {IGroup} from "interfaces"
import {useShow} from "@pankod/refine-core"

const { Title, Text } = Typography

const ROLE_KEY = "role"

export const DetailGroup: React.FC = () => {
  const { queryResult } = useShow<IGroup>({
    metaData: { populate: '*' }
  })
  const { data, isLoading } = queryResult
  const record = data?.data

  return (
    <Show isLoading={isLoading} canEdit={localStorage.getItem(ROLE_KEY) === "manager"}>
      <Title level={5}>Name:</Title>
      <Text>{record?.groupName || "-"}</Text>

      <Title level={5}>Main Token:</Title>
      <Text>{record?.project.mainToken || "-"}</Text>

      <Title level={5}>Sub Token:</Title>
      <Text>{record?.project.subToken || "-"}</Text>

      <Title level={5}>Game:</Title>
      <Text>{record?.project.name || "-"}</Text>

      <Title level={5}>Publish Time:</Title>
      <Text>{record?.publishedAt || "-"}</Text>

      <Title level={5}>Status:</Title>
      <Text>{record?.status?.title ?? "-"}</Text>
    </Show>
  )
}
