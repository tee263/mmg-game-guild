import React from "react"
import {Edit, Form, Input, Select, useForm, useSelect,} from "@pankod/refine-antd"
import {IGroup, IStatus} from "interfaces"
import {IResourceComponentsProps} from "@pankod/refine-core"

export const EditGroup: React.FC<IResourceComponentsProps> = () => {
  const { formProps, saveButtonProps } = useForm<IGroup>({
    metaData: { populate: '*' }
  })

  const { selectProps: statusProps } = useSelect<IStatus>({
    resource: "statuses",
  })

  return (
    <Edit saveButtonProps={saveButtonProps}>
      <Form {...formProps} wrapperCol={{ span: 12 }} layout="vertical">
        <Form.Item label="Group Name" name="groupName">
          <Input />
        </Form.Item>

        <Form.Item
          label="Status"
          name={["status", "id"]}
        >
          <Select {...statusProps} />
        </Form.Item>

        <Form.Item label="Project" name={"project"} style={{display: 'none'}}>
          <Input disabled={true}/>
        </Form.Item>

        <Form.Item label="Main Token" name={["project", "mainToken"]}>
          <Input disabled={true}/>
        </Form.Item>

        <Form.Item label="Sub Token" name={["project", "subToken"]}>
          <Input disabled={true}/>
        </Form.Item>

        <Form.Item label="Game" name={["project", "name"]}>
          <Input disabled={true}/>
        </Form.Item>

      </Form>
    </Edit>
  )
}
