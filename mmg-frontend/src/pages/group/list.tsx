import React from "react"
import {Col, EditButton, List, Row, Space, Table, useTable,} from "@pankod/refine-antd"
import {IGameFilterVariables, IGroup,} from "interfaces"
import {CrudFilters, HttpError, IResourceComponentsProps} from "@pankod/refine-core"

const ROLE_KEY = "role"

export const ListGroup: React.FC<IResourceComponentsProps> = () => {
  const {tableProps, tableQueryResult} = useTable<IGroup,
    HttpError,
    IGameFilterVariables>({
      onSearch: (params) => {
        const filters: CrudFilters = []
        const {
          groupName,
        } = params

        filters.push(
          {
            field: "groupName",
            operator: "eq",
            value: groupName,
          }
        )
        return filters
      },
      metaData: {
        populate: '*'
      },
      initialSorter: [
        {
          field: "id",
          order: "asc",
        },
      ],
    })

  return (
    <Row gutter={[16, 16]}>
      <Col lg={24} xs={24}>
        <List canCreate={localStorage.getItem(ROLE_KEY) === "manager"}>
          <Table {...tableProps} rowKey="id">
            <Table.Column dataIndex="id" title="Group Name"
              render={val => <a
                href={`/groups/show/${val}`}>{tableQueryResult.data?.data.find(_ => _.id === val)?.groupName}</a>}
            />
            <Table.Column dataIndex={["project", "name"]} title="Game"/>
            <Table.Column dataIndex={["project", "mainToken"]} title="Main Token"/>
            <Table.Column dataIndex={["project", "subToken"]} title="Sub Token"/>
            <Table.Column dataIndex={["status", "title"]} title="Status"/>
            <Table.Column<IGroup>
              title="Actions"
              dataIndex="actions"
              render={(_, record): React.ReactNode => (
                <Space>
                  <EditButton
                    disabled={localStorage.getItem(ROLE_KEY) === "guest"}
                    size="small"
                    recordItemId={record.id}
                    hideText
                  />
                </Space>
              )}
            />
          </Table>
        </List>
      </Col>
    </Row>
  )
}
