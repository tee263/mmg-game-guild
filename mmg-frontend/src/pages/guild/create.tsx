import {Create, Form, Input, Select, useForm, useSelect,} from "@pankod/refine-antd"

import {IGroup, IGuild} from "interfaces"
import {IResourceComponentsProps} from "@pankod/refine-core"

export const CreateGuild: React.FC<IResourceComponentsProps> = () => {
  const { formProps, saveButtonProps } = useForm<IGuild>({
    resource: "guilds",
  })

  const { selectProps: groupSelectProps } = useSelect<IGroup>({
    resource: "groups",
    optionLabel: "groupName"
  })

  return (
    <Create saveButtonProps={saveButtonProps}>
      <Form {...formProps} wrapperCol={{ span: 12 }} layout="vertical">
        <Form.Item
          label="Name"
          name="name"
          rules={[
            {
              required: true,
            },
          ]}
        >
          <Input />
        </Form.Item>
                
        <Form.Item 
          label="Select Group"
          name="group"
          rules={[
            {
              required: true,
            },
          ]}>
          <Select {...groupSelectProps} />
        </Form.Item>
               
      </Form>
    </Create>
  )
}
