import {Show, Typography,} from "@pankod/refine-antd"
import {IGuild} from "interfaces"
import {useShow} from "@pankod/refine-core"

const { Title, Text } = Typography

export const DetailGuild: React.FC = () => {
  const { queryResult } = useShow<IGuild>({
    metaData: { populate: ['game'] }
  })
  const { data, isLoading } = queryResult
  const record = data?.data

  return (
    <Show isLoading={isLoading}>
      <Title level={5}>Guild:</Title>
      <Text>{record?.name || "-"}</Text>

      <Title level={5}>Game:</Title>
      <Text>{record?.group?.groupName ?? "-"}</Text>
    </Show>
  )
}
