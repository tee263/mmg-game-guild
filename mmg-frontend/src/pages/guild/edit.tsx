import React from "react"
import {Edit, Form, Input, Select, useForm, useSelect,} from "@pankod/refine-antd"
import {IGroup, IGuild} from "interfaces"
import {IResourceComponentsProps} from "@pankod/refine-core"

export const EditGuild: React.FC<IResourceComponentsProps> = () => {
  const { formProps, saveButtonProps, queryResult } = useForm<IGuild>({
    metaData: { populate: ['group'] }
  })
  const { selectProps: groupProps } = useSelect<IGroup>({
    resource: "groups",
    optionLabel: "groupName",
    defaultValue: queryResult?.data?.data.group?.id,
  })

  return (
    <Edit saveButtonProps={saveButtonProps}>
      <Form {...formProps} wrapperCol={{ span: 12 }} layout="vertical">
        <Form.Item label="Guild Name" name="name">
          <Input />
        </Form.Item>

        <Form.Item
          wrapperCol={{ span: 12}}
          label="Group"
          name={["group", "id"]}
        >
          <Select {...groupProps} />
        </Form.Item>
      </Form>
    </Edit>
  )
}
