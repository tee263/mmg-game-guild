import React from "react"
import {Button, Col, Form, FormProps, Row, Select, useSelect,} from "@pankod/refine-antd"
import {IGroup} from "../../interfaces"

export const Filter: React.FC<{ formProps: FormProps }> = ({ formProps }) => {

  const { selectProps: gameProps } = useSelect<IGroup>({
    resource: "groups",
    optionLabel: 'groupName',
  })

  return (
    <Form {...formProps}>
      <Row gutter={16}>
        <Col lg={8} xs={24}>
          <Form.Item label="Group" name="group">
            <Select
              mode='multiple'
              placeholder='Group'
              {...gameProps}
            />
          </Form.Item>
        </Col>
        <Col lg={4} xs={24}>
          <Form.Item>
            <Button htmlType="submit" type="primary">
              Filter
            </Button>
          </Form.Item>
        </Col>
      </Row>
    </Form>
  )
}
