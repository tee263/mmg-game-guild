import React from "react"
import {Col, EditButton, List, Row, ShowButton, Space, Table, useTable,} from "@pankod/refine-antd"

import {IGuild,} from "interfaces"
import {CrudFilters, HttpError, IResourceComponentsProps} from "@pankod/refine-core"
import {Filter} from "./filter"

const ROLE_KEY = "role"

export const ListGuild: React.FC<IResourceComponentsProps> = () => {
  const { tableProps, searchFormProps } = useTable<
      IGuild,
      HttpError
    >({
      onSearch: (params: any) => {
        const { group } = params
        const filters: CrudFilters = []
        filters.push(
          {
            field: "[group][id]",
            operator: "in",
            value: group,
          }
        )
        return filters
      },
      metaData: { populate: ['group', 'guild_leader.users_permissions_user'] }
    })

  return (
    <Row gutter={[16, 16]}>
      <Col xs={24}>
        <Filter formProps={searchFormProps} />
      </Col>
      <Col lg={24} xs={24}>
        <List canCreate={localStorage.getItem(ROLE_KEY) === "manager"}>
          <Table {...tableProps} rowKey="id">
            <Table.Column dataIndex="name" title="Name" />
            <Table.Column
              dataIndex={["group", "groupName"]}
              title="Group Name"
            />
            <Table.Column
              dataIndex='guild_leader'
              title="Leader"
              render={val => <a href={`/guild-leaders/show/${val?.id}`}>{val?.name}</a>}
            />

            <Table.Column
              dataIndex={['guild_leader', 'users_permissions_user', 'email']}
              title="Leader Email"
            />
            <Table.Column<IGuild>
              dataIndex="actions"
              align="right"
              render={(_:any, record:any): React.ReactNode => {
                return (
                  <Space>
                    <ShowButton
                      disabled={localStorage.getItem(ROLE_KEY) === "guest"}
                      size="middle"
                      recordItemId={record.id}
                      hideText
                    />
                    <EditButton
                      disabled={localStorage.getItem(ROLE_KEY) === "guest"}
                      size="middle"
                      recordItemId={record.id}
                      hideText
                    />
                  </Space>
                )
              }}
            />
          </Table>
        </List>
      </Col>
            
    </Row>
  )
}
