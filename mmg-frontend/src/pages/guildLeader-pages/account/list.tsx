import React from "react"
import {Card, Col, EditButton, List, Row, ShowButton, Space, Table, useTable} from "@pankod/refine-antd"
import {IAccount, IAccountFilterVariables,} from "interfaces"
import {Filter} from "./filter"
import {CrudFilters, HttpError, IResourceComponentsProps} from "@pankod/refine-core"
import {addressShortener} from "../../../utility"
import {API_URL} from "../../../utility/DataMappingUtils"
import {axiosInstance} from "../../../authProvider"

export const ListAccount: React.FC<IResourceComponentsProps> = () => {
  const { tableProps, searchFormProps } = useTable<
        IAccount,
        HttpError,
        IAccountFilterVariables
    >({
      onSearch: (params:any) => {
        const filters: CrudFilters = []
        const {
          email,
          wallet,
          status,
        } = params

        filters.push({
          field: "email",
          operator: "contains",
          value: email,
        }, {
          field: "wallet",
          operator: "contains",
          value: wallet,
        }, {
          field: "status][statusCode]",
          operator: "eq",
          value: status || "active",
        })
        return filters
      },
      initialFilter: [
        {
          field: "status][statusCode]",
          operator: "eq",
          value: "active",
        }
      ],
      metaData: { populate: ['status', 'guild.group.project', 'scholar', 'build'] }
    })

  return (
    <Row gutter={[16, 16]}>
      <Col xs={24}>
        <Card title="Account Filter">
          <Filter formProps={searchFormProps} />
        </Card>
      </Col>
      <Col xs={24}>
        <List>
          <Table {...tableProps} rowKey="id">
            <Table.Column dataIndex={['guild', 'group', 'project', 'name']} title="Game" />
            <Table.Column dataIndex={['guild', 'group', 'groupName']} title="Group" />
            <Table.Column dataIndex={['guild', 'name']} title="Guild" />
            <Table.Column dataIndex="email" title="Email" />
            <Table.Column
              dataIndex="wallet"
              title="Wallet"
              render={val => addressShortener(val)}
            />
            <Table.Column dataIndex={['build', 'name']} title="Build" />
            <Table.Column dataIndex={['guild', 'name']} title="Guild" />
            <Table.Column dataIndex={['status', 'title']} title="Status" />
            <Table.Column<IAccount>
              title="Actions"
              dataIndex="actions"
              render={(_:any, record:any): React.ReactNode => (
                <Space>
                  <ShowButton
                    recordItemId={record.id}
                    hideText
                    onMouseEnter={event => {
                      const myRoleUrl = `${API_URL}/api/sync-today-slp-by-accountID/${record.id}`
                      axiosInstance.post(myRoleUrl).then(_ => console.log("Done!!!" + _.toString()))
                    }}
                  />
                  <EditButton
                    recordItemId={record.id}
                    hideText
                  />
                </Space>
              )}
            />
          </Table>
        </List>
      </Col>
    </Row>
  )
}
