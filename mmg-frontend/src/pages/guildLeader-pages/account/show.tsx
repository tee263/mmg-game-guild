import { IAccount } from "interfaces"
import {useList, useShow} from "@pankod/refine-core"
import {RenderAccount} from "../../account/show/render"
import React, {useState} from "react"
import { getDateNow } from "../../../utility/datetime"
import {ISlp} from "../../../interfaces/slp"

export const ShowAccount: React.FC = () => {
  const [startDate] = useState(new Date().setDate(getDateNow().getDate() - 30))
  const [issuedDate] = useState(new Date().getTime())
  const { queryResult } = useShow<IAccount>({
    metaData: { populate: '*' }
  })
  const { data, isLoading } = queryResult
  const record = data?.data

  const currentEarningResult = useList<ISlp>({
    resource: 'slps',
    config: {
      sort: [
        {
          field: 'createdAt',
          order: 'asc'
        }
      ],
      filters: [
        {
          field: 'accountId',
          operator: 'eq',
          value: record?.id
        },
        {
          field: 'issuedDate',
          operator: 'gte',
          value: startDate.valueOf()
        },
        {
          field: 'issuedDate',
          operator: 'lte',
          value: issuedDate
        }
      ],
      pagination: { pageSize: 100 }
    }
  })

  return (
    <RenderAccount
      isLoading={isLoading}
      accountData={record}
      chartData={currentEarningResult.data?.data}
    />
  )
}
