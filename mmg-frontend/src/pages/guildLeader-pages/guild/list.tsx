import React from "react"
import {Col, List, Row, Table, useTable,} from "@pankod/refine-antd"

import {IGuild,} from "interfaces"
import {CrudFilters, HttpError, IResourceComponentsProps} from "@pankod/refine-core"

export const ListGuild: React.FC<IResourceComponentsProps> = () => {
  const { tableProps } = useTable<
      IGuild,
      HttpError
    >({
      onSearch: () => {
        const filters: CrudFilters = []
        filters.push(
          {
            field: "populate",
            operator: "eq",
            value: true,
          }
        )
        return filters
      },
      metaData: {populate: ['group', 'guild_leader.users_permissions_user']}
    })

  return (
    <Row gutter={[16, 16]}>
      <Col lg={24} xs={24}>
        <List>
          <Table {...tableProps} rowKey="id">
            <Table.Column dataIndex="name" title="Name" />
            <Table.Column
              dataIndex={["group", "groupName"]}
              title="Group"
            />
            <Table.Column
              dataIndex='guild_leader'
              title="Leader"
              render={val => <span>{val?.name}</span>}
            />
            <Table.Column
              dataIndex={['guild_leader', 'users_permissions_user', 'email']}
              title="Leader Email"
            />
          </Table>
        </List>
      </Col>
    </Row>
  )
}
