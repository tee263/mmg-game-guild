import React from "react"
import {Button, Col, Form, FormProps, Row, Select, useSelect,} from "@pankod/refine-antd"
import {IPayout} from "../../../interfaces/payout"

export const Filter: React.FC<{ formProps: FormProps }> = ({ formProps }) => {

  const { selectProps } = useSelect<IPayout>({
    resource: "payouts",
    optionLabel: 'name',
  })

  return (
    <Form {...formProps}>
      <Row gutter={16}>
        <Col lg={8} xs={24}>
          <Form.Item label="Payouts" name="payout">
            <Select
              mode='multiple'
              placeholder='Payout'
              {...selectProps}
            />
          </Form.Item>
        </Col>
        <Col lg={4} xs={24}>
          <Form.Item>
            <Button htmlType="submit" type="primary">
              Filter
            </Button>
          </Form.Item>
        </Col>
      </Row>
    </Form>
  )
}
