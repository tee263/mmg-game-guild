import {
  Show,
  Typography,
} from "@pankod/refine-antd"
import {IScholar} from "../../../interfaces/scholar"
import {useShow} from "@pankod/refine-core"
const { Title, Text } = Typography

export const DetailScholar: React.FC = () => {
  const { queryResult } = useShow<IScholar>({
    metaData: { populate: '*' }
  })
  const { data, isLoading } = queryResult
  const record = data?.data

  return (
    <Show isLoading={isLoading}>
      <Title level={5}>Identify ID:</Title>
      <Text>{record?.identifyId || "-"}</Text>
      <Title level={5}>Accounts:</Title>
      <Text>{record?.accounts.map(_ => _.wallet).join(', ') || "-"}</Text>
      <Title level={5}>Personal Wallet:</Title>
      <Text>{record?.personalWallet || "-"}</Text>
      <Title level={5}>Phone Number:</Title>
      <Text>{record?.phoneNumber || "-"}</Text>
      <Title level={5}>Created At:</Title>
      <Text>{record?.createdAt || "-"}</Text>
    </Show>
  )
}
