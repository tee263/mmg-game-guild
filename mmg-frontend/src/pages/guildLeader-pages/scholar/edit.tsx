import {Edit, Form, Input, useForm, useSelect} from "@pankod/refine-antd"
import {IResourceComponentsProps} from "@pankod/refine-core"

import {IScholar} from "interfaces/scholar"
import {Select} from 'antd'
import {IGuild, IStatus} from "../../../interfaces"

export const EditScholar: React.FC<IResourceComponentsProps> = () => {
  const { formProps, saveButtonProps, queryResult} = useForm<IScholar>({
    metaData: { populate: ['accounts.guild.group', 'users_permissions_user', 'status', 'guild'] }
  })

  const {selectProps: statusProps} = useSelect<IStatus>({
    resource: "statuses",
  })

  const { selectProps: guildsProps } = useSelect<IGuild>({
    resource: "guilds-by-leader",
    optionLabel: "name"
  })

  return (
    <Edit saveButtonProps={saveButtonProps}>
      <Form {...formProps} wrapperCol={{ span: 12 }} layout="vertical">
        <Form.Item rules={[{ required: true }]} label="Identify ID" name="identifyId"><Input /></Form.Item>
        <Form.Item rules={[{ required: true }]} label="Personal Wallet" name="personalWallet"><Input /></Form.Item>
        <Form.Item rules={[{ required: true }]} label="Phone Number" name="phoneNumber"><Input /></Form.Item>
        <Form.Item rules={[{ required: true }]} label="Status" name={["status", "id"]}><Select placeholder='Status'{...statusProps}/></Form.Item>
        <Form.Item rules={[{ required: true }]} label="Guild" name={["guild", "id"]}><Select placeholder='Guild'{...guildsProps}/></Form.Item>
      </Form>
    </Edit>
  )
}
