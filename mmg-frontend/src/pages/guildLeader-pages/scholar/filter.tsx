import React from "react"
import {
  Form,
  FormProps,
  Input,
  Icons,
  Button, useSelect, Select, Col, Row,
} from "@pankod/refine-antd"
import {IStatus} from "../../../interfaces"

export const Filter: React.FC<{ formProps: FormProps }> = ({ formProps }) => {

  const {selectProps: statusProps} = useSelect<IStatus>({
    resource: "statuses",
    optionValue: "statusCode"
  })

  return (
    <Form layout="vertical" {...formProps}>
      <Row gutter={16}>
        <Col lg={8} xs={24}>
          <Form.Item label="Personal Wallet" name="personalWallet">
            <Input placeholder="Personal Wallet" prefix={<Icons.SearchOutlined />} />
          </Form.Item>
        </Col>
        <Col lg={8} xs={24}>
          <Form.Item label="User Name" name="username">
            <Input placeholder="User Name" prefix={<Icons.SearchOutlined />} />
          </Form.Item>
        </Col>
        <Col lg={8} xs={24}>
          <Form.Item label="Status" name="status">
            <Select
              placeholder='Status'
              {...statusProps}
            />
          </Form.Item>
        </Col>
        <Col lg={4} xs={24}>
          <Form.Item>
            <Button htmlType="submit" type="primary">
            Filter
            </Button>
          </Form.Item>
        </Col>
      </Row>
    </Form>
  )
}
