import React, {useEffect} from "react"
import {
  Row,
  Col,
  useSelect,
  Select,
  Form, Button, DatePicker
} from "@pankod/refine-antd"
import './styles.css'
import {FormProps} from "antd"
import { IGuild } from "interfaces"
import dayjs from "dayjs"

interface FilterProps {
  formProps: FormProps<any>;
}

export const Filter: React.FC<FilterProps> = ({ formProps }) => {

  const { selectProps: guildProps } = useSelect<IGuild>({
    resource: "guilds-by-leader",
    optionLabel: 'name',
    metaData: {
      populate: ['users_permissions_user']
    }
  })

  return (
    <Form {...formProps}>
      <Row gutter={16}>
        <Col lg={6} xs={24}>
          <Form.Item
            label="Period Start"
            name="periodStart"
            rules={[{ required: true }]}
            getValueProps={(value) => ({
              value: value ? dayjs(value) : "",
            })}
          >
            <DatePicker />
          </Form.Item>
        </Col>
        <Col lg={6} xs={24}>
          <Form.Item
            label="Period End"
            name="periodEnd"
            dependencies={['periodStart']} 
            rules={[
              { required: true }, 
              ({ getFieldValue }) => ({
                validator(_, value) {
                  const periodStart = getFieldValue('periodStart')
                  const diff = dayjs(value).diff(dayjs(periodStart), 'day')
                  if (diff >= 3 && diff <= 20) return Promise.resolve()
                  return Promise.reject(new Error('Period time must be between 3 and 20 days'))
                },
              }),
            ]}
            getValueProps={(value) => ({
              value: value ? dayjs(value) : "",
            })}
          >
            <DatePicker />
          </Form.Item>
        </Col>
        <Col lg={8} xs={24}>
          <Form.Item label="Guild" name="guild">
            <Select
              allowClear
              placeholder='Guild name'
              {...guildProps}
            />
          </Form.Item>
        </Col>
        <Col lg={4} xs={24}>
          <Form.Item>
            <Button htmlType="submit" type="primary">
              Filter
            </Button>
          </Form.Item>
        </Col>
      </Row>
    </Form>
  )
}
