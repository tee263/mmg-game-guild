import {Create, Form, Input, Select, useForm, useSelect,} from "@pankod/refine-antd"

import {IGame, IGroup, IGuild} from "interfaces"
import {CrudFilter, IResourceComponentsProps} from "@pankod/refine-core"
import {useState} from "react"

export const CreateGuildLeader: React.FC<IResourceComponentsProps> = () => {
  const { formProps, saveButtonProps } = useForm<IGuild>()
  const [selectedGame, setSelectedGame] = useState(null)

  const { selectProps: gameSelectProps } = useSelect<IGroup>({
    resource: "groups",
  })

  const { selectProps: guildSelectProps } = useSelect<IGuild>({
    resource: "guilds",
    optionLabel: 'name',
    filters: [
      {
        field: 'guild_leader][name]',
        operator: 'null',
        value: true
      }
    ],
    metaData: {
      populate: ['users_permissions_user']
    }
  })

  return (
    <Create saveButtonProps={saveButtonProps}>
      <Form {...formProps} wrapperCol={{ span: 12 }} layout="vertical">
        <Form.Item label="Email" name="email" rules={[{ required: true }]}>
          <Input />
        </Form.Item>
        <Form.Item label="Password" name="password" rules={[{ required: true, message: 'Password is required' }]}>
          <Input.Password />
        </Form.Item>
        <Form.Item
          label="Confirm Password" 
          name="confirmPassword" 
          dependencies={['password']} 
          rules={[
            {
              required: true,
              message: 'Please confirm your password!',
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue('password') === value) return Promise.resolve()
                return Promise.reject(new Error('The two passwords that you entered do not match!'))
              },
            }),
          ]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item label="Name" name="name" rules={[{ required: true }]}>
          <Input />
        </Form.Item>

        <Form.Item
          label="Select Guild"
          name="guilds"
          rules={[
            {
              required: true,
            },
          ]}>
          <Select
            mode="multiple"
            {...guildSelectProps}
          />
        </Form.Item>
      </Form>
    </Create>
  )
}
