import {Show, Typography,} from "@pankod/refine-antd"
import {IGuildLeader} from "interfaces"
import {useShow} from "@pankod/refine-core"

const { Title, Text } = Typography

const ROLE_KEY = "role"

export const DetailGuildLeader: React.FC = () => {
  const { queryResult } = useShow<IGuildLeader>({
    metaData: { populate: '*' }
  })
  const { data, isLoading } = queryResult
  const record = data?.data

  return (
    <Show isLoading={isLoading} canEdit={localStorage.getItem(ROLE_KEY) === "manager"}>
      <Title level={5}>Name:</Title>
      <Text>{record?.name || "-"}</Text>
      <Title level={5}>Guilds:</Title>
      <Text>{record?.guilds?.map(_ => _.name).join(', ') ?? "-"}</Text>
    </Show>
  )
}
