import React, {useEffect, useState} from "react"
import {Edit, Form, Input, Select, useForm, useSelect,} from "@pankod/refine-antd"
import {IGame, IGuild, IGuildLeader} from "interfaces"
import {CrudFilter, IResourceComponentsProps} from "@pankod/refine-core"

export const EditGuildLeader: React.FC<IResourceComponentsProps> = () => {
  const [passwordChange, setPasswordChange] = useState(false)
  const { formProps, saveButtonProps, queryResult } = useForm<IGuildLeader>({
    metaData: { populate: '*' }
  })
  const [selectedGame, setSelectedGame] = useState<number>()

  const guildIds = queryResult?.data?.data.guilds.map((_:IGuild) => _.id) || []

  const defaultFilter: CrudFilter = {
    field: 'guild_leader][name]',
    operator: 'null',
    value: true
  }
  
  const { selectProps: guildProps } = useSelect<IGuild>({
    resource: "guilds",
    optionLabel: "name",
    optionValue: "id",
    defaultValue: guildIds,
    filters: selectedGame ? [
      defaultFilter,
      {
        field: 'group][id]]',
        operator: 'eq',
        value: selectedGame,
      },
    ] : [defaultFilter],
    metaData: {
      populate: ['users_permissions_user']
    }
  })

  useEffect(() => {
    setSelectedGame(queryResult?.data?.data.group?.id)
  }, [queryResult?.data?.data.group?.id])

  return (
    <Edit saveButtonProps={saveButtonProps}>
      <Form {...formProps} wrapperCol={{ span: 12 }} layout="vertical">
        <Form.Item 
          label="Name"
          name="name"
          rules={[
            {
              required: true,
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          wrapperCol={{ span: 12 }}
          label="Guild"
          name="guilds"
          rules={[{ required: true }]}
          getValueProps={(tags?: { id: string }[]) => {
            return ({ value: tags?.map((tag) => tag?.id || tag) })
          }}
        >
          <Select
            mode='multiple'
            {...guildProps}
            options={guildProps.options?.filter((option) => {
              if (selectedGame === queryResult?.data?.data.group?.id) return true
              const value = option.value as string
              return !guildIds.includes(value)
            })}
          />
        </Form.Item>

        <Form.Item
          label="Email"
        >
          <Input readOnly value={queryResult?.data?.data.users_permissions_user?.email} disabled={true}/>
        </Form.Item>
        <Form.Item label="New Password" name="password" help="Leave blank if you don't want to change password">
          <Input.Password onChange={(e) => setPasswordChange(e.target.value?.length > 0)} />
        </Form.Item>
        <Form.Item
          label="Confirm New Password" 
          name="confirmPassword" 
          dependencies={['password']} 
          rules={[
            {
              required: passwordChange,
              message: 'Please confirm your password!',
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue('password') === value) return Promise.resolve()
                return Promise.reject(new Error('The two passwords that you entered do not match!'))
              },
            }),
          ]}
        >
          <Input.Password />
        </Form.Item>
      </Form>
    </Edit>
  )
}
