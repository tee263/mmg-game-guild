import React from "react"
import {Col, DeleteButton, EditButton, List, Row, ShowButton, Space, Table, useTable} from "@pankod/refine-antd"
import {IGuild, IGuildLeader,} from "interfaces"
import {HttpError, IResourceComponentsProps} from "@pankod/refine-core"

const ROLE_KEY = "role"

export const ListGuildLeader: React.FC<IResourceComponentsProps> = () => {
  const { tableProps } = useTable<
        IGuildLeader,
        HttpError
    >(
      {
        initialSorter: [
          {
            field: 'createdAt',
            order: 'desc'
          }
        ],
        metaData: { populate: '*' },
      })

  return (
    <Row gutter={[16, 16]}>
      <Col lg={24} xs={24}>
        <List canCreate={localStorage.getItem(ROLE_KEY) === "manager"}>
          <Table {...tableProps} rowKey="id">
            <Table.Column dataIndex="name" title="Name" />
            <Table.Column
              dataIndex={["guilds"]}
              title="Guild"
              render={val => val?.map((_:any) => _.name).join(', ')}
            />
            <Table.Column
              dataIndex={['users_permissions_user', 'email']}
              title="Leader email"
            />
            <Table.Column
              dataIndex="createdAt"
              title="Created At"
            />
            <Table.Column<IGuild>
              title=""
              dataIndex="actions"
              align="right"
              render={(_:any, record:any): React.ReactNode => {
                return (
                  <Space>
                    <ShowButton
                      disabled={localStorage.getItem(ROLE_KEY) === "guest"}
                      size="middle"
                      recordItemId={record.id}
                      hideText
                    />
                    <EditButton
                      disabled={localStorage.getItem(ROLE_KEY) === "guest"}
                      size="middle"
                      recordItemId={record.id}
                      hideText
                    />
                    <DeleteButton
                      disabled={localStorage.getItem(ROLE_KEY) === "guest"}
                      size="middle"
                      recordItemId={record.id}
                      hideText
                    />
                  </Space>
                )
              }}
            />
          </Table>
        </List>
      </Col>
            
    </Row>
  )
}
