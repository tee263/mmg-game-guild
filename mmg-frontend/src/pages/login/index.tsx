import React from "react"
import {
  Row,
  Col,
  AntdLayout,
  Card,
  Typography,
  Form,
  Input,
  Button,
} from "@pankod/refine-antd"

import "./styles.css"
import {useLogin, useNavigation} from "@pankod/refine-core"

const { Title, Link } = Typography

export interface ILoginForm {
    email: string;
    password: string;
}

export const LoginPage: React.FC = () => {
  const [form] = Form.useForm<ILoginForm>()

  const { mutate: login } = useLogin<ILoginForm>()
  const { push } = useNavigation()

  const CardTitle = (
    <Title level={3} className="title">
        Sign in your account
    </Title>
  )

  return (
    <AntdLayout className="layout">
      <Row
        justify="center"
        align="middle"
        style={{
          height: "100vh",
        }}
      >
        <Col xs={22}>
          <div className="container">
            <div className="imageContainer">
              <img src="/coincu.png" alt="Coincu Logo" style={{width: '100%'}}/>
            </div>
            <Card title={CardTitle} headStyle={{ borderBottom: 0 }}>
              <Form<ILoginForm>
                layout="vertical"
                form={form}
                onFinish={(values:any) => {
                  login(values)
                }}
                initialValues={{
                  email: "",
                  password: "",
                }}
              >
                <Form.Item
                  name="username"
                  label="Email"
                  rules={[{ required: true, type: "email",message:"Please input a valid email!" }]}
                >
                  <Input size="large" placeholder="Email" />
                </Form.Item>
                <Form.Item
                  name="password"
                  label="Password"
                  rules={[{ required: true }]}
                  style={{ marginBottom: "12px" }}
                >
                  <Input
                    type="password"
                    placeholder="password"
                    size="large"
                  />
                </Form.Item>
                <Button
                  type="primary"
                  size="large"
                  htmlType="submit"
                  block
                >
                  Sign in
                </Button>
              </Form>
              <Row style={{ marginTop: 16, fontSize: 12, fontWeight: "bold" }}>
                {/*<Col sm={12}>*/}
                {/*    <Text>Forgot Password</Text>*/}
                {/*</Col>*/}
              </Row>
            </Card>
          </div>
        </Col>
      </Row>
    </AntdLayout>
  )
}
