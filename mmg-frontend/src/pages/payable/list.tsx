import {Col, ExportButton, Form, Input, List, Row} from "@pankod/refine-antd"
import {IResourceComponentsProps, useCreate, useCustom} from "@pankod/refine-core"
import React, {useState} from "react"
import {IPaymentRecord, ITxHashData} from "../../interfaces/payout-account"
import {Button, Card, Descriptions, Modal, Table, Tooltip} from "antd"
import {ExportOutlined, SearchOutlined} from "@ant-design/icons"
import {API_URL, payableCol} from "../../utility/DataMappingUtils"
import {getDate} from "../../utility/datetime"
import {openNotification} from "../../utility/NotificationUtils"
import { CSVLink } from "react-csv"

export const ListPayment: React.FC<IResourceComponentsProps> = () => {

  const [visible, setVisible] = useState(false)
  const [txId, setTxId] = useState("")
  const [txData, setTxData] = useState<any>({})
  const [prepareDataToSave, setPrepareDataToSave] = useState<ITxHashData[]>([])
  const onClickSave = () => {
    if(txId === "" || txData === {}) {
      openNotification("We have nothing to save! Please input your transaction hash and press search button!", "", "error")
    } else {
      mutate({
        resource: "save-transaction",
        values: {
          txHash: txId,
          paymentTransaction: prepareDataToSave
        }
      })

      setTxId("")
      setTxData({})
      setVisible(false)
    }
  }

  const allPaymentsTransaction = useCustom<ITxHashData[]>({
    url: `${API_URL}/api/all-payments-transaction`,
    method: "get"
  })

  const transactionInfo = useCustom<ITxHashData>({
    url: `${API_URL}/api/get-info-by-txHash`,
    method: "post",
    queryOptions: {enabled: false},
    config: {
      payload: {
        txCode: txId
      }
    }
  })
  const {mutate} = useCreate<any>()

  const tableResult = useCustom<IPaymentRecord[]>({
    url: `${API_URL}/api/all-payout-accounts`,
    method: "get"
  })

  const tableDataSource: IPaymentRecord[] | undefined = []
  if(tableResult.isSuccess && tableResult.isFetched) {
    tableResult.data?.data.map(item => {
      const itemIndex = tableDataSource.findIndex(i => i?.account?.scholar?.id === item.account?.scholar?.id)
      if(itemIndex === -1) {
        const groupDataByScholarWallet = allPaymentsTransaction?.data?.data.filter(i => i.to === item.account?.scholar?.personalWallet) || []
        const totalInRange = groupDataByScholarWallet.reduce((num, tx) => {
          const slpTransfer = tx.value
          if (slpTransfer <= 0) return num
          return num + slpTransfer
        }, 0)

        tableDataSource.push(
          {
            account: item?.account,
            id: "unused" + item.id,
            needPayment: item?.amount,
            paid: totalInRange || 0,
            remaining: item?.amount - (totalInRange || 0),
            scholarWallet: item?.account?.scholar?.personalWallet || "-",
            scholarId: item?.account?.scholar?.id || "-",
            scholarStatus: item?.account?.scholar?.status?.statusCode || "-",
            scholarName: item?.account?.scholar?.username || "-",
            amount: item?.amount
          }
        )
      } else {
        tableDataSource[itemIndex].needPayment = tableDataSource[itemIndex].needPayment + item.amount
        tableDataSource[itemIndex].remaining = tableDataSource[itemIndex].needPayment - tableDataSource[itemIndex].paid
        tableDataSource[itemIndex].amount = tableDataSource[itemIndex].amount + item.amount
      }
    })
  }

  const dataForReport = tableDataSource.filter(item => item.scholarStatus === "active").sort((a, b): number => {
    const nameA = a.scholarName ? a.scholarName : ''
    const nameB = b.scholarName ? b.scholarName : ''
    return nameA.localeCompare(nameB)
  })

  return (
    <Row gutter={[16, 16]}>
      <Col lg={24} xs={24}>
        <List pageHeaderProps={{extra: <><Button>
          <CSVLink
            filename={"payable.csv"}
            data={dataForReport.flatMap(item => {
              return {
                account: item?.account?.email,
                needPayment: item?.amount,
                paid: item?.paid || "-",
                remaining: item?.remaining,
                scholarWallet: item?.account?.scholar?.personalWallet || "-",
                scholarId: item?.account?.scholar?.id || "-",
                scholarStatus: item?.account?.scholar?.status?.statusCode || "-",
                scholarName: item?.account?.scholar?.username || "-",
                amount: item?.amount
              } 
            })}
            onClick={() => {
              console.log("exporting!")
            }}
          >
              Export to CSV
          </CSVLink>
        </Button><Button onClick={() => {
          setVisible(true)
        }}>Save Transaction</Button></>}}>
          <Modal
            title="Save Transaction"
            centered
            visible={visible}
            destroyOnClose={true}
            okText={"Save"}
            onOk={() => onClickSave()}
            onCancel={() => {
              setTxId("")
              setTxData({})
              setVisible(false)
            }}
            width={1000}
          >

            <Form  wrapperCol={{ span: 24 }} layout="vertical">
              <Form.Item rules={[{ required: true }]} label="Transaction Input" style={{width: "75%"}}>
                <Input placeholder={"Paste transaction you want to track here!"} onChange={(e) => {setTxId(e.target.value)}}/>
                <Tooltip title="search" >
                  <Button style={{position: 'absolute', marginLeft: "25px"}} type="primary" shape="circle" icon={<SearchOutlined />} onClick={() => {
                    if(txId === "") {
                      openNotification("Please input your payment transaction hash!", "", "error")
                    } else {
                      transactionInfo.refetch().then((value: any)=> {
                        if(value.isSuccess) {
                          setTxData(value?.data)
                          const enCryptLogArr = value?.data?.data?.enCryptLogs || []
                          const values: ITxHashData[] = []
                          enCryptLogArr?.map((i : any) => values.push({
                            status: value?.data?.data?.status,
                            blockNumber: value?.data?.data.block_number,
                            timestamp: value?.data?.data.timestamp * 1000,
                            from: i._from || "-",
                            to: i._to || "-",
                            value: i?._value || 0
                          }))
                          setPrepareDataToSave(values)
                        } else {
                          setTxData({})
                        }

                      })
                    }
                  }}/>
                </Tooltip>
              </Form.Item>
            </Form>
            <Card title="Token Transferred">
              <Descriptions>
                <Descriptions.Item label="Status">{txData?.data?.status === 1 ? "Success" : "Failure"}</Descriptions.Item>
                <Descriptions.Item label="Block Number">{txData?.data?.block_number}</Descriptions.Item>
                <Descriptions.Item label="Timestamp"> {getDate(txData?.data?.timestamp * 1000 || 0)}</Descriptions.Item>
              </Descriptions>
              {
                txData?.data?.enCryptLogs?.map((log:any) => {
                  return (
                    <div style={{display: 'flex'}}>From <a title={log?._from} style={{width: '240px', marginLeft: '8px', marginRight: '8px', textOverflow: 'ellipsis', whiteSpace: 'nowrap', overflow: 'hidden'}}>{log._from}</a> To <a title={log?._to} style={{width: '240px', marginLeft: '8px', marginRight: '8px', textOverflow: 'ellipsis', whiteSpace: 'nowrap', overflow: 'hidden'}}>{log._to}</a> For <b style={{margin: '0px 8px 0px 8px'}}>{log._value}</b> Smooth Love Potion (SLP)</div>
                  )
                })
              }
            </Card>
          </Modal>
          <Table columns={payableCol} dataSource={tableDataSource.filter(item => item.scholarStatus === "active").sort((a, b): number => {
            const nameA = a.scholarName ? a.scholarName : ''
            const nameB = b.scholarName ? b.scholarName : ''
            return nameA.localeCompare(nameB)
          })} rowKey={"id"}/>
        </List>
      </Col>
    </Row>
  )
}
