import {Badge, Col, ExportButton, List, Popover, Row, Table, Tag, Tooltip, useTable} from "@pankod/refine-antd"
import {CrudFilters, HttpError, IResourceComponentsProps, useExport} from "@pankod/refine-core"
import React, {useState} from "react"
import {IPayoutAccount, IPayoutAccountState} from "../../interfaces/payout-account"
import {addressShortener} from "../../utility"
import {getDate} from "../../utility/datetime"
import {InfoCircleOutlined} from "@ant-design/icons"
import {mappedStates} from "../payout/contants"
import { numberWithCommas } from "utility/number"
import {Filter} from "./filter"

const ROLE_KEY = "role"

export const ListPayment: React.FC<IResourceComponentsProps> = () => {

  const [filters, setFilters] = useState<CrudFilters>([])

  const { tableProps, searchFormProps, tableQueryResult } = useTable<
      IPayoutAccount,
      HttpError
    >({
      resource: 'payout-accounts',
      permanentSorter: [
        {
          field: "periodEnd",
          order: "desc",
        },
      ],
      onSearch: (params: any) => {
        const { payout } = params
        const filters: CrudFilters = []
        filters.push(
          {
            field: "[payout][id]",
            operator: "in",
            value: payout,
          }
        )
        setFilters(filters)
        return filters
      },
      metaData: { populate: ['payout', 'account.build', 'account.scholar']}
    })
  const dataItems = tableQueryResult.data?.data || []
  const { triggerExport, isLoading } = useExport<IPayoutAccount>({
    mapData: (item) => {
      return {
        Account: item?.account?.email || '-',
        Wallet: item?.account?.wallet || '-',
        Payout: item?.payout?.name || '-',
        PeriodStart: item?.periodStart || '-',
        PeriodEnd: item?.periodEnd || '-',
        Amount: item?.amount || '-',
        Build: item?.account?.build?.name || '-',
        Status: item.state || '-',
      }
    },
    resourceName: 'payout-accounts',
    sorter:[
      {
        field: "periodEnd",
        order: "desc",
      },
    ],
    filters,
    pageSize: 1000,
    metaData: { populate: ['payout', 'account.build', 'account.scholar']}
  })

  return (
    <Row gutter={[16, 16]}>
      <Col xs={24}>
        <Filter formProps={searchFormProps} />
      </Col>
      <Col lg={24} xs={24}>
        <List pageHeaderProps={{
          extra: (
            <ExportButton disabled={(localStorage.getItem(ROLE_KEY) === 'guest' || !(dataItems.length > 0))} onClick={triggerExport} loading={isLoading} />
          ),
        }}>
          <Table
            {...tableProps}
            rowKey="id"
          >
            <Table.Column
              dataIndex={['account', 'email']}
              title="Account"
              //render={(val, record:IPayoutAccount) => (<a href={`/accounts/show/${record.account.id}`}>{val}</a>)}
            />
            <Table.Column
              dataIndex={['account', 'wallet']}
              title="Wallet"
              render={val => addressShortener(val)}
            />
            <Table.Column
              dataIndex={['payout', 'name']}
              title="Payout"
              render={val => addressShortener(val)}
            />
            <Table.Column
              dataIndex='periodStart'
              title="Period Start"
              render={val => getDate(val)}
            />
            <Table.Column
              dataIndex='periodEnd'
              title="Period End"
              render={val => getDate(val)}
            />
            <Table.Column
              dataIndex='amount'
              title="Amount"
              render={(value, record: any) => (
                <Popover
                  placement="left"
                  content={() => {
                    const kpiRulesRange = record.detail?.kpiRulesRange || []
                    return (<div>
                      {kpiRulesRange.map((item: any, idx: number) => (
                        <div key={idx}>
                          <div><strong>{item.start} - {item.end}</strong></div>
                          <div>Total: {item.totalSlp}, Avg: {item.avgSlp}, Payable: {item.totalSlp}*{item.maxPercent * 100}% = <strong>{item.payable}</strong></div>
                        </div>
                      ))}
                    </div>)
                  }}
                >
                  <Badge count={<InfoCircleOutlined style={{ color: '#108ee9' }} />}>
                    <Tag color="blue" style={{fontSize: 14}}>
                      <strong>
                        {numberWithCommas(value)}
                      </strong>
                    </Tag>
                  </Badge>
                </Popover>
              )}
            />
            <Table.Column
              dataIndex={['account', 'build', 'name']}
              title="Build"
            />
            <Table.Column<IPayoutAccount>
              dataIndex='state'
              title="Status"
              render={(val:IPayoutAccountState, record) => {
                return (
                  <>
                    <span>{mappedStates[val]} </span>
                    {record.scholarNote && (
                      <Tooltip
                        placement="rightTop"
                        title={record.scholarNote}
                      >
                        <InfoCircleOutlined style={{ color: '#108ee9' }} />
                      </Tooltip>
                    )}
                  </>
                )
              }}
            />
            {/*<Table.Column<IPayoutAccount>*/}
            {/*  title="Actions"*/}
            {/*  render={(record): React.ReactNode => (*/}
            {/*    <Space>*/}
            {/*      <Button*/}
            {/*        size="small"*/}
            {/*        onClick={() => {*/}
            {/*          // setPayoutDetail(record.detail)*/}
            {/*          // setAccountDetail(record.account)*/}
            {/*          // show()*/}
            {/*        }}*/}
            {/*      >*/}
            {/*        <EyeOutlined />*/}
            {/*      </Button>*/}
            {/*      <DeleteButton*/}
            {/*        resourceName='payout-accounts'*/}
            {/*        size="small"*/}
            {/*        recordItemId={record.id}*/}
            {/*        hideText*/}
            {/*      />*/}
            {/*    </Space>*/}
            {/*  )}*/}
            {/*/>*/}
          </Table>
        </List>
      </Col>
    </Row>
  )
}
