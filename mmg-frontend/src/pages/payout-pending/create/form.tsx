import React from "react"
import {HttpError, IResourceComponentsProps} from "@pankod/refine-core"
import {
  DatePicker,
  FilterDropdown,
  Form,
  Input,
  Select,
  Space,
  Table,
  useSelect,
  useTable
} from "@pankod/refine-antd"
import dayjs from "dayjs"
import {IAccount} from "../../../interfaces"
import {addressShortener} from "../../../utility"
import {IBuild} from "../../../interfaces/build"
import {IAccountFilterVariables} from "../../../interfaces/account"
import {numberWithCommas} from "../../../utility/number"

export const FormPayout: React.FC<IResourceComponentsProps | any> = (props) => {
  const {
    formProps,
    onFinish,
  } = props

  const [selectedRowKeys, setSelectedRowKeys] = React.useState<React.Key[]>([],)

  const onSelectChange = (selectedRowKeys: React.Key[]) => {
    setSelectedRowKeys(selectedRowKeys)
  }

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
    selections: [
      Table.SELECTION_ALL,
      Table.SELECTION_INVERT,
      Table.SELECTION_NONE,
    ],
  }

  const { selectProps: selectGameProps } = useSelect({
    resource: "games",
    optionLabel: "title",
  })

  const { selectProps: selectBuildProps } = useSelect<IBuild>({
    resource: "builds",
    optionLabel: "name"
  })

  const { tableProps: accountProps } = useTable<
    IAccount,
    HttpError,
    IAccountFilterVariables
    >({
    initialPageSize: 100,
    resource: 'accounts',
    metaData: { populate: [ 'status', 'game', 'build' ] }
  })

  const handleSubmit = (values:any) => {
    const data = {
      ...values,
      accounts: selectedRowKeys
    }
    return onFinish(data)
  }

  return (
    <Form
      {...formProps}
      wrapperCol={{ span: 12 }}
      layout="horizontal"
      onFinish={handleSubmit}
    >
      <Form.Item
        label="Name"
        name="name"
        rules={[{ required: true }]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Period Start"
        name="periodStart"
        rules={[{ required: true }]}
        getValueProps={(value) => ({
          value: value ? dayjs(value) : "",
        })}
      >
        <DatePicker />
      </Form.Item>
      <Form.Item
        label="Period End"
        name="periodEnd"
        rules={[{ required: true }]}
        getValueProps={(value) => ({
          value: value ? dayjs(value) : "",
        })}
      >
        <DatePicker />
      </Form.Item>
      <Table<IAccount>
        {...accountProps}
        rowSelection={rowSelection}
        rowKey="id"
      >
        <Table.Column
          key="[game][id]"
          dataIndex={['game', 'title']}
          title="Game"
          filterDropdown={(props) => (
            <FilterDropdown {...props}>
              <Select
                style={{ minWidth: 200 }}
                mode="multiple"
                placeholder="Select Game"
                {...selectGameProps}
              />
            </FilterDropdown>
          )}
        />
        <Table.Column
          key="[build][id]"
          dataIndex={["build", "name"]}
          title="Build"
          filterDropdown={(props) => (
            <FilterDropdown {...props}>
              <Select
                style={{ minWidth: 200 }}
                mode="multiple"
                placeholder="Select Build"
                {...selectBuildProps}
              />
            </FilterDropdown>
          )}
        />
        <Table.Column dataIndex="email" title="Email" />
        <Table.Column
          dataIndex="wallet"
          title="Wallet"
          render={val => addressShortener(val)}
        />
        <Table.Column
          dataIndex={["slp", "total"]}
          title="Total SLP"
          render={(value:string) => (<strong>{numberWithCommas(value || 0)} SLP</strong>)}
        />
        <Table.Column<IAccount>
          title="Actions"
          dataIndex="actions"
          render={(_, record): React.ReactNode => (
            <Space>
              abc
            </Space>
          )}
        />
      </Table>
    </Form>
  )
}