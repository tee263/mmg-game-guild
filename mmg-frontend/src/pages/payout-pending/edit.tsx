import {
  Button,
  Modal,
  Show,
  Space,
  Table,
  Typography, useModal,
  useTable,
} from "@pankod/refine-antd"
import {IResourceComponentsProps, useShow} from "@pankod/refine-core"
import { IPayoutConfirm } from "../../interfaces/payout"
import React, {useState} from "react"
import { getDate } from "../../utility/datetime"
import {addressShortener} from "../../utility"
import {IPayoutAccount} from "../../interfaces/payout-account"
import {EyeOutlined} from "@ant-design/icons"
import {PlotChart} from "../payout/edit/payout-tracker"
import {IAccount} from "../../interfaces"

const { Title, Text } = Typography

export const ShowPayoutPendingDetail: React.FC<IResourceComponentsProps> = () => {
  const [payoutDetail, setPayoutDetail] = useState()
  const [accountDetail, setAccountDetail] = useState<IAccount>()

  const { queryResult } = useShow<IPayoutConfirm>({
    metaData: { populate: ['payout']}
  })
  const { data, isLoading } = queryResult
  const record = data?.data

  const {
    tableProps: PayoutAccountProps,
  } = useTable<IPayoutAccount>({
    resource: 'payout-accounts',
    permanentFilter: [
      {
        field: "payout][id]",
        operator: "eq",
        value: record?.payout.id,
      }
    ],
    metaData: { populate: ['account']},
  })

  const { modalProps, show, close } = useModal()

  return (
    <Show  isLoading={isLoading}>
      <Title level={5}>Name:</Title>
      <Text>{record?.payout?.name}</Text>
      <Title level={5}>Period Start:</Title>
      <Text>{getDate(record?.payout?.periodStart ?? new Date())}</Text>
      <Title level={5}>Period End:</Title>
      <Text>{getDate(record?.payout?.periodEnd ?? new Date())}</Text>
      <Title level={5}>Account List:</Title>
      <Table {...PayoutAccountProps} rowKey="id">
        <Table.Column dataIndex={['account', 'email']} title="Account" />
        <Table.Column
          dataIndex={['account', 'wallet']}
          title="Wallet"
          render={val => addressShortener(val)}
        />
        <Table.Column
          dataIndex='periodStart'
          title="Period Start"
          render={(val) => getDate(val)}
        />
        <Table.Column
          dataIndex='periodEnd'
          title="Period End"
          render={(val) => getDate(val)}
        />
        <Table.Column
          dataIndex='amount'
          title="Amount"
        />
        <Table.Column<IPayoutAccount>
          title="Actions"
          dataIndex="actions"
          render={(_, record): React.ReactNode => (
            <Space>
              <Button
                size="small"
                onClick={() => {
                  setPayoutDetail(record.detail)
                  setAccountDetail(record.account)
                  show()
                }}
              >
                <EyeOutlined />
              </Button>
            </Space>
          )}
        />
      </Table>

      <Modal
        {...modalProps}
        onOk={close}
        width={1020}
      >
        <PlotChart
          data={payoutDetail}
          account={accountDetail}
        />
      </Modal>
    </Show>
  )
}
