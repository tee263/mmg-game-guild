import React from "react"
import {
  useTable,
  List,
  Row,
  Col,
  Card,
  Badge,
  Steps,
  Button,
  ShowButton, DeleteButton,
} from "@pankod/refine-antd"
import {
  HttpError,
  IResourceComponentsProps,
  useUpdate
} from "@pankod/refine-core"
import { IPayoutConfirm} from "../../interfaces/payout"
import { Popconfirm } from 'antd'

const { Step } = Steps

export const ListPayoutPending: React.FC<IResourceComponentsProps> = () => {
  const { tableProps } = useTable<
    IPayoutConfirm,
    HttpError
  >({
    metaData: { populate: '*' }
  })

  const { mutate } = useUpdate<IPayoutConfirm>()
  const handleConfirmPayout = (recordId:string) => {
    return mutate({
      id: recordId,
      resource: 'payout-confirms',
      values: {
        isConfirmed: true
      }
    })
  }

  return (
    <Row gutter={[16, 16]}>
      <Col lg={24} xs={24}>
        <List>
          <Card>
            {tableProps.dataSource?.map(data => {
              const { isConfirmed } = data
              const actions = [<ShowButton recordItemId={data.id} />]
              if (!isConfirmed) {
                actions.push(<DeleteButton recordItemId={data.id} />)
                actions.push(<Popconfirm
                  okType="danger"
                  title='Are you sure?'
                  onConfirm={():void => handleConfirmPayout(data.id)}
                >
                  <Button>
                    Confirm
                  </Button>
                </Popconfirm>)
              }
              return (
                <Card.Grid key={data.id}>
                  <Badge.Ribbon text={data.isConfirmed ? 'Confirmed' : 'Pending'}>
                    <Card
                      bordered={false}
                      title={data.payout.name}
                      actions={actions}
                    >
                      <div style={{marginTop: 20}}>
                        <Steps progressDot direction="vertical" current={1}>
                          <Step title="Start from" description={data.payout.periodStart} />
                          <Step title="End at" description={data.payout.periodEnd} />
                        </Steps>
                      </div>
                    </Card>
                  </Badge.Ribbon>
                </Card.Grid>
              )
            })}
          </Card>
        </List>
      </Col>
    </Row>
  )
}
