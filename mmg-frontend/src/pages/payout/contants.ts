
export const mappedStates = {
  waitForScholar: 'Wait For Scholar',
  rejectedByScholar: 'Rejected By Scholar',
  waitForManager: 'Wait For Manager',
  rejectedByManager: 'Rejected By Manager',
  confirmed: 'Confirmed'
}