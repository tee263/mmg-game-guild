import {
  useForm,
  Create,
} from "@pankod/refine-antd"
import { IResourceComponentsProps } from "@pankod/refine-core"
import {IPayout} from "../../../interfaces/payout"
import React from "react"
import {FormPayout} from "./form"

export const CreatePayout: React.FC<IResourceComponentsProps> = () => {
  const { formProps, saveButtonProps, onFinish } = useForm<IPayout>()

  return (
    <Create saveButtonProps={saveButtonProps}>
      <FormPayout
        formProps={formProps}
        onFinish={onFinish}
      />
    </Create>
  )
}
