import React from "react"
import {HttpError, IResourceComponentsProps} from "@pankod/refine-core"
import {
  Checkbox,
  DatePicker,
  FilterDropdown,
  Form,
  Input,
  Select,
  Table,
  useSelect,
  useTable
} from "@pankod/refine-antd"
import dayjs from "dayjs"
import {IAccount} from "../../../interfaces"
import {addressShortener} from "../../../utility"
import {IBuild} from "../../../interfaces/build"
import {IAccountFilterVariables} from "../../../interfaces/account"
import {numberWithCommas} from "../../../utility/number"
import {IPayoutAccount} from "interfaces/payout-account"

export const FormPayout: React.FC<IResourceComponentsProps | any> = (props) => {
  const {
    formProps,
    onFinish,
  } = props
  const [selectAllAccounts, setSelectAllAccounts] = React.useState<boolean>(false)
  const [selectedRowKeys, setSelectedRowKeys] = React.useState<React.Key[]>([],)

  const onSelectChange = (selectedRowKeys: React.Key[]) => {
    setSelectedRowKeys(selectedRowKeys)
  }

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
    selections: [
      Table.SELECTION_ALL,
      Table.SELECTION_INVERT,
      Table.SELECTION_NONE,
    ],
  }

  const { selectProps: selectBuildProps } = useSelect<IBuild>({
    resource: "builds",
    optionLabel: "name"
  })

  const { tableProps: accountProps } = useTable<
    IAccount,
    HttpError,
    IAccountFilterVariables
    >({
      initialPageSize: 100,
      resource: 'accounts',
      permanentFilter: [
        {
          field: "status][statusCode]",
          operator: "eq",
          value: "active",
        }
      ],
      metaData: { populate: [ 'status', 'guild', 'build', 'build.project' ] }
    })

  const handleSubmit = (values:any) => {
    const data = {
      ...values,
      accounts: selectedRowKeys,
      selectAllAccounts,
    }
    return onFinish(data)
  }

  const accountsPagination = accountProps.pagination as any

  return (
    <Form
      {...formProps}
      wrapperCol={{ span: 12 }}
      layout="horizontal"
      onFinish={handleSubmit}
    >
      <Form.Item
        label="Name"
        name="name"
        rules={[{ required: true }]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Period Start"
        name="periodStart"
        rules={[{ required: true }]}
        getValueProps={(value) => ({
          value: value ? dayjs(value) : "",
        })}
      >
        <DatePicker />
      </Form.Item>
      <Form.Item
        label="Period End"
        name="periodEnd"
        rules={[{ required: true }]}
        getValueProps={(value) => ({
          value: value ? dayjs(value) : "",
        })}
      >
        <DatePicker />
      </Form.Item>
      <Form.Item
        //label={`Select all ${accountsPagination?.current || 0} accounts`}
        label={`Select all accounts`}
        name="selectAllAccounts"
      >
        <Checkbox 
          onChange={(e) => setSelectAllAccounts(e.target.checked)} 
          value="selectAllAccounts" 
          checked={selectAllAccounts}
        />
      </Form.Item>
      <Table<IAccount>
        {...accountProps}
        rowSelection={rowSelection}
        rowKey="id"
      >
        <Table.Column
          key="[build][project]"
          dataIndex={['build', 'project', 'name']}
          title="Game"
        />
        <Table.Column
          key="[build][id]"
          dataIndex={["build", "name"]}
          title="Build"
          filterDropdown={(props) => (
            <FilterDropdown {...props}>
              <Select
                style={{ minWidth: 200 }}
                mode="multiple"
                placeholder="Select Build"
                {...selectBuildProps}
              />
            </FilterDropdown>
          )}
        />
        <Table.Column
          dataIndex="email"
          title="Account"
          render={(val, record:IPayoutAccount) => (<a href={`/accounts/show/${record.id}`}>{val}</a>)}
        />
        <Table.Column
          dataIndex="wallet"
          title="Wallet"
          render={val => addressShortener(val)}
        />
        <Table.Column
          dataIndex={["slp", "total"]}
          title="Total SLP"
          render={(value:string) => (<strong>{numberWithCommas(value || 0)} SLP</strong>)}
        />
      </Table>
    </Form>
  )
}