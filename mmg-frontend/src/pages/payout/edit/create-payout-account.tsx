import React, {Key, useState } from "react"
import {Modal, Select, Typography, useForm, useSelect} from "@pankod/refine-antd"
import {IAccount} from "../../../interfaces"
import {IPayout} from "../../../interfaces/payout"

const { Title } = Typography

interface CreatePayoutAccountProps {
  initialValues: any
  modalProps: any
  show: any
  close: any
}

export const CreatePayoutAccount: React.FC<CreatePayoutAccountProps> = (props) => {
  const { onFinish } = useForm<IPayout>({
    resource: 'payout-accounts',
    action: 'create',
    redirect: false
  })
  const [selectedKeys, setSelectedKeys] = useState<string[]>([])

  const { selectProps, queryResult } = useSelect<IAccount>({
    resource: "accounts",
    optionLabel: "email",
    filters: [
      {
        field: "[id]",
        operator: "nin",
        value: props.initialValues?.accounts?.map((_:any) => _.id),
      },
      {
        field: "status][statusCode]",
        operator: "eq",
        value: "active",
      }
    ],
    metaData: { populate: ['status', 'guild.group.project', 'scholar.users_permissions_user'] }
  })

  const handleSubmit = async () => {
    await Promise.all(selectedKeys.map((accountId:Key) =>
      onFinish({
        periodStart: props.initialValues.periodStart,
        periodEnd: props.initialValues.periodEnd,
        account: accountId,
        payout: props.initialValues.id
      })
    ))
    props.close()
  }

  const start = new Date(props?.initialValues?.periodStart)
  const end = new Date(props?.initialValues?.periodEnd)
  const selectOptions = (queryResult?.data?.data.filter(i => i.validEndDate == null || new Date(i.validEndDate) > start && new Date(i.validEndDate) <= end))?.map(i => i.id)
  selectProps.options = selectProps?.options?.filter(i => selectOptions?.findIndex(item => item == i?.value))

  return (
    <Modal
      {...props.modalProps}
      onOk={handleSubmit}
      width={920}
    >
      <Title>Add Account To The Payout</Title>
      <Title level={5}>Select Accounts:</Title>
      <Select
        {...selectProps}
        placeholder='Accounts'
        mode="multiple"
        style={{ width: '100%' }}
        onChange={(val:any) => setSelectedKeys(val)}
      />
    </Modal>
  )
}