import {Button, Edit, Form, Input, Modal, Typography, useForm, useModal,} from "@pankod/refine-antd"
import {IResourceComponentsProps} from "@pankod/refine-core"
import {IPayout} from "../../../interfaces/payout"
import React, {useState} from "react"
import {PayoutAccount} from "./payout-account"
import {getDate} from "../../../utility/datetime"
import {UserAddOutlined} from "@ant-design/icons"
import {PlotChart} from "./payout-tracker"
import {CreatePayoutAccount} from "./create-payout-account"
import {IAccount} from "../../../interfaces"

const { Title, Text } = Typography
const ROLE_KEY = "role"

export const EditPayout: React.FC<IResourceComponentsProps> = () => {
  const [payoutDetail, setPayoutDetail] = useState()
  const [accountDetail, setAccountDetail] = useState<IAccount>()

  const { formProps, queryResult } = useForm<IPayout>({
    redirect: false,
    metaData: { populate: ['accounts', 'payout_accounts']}
  })

  const { modalProps, show, close } = useModal()
  const { modalProps: modalAddAccountProps, show: showAddAccount, close: closeAddAccount } = useModal()

  const data = queryResult?.data?.data
  return (
    <Edit>
      <Form
        {...formProps}
        wrapperCol={{ span: 12 }}
        layout="vertical"
      >
        <Form.Item
          label="Name"
          name="name"
          rules={[{ required: true }]}
        >
          <Input disabled={localStorage.getItem(ROLE_KEY) === 'accountant'}/>
        </Form.Item>
        <Title level={5}>Period Start:</Title>
        <Text>{getDate(data?.periodStart ?? 0)}</Text>
        <Title level={5}>Period End:</Title>
        <Text>{getDate(data?.periodEnd ?? 0)}</Text>
      </Form>
      <div style={{margin: '20px 0'}}>
        <Button onClick={showAddAccount} disabled={localStorage.getItem(ROLE_KEY) === 'accountant'}>
          <UserAddOutlined /> Add Account
        </Button>
      </div>
      <PayoutAccount
        initialValues={data}
        setPayoutDetail={setPayoutDetail}
        setAccountDetail={setAccountDetail}
        show={show}
      />
      <Modal
        {...modalProps}
        onOk={close}
        width={1020}
      >
        <PlotChart
          data={payoutDetail}
          account={accountDetail}
        />
      </Modal>
      <CreatePayoutAccount
        initialValues={data}
        modalProps={modalAddAccountProps}
        show={showAddAccount}
        close={closeAddAccount}
      />
    </Edit>
  )
}
