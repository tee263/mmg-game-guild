import React, {Key} from "react"
import {
  Badge,
  Button,
  DeleteButton,
  ExportButton,
  List,
  Popover,
  Space,
  Table,
  Tag,
  Tooltip,
  useTable
} from "@pankod/refine-antd"
import {addressShortener} from "../../../utility"
import {IPayoutAccount, IPayoutAccountState} from "../../../interfaces/payout-account"
import {getDate} from "../../../utility/datetime"
import {EyeOutlined, InfoCircleOutlined} from '@ant-design/icons'
import {mappedStates} from "../contants"
import {useCreate, useExport} from "@pankod/refine-core"
import {numberWithCommas} from "utility/number"

const ROLE_KEY = "role"

export const PayoutAccount: React.FC<any> = (props) => {
  const { initialValues, setPayoutDetail, setAccountDetail, show } = props
  const [selectedRowKeys, setSelectedRowKeys] = React.useState<Key[]>([],)
  const {
    tableProps: PayoutAccountProps,
  } = useTable<IPayoutAccount>({
    resource: 'payout-accounts',
    permanentFilter: [
      {
        field: "payout][id]",
        operator: "eq",
        value: initialValues?.id,
      }
    ],
    metaData: { populate: ['payout' ,'account.build.kpi_rules']},
  })

  const { mutate, isLoading } = useCreate<IPayoutAccount>()
  const handleConfirmPayout = (recordId:Key, result:boolean) => {
    return mutate({
      resource: `payout-accounts/${recordId}/manager-confirm`,
      values: {
        result
      }
    })
  }
  const handleConfirmMany = (result:boolean) => {
    selectedRowKeys.map((id) => handleConfirmPayout(id, result))
  }

  const onSelectChange = (selectedRowKeys: React.Key[]) => {
    setSelectedRowKeys(selectedRowKeys)
  }
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
    selections: [
      Table.SELECTION_ALL,
      Table.SELECTION_INVERT,
      Table.SELECTION_NONE,
    ],
    getCheckboxProps: (record: IPayoutAccount) => ({
      disabled: record.state !== 'waitForManager'
    }),
  }

  const { triggerExport, isLoading: isLoadingExport } = useExport<IPayoutAccount>({
    resourceName: `payout-accounts-by-payout-id/${[initialValues?.id] || [-1]}`,
    pageSize: 1000,
    mapData: (item) => {
      return {
        Account: item.account.email,
        Wallet: item.account.wallet,
        Payout: item.payout.name,
        PeriodStart: item.periodStart,
        PeriodEnd: item.periodEnd,
        Build: item.account?.build?.name || '-',
        Status: item.state,
      }
    }
  })

  return (
    <List
      pageHeaderProps={{
        subTitle: (selectedRowKeys.length > 0) && (
          <Space>
            <Button
              type='primary'
              onClick={() => handleConfirmMany(true)}
              loading={isLoading}
            >
              {`Confirm selected ${selectedRowKeys.length} items`}
            </Button>
            <Button
              danger
              onClick={() => handleConfirmMany(false)}
              loading={isLoading}
            >
              {`Reject selected ${selectedRowKeys.length} items`}
            </Button>
          </Space>
        ),
        extra: (
          <ExportButton onClick={triggerExport} loading={isLoadingExport} />
        ),
      }}
      canCreate={false}
      title=''
    >
      <Table
        {...PayoutAccountProps}
        rowKey="id"
        rowSelection={rowSelection}
      >
        <Table.Column
          dataIndex={['account', 'email']}
          title="Account"
          render={(val, record:IPayoutAccount) => (<a href={`/accounts/show/${record?.account?.id}`}>{val}</a>)}
        />
        <Table.Column
          dataIndex={['account', 'wallet']}
          title="Wallet"
          render={val => addressShortener(val)}
        />
        <Table.Column
          dataIndex='periodStart'
          title="Period Start"
          render={val => getDate(val)}
        />
        <Table.Column
          dataIndex='periodEnd'
          title="Period End"
          render={val => getDate(val)}
        />
        <Table.Column
          dataIndex='amount'
          title="Amount"
          render={(val, record: any) => (
            <Popover
              placement="left"
              content={() => {
                const kpiRulesRange = record.detail?.kpiRulesRange || []
                return (<div>
                  {kpiRulesRange.map((item: any, idx: number) => (
                    <div key={idx}>
                      <div><strong>{item.start} - {item.end}</strong></div>
                      <div>Total: {item.totalSlp}, Avg: {item.avgSlp}, Payable: {item.totalSlp}*{item.maxPercent * 100}% = <strong>{item.payable}</strong></div>
                    </div>
                  ))}
                </div>)
              }}
            >
              <Badge count={<InfoCircleOutlined style={{ color: '#108ee9' }} />}>
                <Tag color="blue" style={{fontSize: 14}}>
                  <strong>
                    {numberWithCommas(val)}
                  </strong>
                </Tag>
              </Badge>
            </Popover>
          )}
        />
        <Table.Column
          dataIndex={['account', 'build', 'name']}
          title="Build"
        />
        <Table.Column<IPayoutAccount>
          dataIndex='state'
          title="Status"
          render={(val:IPayoutAccountState, record) => {
            return (
              <>
                <span>{mappedStates[val]} </span>
                {record.scholarNote && (
                  <Tooltip
                    placement="rightTop"
                    title={record.scholarNote}
                  >
                    <InfoCircleOutlined style={{ color: '#108ee9' }} />
                  </Tooltip>
                )}
              </>
            )
          }}
        />
        <Table.Column<IPayoutAccount>
          title="Actions"
          render={(record): React.ReactNode => (
            <Space>
              <Button
                size="small"
                onClick={() => {
                  setPayoutDetail(record.detail)
                  setAccountDetail(record.account)
                  show()
                }}
              >
                <EyeOutlined />
              </Button>
              {localStorage.getItem(ROLE_KEY) === 'accountant' ? null : (<DeleteButton
                resourceName='payout-accounts'
                size="small"
                recordItemId={record.id}
                hideText
              />)}
            </Space>
          )}
        />
      </Table>
    </List>
  )
}