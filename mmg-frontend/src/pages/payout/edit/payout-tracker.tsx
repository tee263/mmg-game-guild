import React from "react"
import { Column } from '@ant-design/plots'
import {IAccount} from "../../../interfaces"
import {addressShortener} from "../../../utility"
import {Typography} from "@pankod/refine-antd"

const { Title } = Typography

interface ChartProps {
  data?: {
    days: string[];
    slps: any;
  },
  account?: IAccount;
}

export const PlotChart: React.FC<ChartProps> = ({ data, account }) => {
  const _data = data?.days?.map(day => ({
    day,
    value: data.slps[day] || 0
  })) ?? []
  const config = {
    data: _data,
    xField: 'day',
    yField: 'value',
    seriesField: '',
    xAxis: {
      label: {
        autoHide: true,
        autoRotate: true,
      },
    },
  }
  return (
    <div>
      <Title level={2}>Tracker data: {addressShortener(account?.wallet)}</Title>
      <Column {...config} />
    </div>
  )
}