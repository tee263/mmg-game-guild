import {Badge, Card, DeleteButton, EditButton, List, Steps, useTable} from "@pankod/refine-antd"
import {HttpError, IResourceComponentsProps, useCreate} from "@pankod/refine-core"
import {IPayout} from "../../interfaces/payout"

const { Step } = Steps
const ROLE_KEY = "role"

export const ListPayout: React.FC<IResourceComponentsProps> = () => {
  const { tableProps } = useTable<
      IPayout,
      HttpError
    >({
      metaData: { populate: ['payout_confirm', 'payout_accounts.account']}
    })

  const { mutate } = useCreate<IPayout>()
  const handleConfirmPayout = (recordId:string) => {
    return mutate({
      resource: `payouts/${recordId}/confirm`,
      values: {}
    })
  }

  return (
    <List canCreate={localStorage.getItem(ROLE_KEY) !== 'accountant' && localStorage.getItem(ROLE_KEY) !== 'guest'}>
      <Card>
        {tableProps.dataSource?.map(data => {
          const { isPublished } = data
          const actions = [<EditButton recordItemId={data.id} disabled={localStorage.getItem(ROLE_KEY) === 'guest'}>Show</EditButton>]
          if (!isPublished && !(localStorage.getItem(ROLE_KEY) === 'accountant') && !(localStorage.getItem(ROLE_KEY) === 'guest')) {
            actions.push(<DeleteButton recordItemId={data.id} />)
            // actions.push(<Popconfirm
            //   okType="danger"
            //   title='Are you sure?'
            //   onConfirm={():void => handleConfirmPayout(data.id)}
            // >
            //   <Button>
            //     Confirm
            //   </Button>
            // </Popconfirm>)
          }
          const _isPublished = data.isPublished
          const isRejected = !data.payout_confirm
          return (
            <Card.Grid key={data.id}>
              <Badge.Ribbon
                text={_isPublished ?
                  (isRejected ?
                    'Rejected' : (
                      data.payout_confirm.isConfirmed ?
                        'Confirmed' : 'Pending'
                    ))
                  : 'Draft'
                }
                color={isRejected ? 'red' : undefined}
              >
                <Card
                  bordered={false}
                  title={data.name}
                  actions={actions}
                >
                  <div style={{marginTop: 20}}>
                    <Steps progressDot direction="vertical" current={1}>
                      <Step title="Start from" description={data.periodStart} />
                      <Step title="End at" description={data.periodEnd} />
                    </Steps>
                  </div>
                </Card>
              </Badge.Ribbon>
            </Card.Grid>
          )
        })}
      </Card>
    </List>
  )
}
