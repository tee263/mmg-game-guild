import React from "react"
import {
  Form,
  FormProps,
  Input,
  Icons,
  Button,
  Row,
  Col
} from "@pankod/refine-antd"

export const Filter: React.FC<{ formProps: FormProps }> = ({ formProps }) => {
  return (
    <Form layout="horizontal" {...formProps}>
      <Row gutter={16}>
        <Col lg={6} xs={24}>
          <Form.Item label="Search" name="email">
            <Input placeholder="Email" prefix={<Icons.SearchOutlined />} />
          </Form.Item>
        </Col>
        <Col lg={6} xs={24}>
          <Form.Item label="Wallet" name="wallet">
            <Input placeholder="Wallet" prefix={<Icons.SearchOutlined />} />
          </Form.Item>
        </Col>
        <Col lg={6} xs={24}>
          <Form.Item>
            <Button htmlType="submit" type="primary">
              Filter
            </Button>
          </Form.Item>
        </Col>
      </Row>
    </Form>
  )
}
