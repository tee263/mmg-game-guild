import {Form, Input, Select, useSelect} from "@pankod/refine-antd"
import {IAccount, IGuild, IStatus} from "../../../interfaces"

const FormAccount: React.FC<any> = ({formProps, queryResult}) => {
  const { selectProps: selectTransferStatusProps } = useSelect<IGuild>({
    resource: "transfer-statuses",
    optionLabel: "status",
  })
  const { selectProps: statusProps } = useSelect<IStatus>({
    resource: "statuses",
  })
  const { selectProps: gameProps } = useSelect<IAccount>({
    resource: "games",
    optionLabel: "title",
  })
  const { selectProps: guildProps } = useSelect<IGuild>({
    resource: "guilds-by-leader",
    optionLabel: "name",
  })

  return (
    <Form {...formProps} wrapperCol={{ span: 12 }} layout="vertical">
      <Form.Item label="Email" name="email">
        <Input />
      </Form.Item>
      <Form.Item label="Password" name="password">
        <Input />
      </Form.Item>
      <Form.Item label="Wallet" name="wallet" rules={[{required: true}]}>
        <Input />
      </Form.Item>
      <Form.Item label="Build" name="build" rules={[{required: true}]}>
        <Input />
      </Form.Item>
      <Form.Item
        label="Guild"
        name={["guild", "id"]}
        rules={[{required: true}]}
      >
        <Select {...guildProps} />
      </Form.Item>
      <Form.Item
        label="Axie Transfer"
        name={["transfer_status", "id"]}
      >
        <Select {...selectTransferStatusProps} />
      </Form.Item>
      <Form.Item
        label="Status"
        name={['status', 'id']}
      >
        <Select {...statusProps} />
      </Form.Item>
      <Form.Item
        label="Game"
        name={['game', 'id']}
        rules={[{required: true}]}
      >
        <Select {...gameProps} />
      </Form.Item>
    </Form>
  )
}

export default FormAccount
