import React from "react"
import {Button, EditButton, Form, Input, List, SaveButton, Space, Table, useEditableTable} from "@pankod/refine-antd"
import {IAccount, ISlp,} from "interfaces"
import {HttpError, IResourceComponentsProps, useList} from "@pankod/refine-core"
import {numberWithCommas} from "../../../utility/number"
import {getDate} from "../../../utility/datetime"
import {openNotification} from "../../../utility/NotificationUtils"

export const ListAccount: React.FC<IResourceComponentsProps> = () => {
  const {
    tableProps,
    formProps,
    tableQueryResult,
    isEditing,
    editButtonProps,
    saveButtonProps,
    cancelButtonProps,
  } = useEditableTable<ISlp,
    HttpError>({
    resource: "slp-by-scholar",
    redirect: false
  })

  const accountQueryResult = useList<IAccount>({
    resource: "accounts-by-scholar",
    config: {
      filters: [
        {
          field: '[id]',
          operator: 'in',
          value: tableQueryResult.data?.data.map(_ => _.accountId) || []
        }
      ]
    }
  })
  const accounts = accountQueryResult.data?.data || []

  return (
    <List>
      <Form {...formProps}>
        <Table {...tableProps} rowKey="id">
          <Table.Column
            dataIndex="accountId"
            title="Account"
            render={val => accounts.find(_ => _.id === val)?.email}
          />
          <Table.Column
            sorter
            dataIndex="todaySoFar"
            title="Today Earning"
            render={(value: string, record: any) => {
              if (isEditing(record.id)) {
                return (
                  <Form.Item
                    name="todaySoFar"
                    rules={[{required: true}]}
                  >
                    <Input/>
                  </Form.Item>
                )
              }
              return value || 0
            }}
          />
          <Table.Column
            sorter
            dataIndex="yesterdaySLP"
            title="Yesterday Earning"
            render={(value: string) => value || 0}
          />
          <Table.Column
            sorter
            dataIndex="average"
            title="Average"
            render={(value: string) => value || 0}
          />
          <Table.Column
            sorter
            dataIndex="total"
            title="Total"
            render={(value: string) => (<strong>{numberWithCommas(value || 0)}</strong>)}
          />
          <Table.Column
            sorter
            dataIndex="updatedAt"
            title="Date"
            render={val => getDate(val)}
          />
          <Table.Column<IAccount>
            title="Actions"
            render={(record: any): React.ReactNode => {
              if (isEditing(record.id)) {
                return (
                  <Space>
                    <SaveButton
                      {...saveButtonProps}
                      size="small"
                    />
                    <Button
                      {...cancelButtonProps}
                      size="small"
                    >
                      Cancel
                    </Button>
                  </Space>
                )
              }

              if (!record.isScholarEditable) return (<Button size="small" onClick={() => {
                const password = accounts.find(value => value.id === record.accountId)?.password
                password ? navigator.clipboard.writeText(password).then(() => openNotification("Copy password success", "", "success")) : openNotification("No password for this account", "", "error")
              }}>
                Copy Pass
              </Button>)
              return (
                <Space>
                  <EditButton
                    {...editButtonProps(record.id)}
                    size="small"
                    recordItemId={record.id}
                  />
                  <Button size="small" onClick={() => {
                    const password = accounts.find(value => value.id === record.accountId)?.password
                    password ? navigator.clipboard.writeText(password).then(() => openNotification("Copy password success", "", "success")) : openNotification("No password for this account", "", "error")
                  }}>
                    Copy Pass
                  </Button>
                </Space>

              )
            }}
          />
        </Table>
      </Form>
    </List>
  )
}