import {
  Button, EditButton,
  Form, Input,
  SaveButton,
  Show, Space, Table,
  Typography, useEditableTable,
} from "@pankod/refine-antd"
import {IAccount, ISlp} from "interfaces"
import {useShow} from "@pankod/refine-core"
import React from "react"
import {getDate} from "../../../utility/datetime"

const { Title, Text } = Typography

export const ShowAccount: React.FC = () => {
  const { queryResult } = useShow<IAccount>({
    metaData: { populate: '*' }
  })
  const { data, isLoading } = queryResult
  const record = data?.data

  const {
    tableProps,
    formProps,
    isEditing,
    editButtonProps,
    saveButtonProps,
    cancelButtonProps,
  } = useEditableTable<ISlp>({
    resource: 'slps',
    permanentFilter: [
      {
        field: "accountId",
        operator: "eq",
        value: record?.id,
      }, {
        field: "issuedDate",
        operator: "gte",
        value: record?.paidAt,
      },
    ],
    initialSorter: [
      {
        field: 'issuedDate',
        order: 'desc'
      }
    ],
    redirect: false
  })

  return (
    <Show isLoading={isLoading}>
      <Title level={5}>Email:</Title>
      <Text>{record?.email || "-"}</Text>
      <Title level={5}>Wallet:</Title>
      <Text>{record?.wallet}</Text>
      <Title level={5}>Build:</Title>
      <Text>{record?.build?.name ?? "-"}</Text>
      <Title level={5}>Status:</Title>
      <Text>{record?.status?.title ?? "-"}</Text>
      <Title level={5}>Game:</Title>
      <Text>{record?.guild?.group?.groupName ?? "-"}</Text>

      <Title level={5}>Dialy Amount:</Title>
      <Form {...formProps}>
        <Table {...tableProps} rowKey="id">
          <Table.Column
            dataIndex={['issuedDate']}
            title="Time"
            render={(v) => getDate(v)}
          />
          <Table.Column<IAccount>
            dataIndex='todaySoFar'
            title="Amount"
            render={(v, record) => {
              if (isEditing(record.id.toString())) {
                return (
                  <Form.Item
                    name='todaySoFar'
                    rules={[{required: true}]}
                  >
                    <Input />
                  </Form.Item>
                )
              }
              return v
            }}
          />
          <Table.Column dataIndex={['total']} title="Total" />
          <Table.Column<ISlp>
            title="Actions"
            dataIndex="actions"
            render={(_, record): React.ReactNode => {
              if (isEditing(record.id)) {
                return (
                  <Space>
                    <SaveButton
                      {...saveButtonProps}
                      size="small"
                    />
                    <Button
                      {...cancelButtonProps}
                      size="small"
                    >
                      Cancel
                    </Button>
                  </Space>
                )
              }
              return (
                <EditButton
                  {...editButtonProps(record.id)}
                  size="small"
                />
              )
            }}
          />
        </Table>
      </Form>
    </Show>
  )
}
