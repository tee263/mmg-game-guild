import React, {useEffect, useState} from "react"
import {
  useTable,
  List,
  Card,
  Badge,
  Steps,
  Button,
  Tag,
  Tooltip,
  useModal,
  Modal,
  ShowButton,
  Popconfirm,
  Tabs,
} from "@pankod/refine-antd"
import {HttpError, IResourceComponentsProps, useCreate} from "@pankod/refine-core"
import {IPayoutAccount} from "../../../interfaces/payout-account"
import { InfoCircleOutlined, SaveOutlined, DeleteOutlined } from "@ant-design/icons"
import { getDate } from "utility/datetime"
import {IAccount} from "../../../interfaces"
import {RibbonProps} from "antd/lib/badge/Ribbon"
import { PlotChart } from "pages/payout/edit/payout-tracker"

const { TabPane } = Tabs
const { Step } = Steps

export const ListPayout: React.FC<IResourceComponentsProps> = () => {
  const [payoutDetail, setPayoutDetail] = useState()
  const [accountDetail, setAccountDetail] = useState<IAccount>()
  const [note, setNote] = useState('')
  const [tab, setTab] = useState('processing')

  const { tableProps, tableQueryResult, filters } = useTable<
      IPayoutAccount,
      HttpError
    >({
      resource: 'payout-accounts/my',
      permanentSorter: [
        {
          field: 'createdAt',
          order: 'desc'
        }
      ],
      metaData: { populate: ['account.build.kpi_rules']}
    })

  useEffect(() => {
    filters?.splice(
      filters?.findIndex((_:any) => _.field === 'state'),
      1
    )
    if (tab === 'paid') {
      filters?.push({
        field: 'state',
        operator: 'eq',
        value: 'confirmed'
      })
    } else {
      filters?.push({
        field: 'state',
        operator: 'ne',
        value: 'confirmed'
      })
    }
    tableQueryResult.refetch()
  }, [tab])

  const { mutateAsync } = useCreate<IPayoutAccount>()
  const handleConfirmPayout = async (recordId:string, isConfirm:boolean) => {
    await mutateAsync({
      resource: `payout-accounts/${recordId}/scholar-confirm`,
      values: {
        result: isConfirm,
        scholarNote: note
      }
    })
    return tableQueryResult.refetch()
  }

  const { modalProps, show, close } = useModal()

  const renderGridDetail = (data:IPayoutAccount) => {
    const yourPercent = data.detail?.kpi?.percent || 0
    return (
      <div style={{marginTop: 20}}>
        <Steps progressDot current={1}>
          <Step title="Start from" description={getDate(data.periodStart)} />
          <Step title="End at" description={getDate(data.periodEnd)} />
        </Steps>
        <div style={{marginTop: 20}}>Build: <b>{data.account.build?.name}</b></div>
        <div>Total Earning: <b>{data.amount}</b></div>
        <div>Average Earning: <b>{data.detail.average.toFixed(2)}%</b></div>
        <div style={{marginBottom: 20}}>
          Your salary: <b>{data.amount * yourPercent} : </b>
          <Tooltip
            placement="rightTop"
            title={(
              <div>
                {data.account.build?.kpi_rules?.map((kpi:any) => (
                  <div>{kpi.value}: <strong>{kpi.percent * 100}%</strong></div>
                ))}
              </div>
            )}
          >
            <Tag color="magenta" style={{fontSize: 14}}>
              {yourPercent * 100}%
            </Tag>
            <InfoCircleOutlined style={{ color: '#108ee9' }} />
          </Tooltip>
        </div>
      </div>
    )
  }

  return (
    <List
      pageHeaderProps={{
        subTitle: (
          <Tabs onTabClick={key => setTab(key)}>
            <TabPane tab="On Progress" key="processing" />
            <TabPane tab="Paid" key="paid" />
          </Tabs>
        )
      }}
    >
      <Card>
        {tableProps.dataSource?.map(data => {
          const { state } = data
          const actions = [(
            <ShowButton
              onClick={() => {
                setPayoutDetail(data.detail)
                setAccountDetail(data.account)
                show()
              }}
            />
          )]
          if (state === 'waitForScholar') {
            actions.push(
              <Popconfirm
                okText='Confirm'
                okType='primary'
                title='Are you sure?'
                onConfirm={() => handleConfirmPayout(data.id, true)}
              >
                <Button
                  type="primary"
                  icon={<SaveOutlined />}
                >
                  Confirm
                </Button>
              </Popconfirm>
            )
            actions.push(
              <Popconfirm
                okText='Confirm'
                okType='danger'
                title={(
                  <div>
                    <div>Please tell more your problem?</div>
                    <textarea
                      onChange={e => setNote(e.target.value)}
                    />
                  </div>
                )}
                onConfirm={() => handleConfirmPayout(data.id, false)}
              >
                <Button
                  danger
                  icon={<DeleteOutlined />}
                >
                  Reject
                </Button>
              </Popconfirm>
            )
          }

          const rbProps:RibbonProps = {}
          switch (state) {
          case 'waitForScholar':
            rbProps.text = 'Ready To Confirm'
            rbProps.color = 'blue'
            break
          case 'rejectedByScholar':
            rbProps.text = 'Rejected'
            rbProps.color = 'red'
            break
          default:
            rbProps.text = 'Confirmed'
            rbProps.color = 'green'
          }
          return (
            <Card.Grid key={data.id}>
              <Badge.Ribbon
                {...rbProps}
              >
                <Card
                  bordered={false}
                  title={`${data.id} - ${data.account.email}`}
                  actions={actions}
                >
                  {renderGridDetail(data)}
                </Card>
              </Badge.Ribbon>
            </Card.Grid>
          )
        })}
      </Card>
      <Modal
        {...modalProps}
        onOk={close}
        width={1020}
      >
        <PlotChart
          data={payoutDetail}
          account={accountDetail}
        />
      </Modal>
    </List>
  )
}
