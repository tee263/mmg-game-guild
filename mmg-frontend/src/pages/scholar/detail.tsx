import {
  Show,
  Typography,
} from "@pankod/refine-antd"
import { useShow } from "@pankod/refine-core"
import {IScholar} from "../../interfaces/scholar"
const { Title, Text } = Typography

export const DetailScholar: React.FC = () => {
  const { queryResult } = useShow<IScholar>({
    metaData: { populate: ['accounts.guild.group', 'users_permissions_user'] }
  })
  const { data, isLoading } = queryResult
  const record = data?.data

  return (
    <Show isLoading={isLoading}>
      <Title level={5}>Identify ID:</Title>
      <Text>{record?.identifyId || "-"}</Text>
      <Title level={5}>Accounts:</Title>
      <Text>{record?.accounts.map(_ => _.wallet).join(', ') || "-"}</Text>
      <Title level={5}>Personal Wallet:</Title>
      <Text>{record?.personalWallet || "-"}</Text>
      <Title level={5}>Phone Number:</Title>
      <Text>{record?.phoneNumber || "-"}</Text>
      <Title level={5}>Created At:</Title>
      <Text>{record?.createdAt || "-"}</Text>
    </Show>
  )
}
