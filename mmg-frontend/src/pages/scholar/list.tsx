import React, {useState} from "react"
import {
  Card,
  Col,
  CreateButton, DeleteButton,
  EditButton,
  Form,
  Input,
  List,
  Row,
  Select,
  ShowButton,
  Space,
  Tag,
  useSelect,
  useTable
} from "@pankod/refine-antd"
import {CrudFilters, HttpError, IResourceComponentsProps, useCreateMany, useCustom} from "@pankod/refine-core"
import {IAuthUser, IGuild, IRegisUser, IUserFilterVaribles} from "interfaces"
import {IScholar} from "../../interfaces/scholar"
import readXlsxFile from 'read-excel-file'
import {Button, Modal, Table} from "antd"
import {
  API_URL,
  emailRegex,
  errorCol,
  excelCol,
  schemaErrorCol,
  scholarSchema as schema, STATUS_ENUM
} from "../../utility/DataMappingUtils"
import {openNotification} from "../../utility/NotificationUtils"
import {IRole} from "../../interfaces/role"
import {Filter} from "./filter"
import {SortOrder} from "antd/lib/table/interface"

const ROLE_KEY = "role"

export const ListScholar: React.FC<IResourceComponentsProps> = () => {

  const allScholars = useCustom<IScholar[]>({
    url: `${API_URL}/api/all-scholars`,
    method: "get",
    queryOptions: {enabled: false}
  })

  const scholarRole = useCustom<IRole>({
    url: `${API_URL}/api/role-by-type/scholar`,
    method: "get"
  })

  const [visible, setVisible] = useState(false)
  const [errorPopVisible, setErrorPopVisible] = useState(false)
  const [selectedStatus, setSelectedStatus] = useState('active')
  const [selectedGuild, setSelectedGuild] = React.useState(-1)
  const [excelDataScholars, setExcelDataScholars] = useState<any>([])
  const [schemaErorrs, setSchemaErorrs] = useState<any>([])
  const [allScholarsData, setAllScholarsData] = useState<any>([])
  const errorData: any[] = []

  excelDataScholars.forEach((record : any, index: any) => {
    if(allScholarsData.length > 0) {
      const {username, email} = record

      if(!emailRegex.test(email)) {
        errorData.push({field: "Email", row: index + 2, value:"'" + email + "' => Invalid Email Address"})
      }

      allScholarsData.forEach((scholar: any) => {
        if(username === scholar.username) {
          errorData.push({field: "User Name", row: index + 2, value:"'" + username + "' => Existed on System"})
        }
        if(email === scholar.email) {
          errorData.push({field: "Email", row: index + 2, value:"'" + email + "' => Existed on System"})
        }
      })
    }
  })

  const { mutate }  = useCreateMany<IRegisUser>()

  const { selectProps: guildsProps } = useSelect<IGuild>({
    resource: "guilds",
    optionLabel: "name"
  })

  const {tableProps, searchFormProps} = useTable<
      IAuthUser,
      HttpError, 
      IUserFilterVaribles>({
        onSearch: (params: any) => {
          const filters: CrudFilters = []
          const {
            personalWallet,
            username,
            status
          } = params

          filters.push({
            field: "personalWallet",
            operator: "contains",
            value: personalWallet,
          },{
            field: "users_permissions_user][username]",
            operator: "contains",
            value: username,
          },{
            field: "status][statusCode]",
            operator: "eq",
            value: status,
          })
          setSelectedStatus(status)
          return filters
        },
        initialSorter: [
          {
            field: "[users_permissions_user][username]",
            order: "asc",
          },
        ],
        initialFilter: [
          {
            field: "status][statusCode]",
            operator: "eq",
            value: "active",
          }
        ],
        metaData: {
          populate: ['users_permissions_user', 'status', 'accounts']
        }
      })

  const onClickImport = () => {
    if(selectedGuild === -1) {
      openNotification("Please Select guild before doing Import!", "", "error")
    } else if(errorData.length > 0 || schemaErorrs.length > 0) {
      setVisible(false)
      setErrorPopVisible(true)
    } else {
      const dataToImport = excelDataScholars.map((item: any) => {
        return {...item, role: scholarRole.isSuccess ? {id: scholarRole.data?.data.id} : undefined, guild: {id: selectedGuild}}
      })
      mutate({
        resource: 'scholars',
        values: dataToImport
      })
      setExcelDataScholars([])
      setSelectedGuild(-1)
      setSchemaErorrs([])
      setVisible(false)
    }
  }

  return (
    <Row gutter={[16, 16]}>
      <Col xs={24}>
        <Card title="Scholar Filter">
          <Filter formProps={searchFormProps} />
        </Card>
      </Col>
      <Col xs={24}>
        <Modal
          title="Import Scholars"
          centered
          visible={visible}
          destroyOnClose={true}
          okText={"Import"}
          onOk={() => onClickImport()}
          onCancel={() => {
            setExcelDataScholars([])
            setSelectedGuild(-1)
            setSchemaErorrs([])
            setVisible(false)
          }}
          width={1300}
        >

          <Form  wrapperCol={{ span: 24 }} layout="vertical">
            <Button style={{position: 'absolute',top: '66px', right: '24px', zIndex: 1}} type={"primary"} onClick={() => {
              const a = document.createElement("a")
              a.href = "/files/Import_Scholars.xlsx"
              a.setAttribute("download", 'Import_scholar_template.xlsx')
              a.click()
            }}>Download Template</Button>
            <Form.Item rules={[{ required: true }]} label="Select Guild" name="guild"><Select {...guildsProps} onChange={
              (guild:any) => {
                setSelectedGuild(parseInt(guild))
              }
            }/></Form.Item>
            <Form.Item rules={[{ required: true }]} label="File To Import">
              <Input type="file" accept=".xlsx" disabled={selectedGuild === -1} onChange={(event) => {
                if (event.target.files) {
                  readXlsxFile(event.target.files[0], { schema }).then((rows) => {
                    setExcelDataScholars(rows.rows)
                    setSchemaErorrs(rows.errors)
                  })
                }
              }
              }/>
            </Form.Item>
          </Form>
          <Table columns={excelCol} dataSource={excelDataScholars} rowKey={"email"}/>
        </Modal>

        <Modal
          title={<h3 style={{color: 'red'}}>Input File Invalid</h3>}
          centered
          visible={errorPopVisible}
          destroyOnClose={true}
          onOk={() =>{
            setExcelDataScholars([])
            setSchemaErorrs([])
            setSelectedGuild(-1)
            setErrorPopVisible(false)
          }}
          onCancel={() => {
            setExcelDataScholars([])
            setSchemaErorrs([])
            setSelectedGuild(-1)
            setErrorPopVisible(false)
          }}
          width={900}
        >
          {schemaErorrs.length > 0 ? <Table columns={schemaErrorCol} dataSource={schemaErorrs} rowKey={"row"}/> :  <Table columns={errorCol} dataSource={errorData} rowKey={"row"}/>}

          <h3 style={{color: 'red', textAlign: 'center'}}>Please correct file data before doing Import!</h3>
        </Modal>
        <List createButtonProps={{size: "small"}} pageHeaderProps={{
          extra:
                        <div>
                          <div className="ant-upload ant-upload-select ant-upload-select-text">
                            <span tabIndex={0}
                              className="ant-upload"
                              role="button">
                              <button disabled={localStorage.getItem(ROLE_KEY) === "guest"} type="button" className="ant-btn ant-btn-default" onClick={() => {
                                allScholars.refetch().then(values => {
                                  setAllScholarsData(values.data?.data)
                                })
                                setVisible(true)}
                              }>
                                <span role="img" aria-label="import" className="anticon anticon-import">
                                  <svg viewBox="64 64 896 896"
                                    focusable="false"
                                    data-icon="import" width="1em"
                                    height="1em"
                                    fill="currentColor"
                                    aria-hidden="true">
                                    <path d="M888.3 757.4h-53.8c-4.2 0-7.7 3.5-7.7 7.7v61.8H197.1V197.1h629.8v61.8c0 4.2 3.5 7.7 7.7 7.7h53.8c4.2 0 7.7-3.4 7.7-7.7V158.7c0-17-13.7-30.7-30.7-30.7H158.7c-17 0-30.7 13.7-30.7 30.7v706.6c0 17 13.7 30.7 30.7 30.7h706.6c17 0 30.7-13.7 30.7-30.7V765.1c0-4.3-3.5-7.7-7.7-7.7zM902 476H588v-76c0-6.7-7.8-10.5-13-6.3l-141.9 112a8 8 0 000 12.6l141.9 112c5.3 4.2 13 .4 13-6.3v-76h314c4.4 0 8-3.6 8-8v-56c0-4.4-3.6-8-8-8z">
                                    </path>
                                  </svg>
                                </span>
                                <span>Import</span>
                              </button>
                            </span>
                          </div>
                          &nbsp;
                          <CreateButton disabled={localStorage.getItem(ROLE_KEY) === "guest"}/>
                        </div>
        }}>
          <Table {...tableProps} rowKey="id">
            <Table.Column
              dataIndex={["users_permissions_user", "username"]}
              title="Username"
            />
            <Table.Column
              dataIndex={["identifyId"]}
              title="Identify ID"
            />
            <Table.Column
              dataIndex={["personalWallet"]}
              title="Personal Wallet"
            />
            <Table.Column
              dataIndex={["phoneNumber"]}
              title="Phone Number"
            />
            <Table.Column
              dataIndex={["status", "title"]}
              title="Status"
            />
            <Table.Column<IAuthUser>
              title=""
              dataIndex="actions"
              align="right"
              render={(_:any, record:any): React.ReactNode => {
                return (
                  <Space>
                    <ShowButton
                      disabled={localStorage.getItem(ROLE_KEY) === "guest"}
                      size="middle"
                      recordItemId={record.id}
                      hideText
                    />
                    <EditButton
                      disabled={localStorage.getItem(ROLE_KEY) === "guest"}
                      size="middle"
                      recordItemId={record.id}
                      hideText
                    />
                    {selectedStatus === "active" ? (<DeleteButton
                      disabled={selectedStatus !== "active"}
                      size="middle"
                      recordItemId={record.id}
                      hideText
                    />) : <div></div>}
                    
                  </Space>
                )
              }}
            />
          </Table>
        </List>
      </Col>
    </Row>
  )
}
