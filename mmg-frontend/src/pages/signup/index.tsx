import React, { useState } from "react"
import {
  Row,
  Col,
  AntdLayout,
  Card,
  Typography,
  Form,
  Input,
  Button,
  Spin,
} from "@pankod/refine-antd"
import { useNavigation } from "@pankod/refine-core"
import "./styles.css"

const { Text, Title } = Typography

export interface ISignup {
    email: string;
    password: string;
}

export const Signup: React.FC = () => {
  const [form] = Form.useForm<ISignup>()
  const { push } = useNavigation()
  const [loading, setLoading] = useState(false)

  const CardTitle = (
    <Title level={3} className="title">
      Sign Up
    </Title>
  )

  const signup = async (email: string, password: string) => {
    // setLoading(true)
    // const { error } = await supabaseClient.auth.signUp({
    //   email,
    //   password,
    // })
    // setLoading(false)
    //
    // if (error) {
    //   return Promise.reject(error)
    // }
    // push('/login')
  }

  return (
    <AntdLayout className="layout">
      <Row
        justify="center"
        align="middle"
        style={{
          height: "100vh",
        }}
      >
        <Col xs={22}>
          <div className="container">
            <div className="imageContainer">
              <img src="./coincu.png" alt="Refine Logo" />
            </div>
            <Card title={CardTitle} headStyle={{ borderBottom: 0 }}>
              <Form<ISignup>
                layout="vertical"
                form={form}
                onFinish={(values:any) => signup(values.email, values.password)}
              >
                <Form.Item
                  name="email"
                  label="Email"
                  rules={[{ required: true, type: "email" }]}
                >
                  <Input size="large" placeholder="Email" />
                </Form.Item>
                <Form.Item
                  name="password"
                  label="Password"
                  rules={[{ required: true }]}
                  style={{ marginBottom: "12px" }}
                >
                  <Input
                    type="password"
                    placeholder="●●●●●●●●"
                    size="large"
                  />
                </Form.Item>
                <div style={{ marginTop: 24 }}>
                  <Spin spinning={loading}>
                    <Button
                      type="primary"
                      size="large"
                      htmlType="submit"
                      block
                    >
                    Create Account
                    </Button>
                  </Spin>
                </div>
              </Form>
              <div style={{ marginTop: 16 }}>
                <Text style={{ fontSize: 12 }}>Already have an account?
                  <a
                    href="#"
                    style={{
                      fontWeight: "bold",
                      marginLeft: 12,
                    }}
                    onClick={() => push("login")}
                  >
                    Sign in
                  </a>
                </Text>
              </div>
            </Card>
          </div>
        </Col>
      </Row>
    </AntdLayout>
  )
}
