import React from "react"
import {
  Badge,
  Card,
  Col,
  List,
  Popover,
  Row,
  Table,
  Tag,
  useTable,
} from "@pankod/refine-antd"
import {CrudFilters, HttpError, IResourceComponentsProps} from "@pankod/refine-core"
import {numberWithCommas} from "../../utility/number"
import {ISlp} from "../../interfaces/slp"
import {Filter} from "./filter"
import {InfoCircleOutlined } from '@ant-design/icons'
import { getDate } from "utility/datetime"

export const ListSlp: React.FC<IResourceComponentsProps> = () => {
  const {
    tableProps,
    searchFormProps,
  } = useTable<
    ISlp,
    HttpError
  >({
    onSearch: (params:any) => {
      const filters: CrudFilters = []
      const {
        group, guild, game, periodStart, periodEnd,
      } = params

      filters.push({
        field: "gameId",
        operator: "eq",
        value: game,
      }, {
        field: "guildId",
        operator: "eq",
        value: guild,
      }, {
        field: "groupId",
        operator: "eq",
        value: group,
      },
      {
        field: "periodStart",
        operator: "eq",
        value: periodStart,
      }, {
        field: "periodEnd",
        operator: "eq",
        value: periodEnd,
      })
      return filters
    },
    permanentSorter: [
      {
        field: 'issuedDate',
        order: 'desc'
      }
    ],
  })

  const accountsPagination = tableProps.pagination as any
  const currentPage = accountsPagination?.current || 1
  const pageSize = accountsPagination?.pageSize || 10
  
  return (
    <Row gutter={[16, 16]}>
      <Col xs={24}>
        <Card title="Earning Filter">
          <Filter
            formProps={searchFormProps}
          />
        </Card>
      </Col>
      <Col xs={24}>
        <List>
          <Table {...tableProps} rowKey="accountId">
            <Table.Column
              dataIndex="account"
              title="#"
              render={(_, __, index) => {
                const order = ((currentPage - 1) * pageSize) + index + 1
                return order
              }}
            />
            <Table.Column
              dataIndex='account'
              title="Account"
              render={val => <a href={`/accounts/show/${val?.id}`}>{val?.email}</a>}
            />
            <Table.Column
              dataIndex={['account', 'scholar', 'username']}
              title="Scholar"
            />
            <Table.Column
              dataIndex={['account', 'build', 'name']}
              title="Build"
            />
            <Table.Column
              dataIndex={['detail', 'slps']}
              title="Mined"
              render={(slps: Record<string, number>, record: any) => (<>
                <Popover
                  placement="left"
                  content={(<div>
                    {record.detail?.days?.map?.((dayString: string) => {
                      const slp = slps?.[dayString] || 0
                      return (
                        <div key={dayString}>
                          {getDate(dayString)}: {slp}
                        </div>
                      )
                    })}
                  </div>)}
                >
                  <Badge count={<InfoCircleOutlined style={{ color: '#108ee9' }} />}>
                    <Tag color="blue" style={{fontSize: 14}}>
                      <strong>
                        {numberWithCommas(Object.values(slps).reduce((total, slp) => total + slp, 0) || 0)}
                      </strong>
                    </Tag>
                  </Badge>
                </Popover>
              </>)}
            />
            <Table.Column
              dataIndex={['detail', 'total']}
              title="Payable to scholar"
              render={(value: number, record: any) => (<>
                <Popover
                  placement="left"
                  content={() => {
                    const kpiRulesRange = record.detail?.kpiRulesRange || []
                    return (<div>
                      {kpiRulesRange.map((item: any, idx: number) => (
                        <div key={idx}>
                          <div><strong>{item.start} - {item.end}</strong></div>
                          <div>Total: {item.totalSlp}, Avg: {item.avgSlp}, Payable: {item.totalSlp}*{item.maxPercent * 100}% = <strong>{item.payable}</strong></div>
                        </div>
                      ))}
                    </div>)
                  }}
                >
                  <Badge count={<InfoCircleOutlined style={{ color: '#108ee9' }} />}>
                    <Tag color="blue" style={{fontSize: 14}}>
                      <strong>
                        {numberWithCommas(value)}
                      </strong>
                    </Tag>
                  </Badge>
                </Popover>
              </>)}
            />
          </Table>
        </List>
      </Col>
    </Row>
  )
}
