import {CreateButton, Form, Input, Select, useForm, useSelect} from "@pankod/refine-antd"

import {IResourceComponentsProps, useCustom} from "@pankod/refine-core"
import {IGuild, IRegisUser} from "../../interfaces"
import {IRole} from "../../interfaces/role"
import {openNotification} from '../../utility/NotificationUtils'
const ROLE_KEY = "role"

export const CreateScholar: React.FC<IResourceComponentsProps> = () => {

  const API_URL = process.env.REACT_APP_API_URL

  const {onFinish, formProps} = useForm<IRegisUser>({
    resource: 'scholars',
    action: "create",
    redirect: "list"
  })

  const scholarRole = useCustom<IRole>({
    url: `${API_URL}/api/role-by-type/scholar`,
    method: "get"
  })

  const {selectProps: guildProps} = useSelect<IGuild>({
    resource: localStorage.getItem(ROLE_KEY) !== 'manager' ? "guilds-by-leader" : "guilds",
    optionLabel: "name",
    fetchSize: 100,
    metaData: { populate: "group"}
  })

  const onClick = () => {
    formProps.form?.setFields([
      {
        name: "confirmed",
        value: true
      },
      {
        name: "role",
        value: scholarRole.isSuccess ? {id: scholarRole.data?.data.id} : undefined
      }
    ])

    const temp = formProps.form?.getFieldsValue([
      "username", "email", "password", "confirmed", "role", "confirmPassword", "identifyId", "personalWallet", "phoneNumber", "guild"
    ])
    const {username, email, password, confirmPassword, identifyId, personalWallet, phoneNumber, guild} = temp

    if (password !== confirmPassword) {
      openNotification("Confirm password is not matched!", "", "error")
      formProps.form?.resetFields(["password", "confirmPassword"])
      return
    } else if (!username || !email || !password || !confirmPassword || !identifyId || !personalWallet || !phoneNumber || !guild) {
      openNotification("Some fields is Missing!", "Please check your required field!", "error")
    } else {
      onFinish(temp).then(() => {
        console.log(temp)
      }).finally(() => {
        formProps.form?.resetFields(["username", "password", "email", "confirmPassword", "identifyId", "personalWallet", "phoneNumber", "guild"])
      })
    }
  }

  return (
    <div>
      <div style={{padding: "24px", minHeight: "360px"}}>
        <div className={"ant-page-header ant-page-header-compact"}>
          <div className="ant-page-header-heading">
            <div className="ant-page-header-content" style={{paddingBottom: "12px"}}>
              <div className="ant-page-header-heading-left">
                <div className="ant-page-header-back" onClick={() => history.go(-1)}>
                  <div role="button" tabIndex={0} className="ant-page-header-back-button" aria-label="Back"
                    style={{
                      border: "0px",
                      background: "transparent",
                      paddingLeft: "0px",
                      lineHeight: "inherit",
                      display: "inline-block"
                    }}>
                    <span role="img" aria-label="arrow-left" className="anticon anticon-arrow-left">
                      <svg
                        viewBox="64 64 896 896" focusable="false" data-icon="arrow-left" width="1em" height="1em"
                        fill="currentColor" aria-hidden="true"><path
                          d="M872 474H286.9l350.2-304c5.6-4.9 2.2-14-5.2-14h-88.5c-3.9 0-7.6 1.4-10.5 3.9L155 487.8a31.96 31.96 0 000 48.3L535.1 866c1.5 1.3 3.3 2 5.2 2h91.5c7.4 0 10.8-9.2 5.2-14L286.9 550H872c4.4 0 8-3.6 8-8v-60c0-4.4-3.6-8-8-8z"/>
                      </svg>
                    </span>
                  </div>
                </div>
                <span className="ant-page-header-heading-title" title="Create Account">Create Scholar Account</span>
              </div>
              <span className="ant-page-header-heading-tags"/></div>

          </div>
          <Form {...formProps} wrapperCol={{span: 12}} layout="vertical">
            <Form.Item rules={[{required: true}]} label="User Name" name="username"><Input/></Form.Item>
            <Form.Item rules={[{required: true}, {type: "email"}]} label="Email" name="email"><Input type="email"/></Form.Item>
            <Form.Item rules={[{required: true}]} label="Identify ID" name="identifyId"><Input type={"number"}/></Form.Item>
            <Form.Item rules={[{required: true}]} label="Personal Wallet" name="personalWallet"><Input/></Form.Item>
            <Form.Item rules={[{required: true}]} label="Phone Number" name="phoneNumber"><Input/></Form.Item>
            <Form.Item
              rules={[{required: true}]}
              label="Guild"
              name="guild"
            >
              <Select {...guildProps}/>
            </Form.Item>
            <Form.Item rules={[{required: true}]} label="Password" name="password"><Input type="password"/></Form.Item>
            <Form.Item rules={[{required: true}]} label="Confirm Password" name="confirmPassword"><Input
              type="password"/></Form.Item>
            <Form.Item label="Confirmed" name="confirmed" style={{display: "none"}}><Input/></Form.Item>
            <Form.Item label="Role" name="role" style={{display: "none"}}><Input/></Form.Item>
            <CreateButton onClick={onClick} type={"primary"}>Submit</CreateButton>
          </Form>
        </div>
      </div>
    </div>
  )
}
