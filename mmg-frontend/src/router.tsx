import {CreateGroup, DetailGroup, EditGroup, ListGroup} from "./pages/group"
import {DetailScholar, EditScholar, ListScholar} from "./pages/scholar"
import {CreateGuildLeader, DetailGuildLeader, EditGuildLeader, ListGuildLeader} from "./pages/guildLeader"
import {
  AlipaySquareFilled,
  ApartmentOutlined,
  AreaChartOutlined,
  BuildOutlined,
  DollarOutlined,
  FieldTimeOutlined,
  PayCircleFilled,
  TeamOutlined
} from "@ant-design/icons"
import {CreateGuild, DetailGuild, EditGuild, ListGuild} from "./pages/guild"
import {CreateBuild, DetailBuild, EditBuild, ListBuild} from "./pages/build"
import {CreateAccount, EditAccount, ListAccount, ShowAccount} from "./pages/account"
import {EditAccount as EditAccountByLeader} from "./pages/guildLeader-pages/account/edit"
import {ListAccount as ScholarListAccount, ShowAccount as ScholarShowAccount} from "./pages/scholar-pages/account"
import {ListPayout as ScholarListPayoutAccount} from './pages/scholar-pages/payout'
import {ListAccount as LeadListAccount, ShowAccount as LeadShowAccount} from "./pages/guildLeader-pages/account"
import {DetailGuild as LeadShowGuild, ListGuild as LeadListGuild} from "./pages/guildLeader-pages/guild"
import {
  DetailScholar as LeadShowScholar,
  EditScholar as LeaderEditScholar,
  ListScholar as LeadListScholar
} from "./pages/guildLeader-pages/scholar"
import {ListSlp as LeadListSlp} from "./pages/guildLeader-pages/slp"
import {CreatePayout, EditPayout, ListPayout} from "./pages/payout"
import {ListPayment} from "./pages/payment"
import {ListPayment as ListPayable} from "./../src/pages/payable/list"
import {ListPayment as ListLeaderPayment} from "./pages/guildLeader-pages/payment/list"
import {ListPayoutPending, ShowPayoutPendingDetail} from "./pages/payout-pending"
import {CreateScholar as CreateUserScholar} from "./pages/user/create"
import {IResourceItem} from '@pankod/refine-core'
import {ResourceProps} from "@pankod/refine-core/src/contexts/resource/IResourceContext"
import {ReactComponent as SportEsportIcon} from './assets/icons/sports_esports.svg'
import {ReactComponent as SmartToy} from './assets/icons/smart_toy.svg'
import {ListSlp as ManagerListSlp} from "./pages/slp"

interface IResource extends IResourceItem, ResourceProps {}

const routers:IResource[] = [
  {
    name: 'slp-by-leader',
    list: LeadListSlp,
    icon: <DollarOutlined />,
    options: {
      label: 'Earning'
    }
  },
  {
    name: 'slp-by-manager',
    list: ManagerListSlp,
    icon: <DollarOutlined />,
    options: {
      label: 'Earning'
    }
  },
  {
    name: 'builds',
    list: ListBuild,
    create: CreateBuild,
    show: DetailBuild,
    edit: EditBuild,
    icon: <BuildOutlined />
  },
  {
    name: "groups",
    list: ListGroup,
    edit: EditGroup,
    create: CreateGroup,
    show: DetailGroup,
    icon: (<SportEsportIcon width={14} fill={'#ffffff'}/>)
  }, 
  {
    name: "guilds",
    list: ListGuild,
    edit: EditGuild,
    create: CreateGuild,
    show: DetailGuild,
    icon: <TeamOutlined />
  },
  {
    name: "guilds-by-leader",
    list: LeadListGuild,
    show: LeadShowGuild,
    icon: <TeamOutlined />,
    options: {
      label: "Guilds",
      route: 'guild-by-leader'
    }
  },
  {
    name: "guild-leaders",
    list: ListGuildLeader,
    edit: EditGuildLeader,
    create: CreateGuildLeader,
    show: DetailGuildLeader,
    icon: <ApartmentOutlined />
  },
  {
    name: "accounts",
    create: CreateAccount,
    list: ListAccount,
    edit: EditAccount,
    show: ShowAccount,
    icon: <SmartToy width={14} fill={'#ffffff'} />
  },
  {
    name: "accounts-by-scholar",
    list: ScholarListAccount,
    show: ScholarShowAccount,
    icon: <SmartToy width={14} fill={'#ffffff'} />,
    options: {
      label: "Accounts",
      route: 'account-by-scholar'
    }
  },
  {
    name: "accounts-by-leader",
    list: LeadListAccount,
    show: LeadShowAccount,
    edit: EditAccountByLeader,
    icon: <SmartToy width={14} fill={'#ffffff'} />,
    options: {
      label: "Accounts",
      route: 'accounts-by-leader'
    }
  },
  {
    name: "scholars",
    list: ListScholar,
    edit: EditScholar,
    create: CreateUserScholar,
    show: DetailScholar,
    icon: <SmartToy width={14} fill={'#ffffff'} />
  },
  {
    name: "scholars-by-leader",
    list: LeadListScholar,
    show: LeadShowScholar,
    create: CreateUserScholar,
    edit: LeaderEditScholar,
    icon: <SmartToy width={14} fill={'#ffffff'} />,
    options: {
      label: "Scholars",
      route: 'scholar-by-leader'
    }
  },
  {
    name: 'scholar/payout-accounts',
    list: ScholarListPayoutAccount,
    icon: <AreaChartOutlined width={14} />,
    options: {
      label: 'Payouts'
    }
  },
  {
    name: "payouts",
    list: ListPayout,
    create: CreatePayout,
    edit: EditPayout,
    icon: <AreaChartOutlined width={14} />
  },
  {
    name: "payments",
    list: ListPayment,
    icon: <PayCircleFilled width={14} />
  },
  {
    name: "payment-transaction",
    list: ListPayable,
    icon: <AlipaySquareFilled width={14} />,
    options: {
      label: 'Payable'
    }
  },
  {
    name: "payments-by-leader",
    list: ListLeaderPayment,
    icon: <PayCircleFilled width={14} />,
    options: {
      label: 'Payment'
    }
  },
  {
    name: "payout-confirms",
    options: {
      label: "Payout Pending"
    },
    list: ListPayoutPending,
    show: ShowPayoutPendingDetail,
    icon: <FieldTimeOutlined width={14} />
  }
]

export default routers