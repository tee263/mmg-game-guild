export const scholarSchema = {
  'User Name': {
    prop: 'username',
    type: String,
    required: true
  },
  'Email': {
    prop: 'email',
    type: String,
    required: true
  },
  'Identify ID': {
    prop: 'identifyId',
    type: String,
    required: true
  },
  'Personal Wallet': {
    prop: 'personalWallet',
    type: String,
    required: true
  },
  'Phone Number': {
    prop: 'phoneNumber',
    type: String,
    required: true
  },
  'Password': {
    prop: 'password',
    type: String,
    required: true
  }
}

export const accountSchema = {
  'Account Email': {
    prop: 'email',
    type: String,
    required: true
  },
  'Password': {
    prop: 'password',
    type: String,
    required: true
  },
  'Account Wallet': {
    prop: 'wallet',
    type: String,
    required: true
  },
  'Build': {
    prop: 'build',
    type: Number,
    required: true
  }
}

export const payableCol = [
  {
    title: 'Account',
    dataIndex: ['account', 'email'],
    key: 'accountEmail',
  },
  {
    title: 'Scholar ID',
    dataIndex: 'scholarId',
    key: 'scholarId',
  },
  {
    title: 'Scholar Name',
    dataIndex: 'scholarName',
    key: 'scholarName',
  },
  // {
  //   title: 'Scholar Status',
  //   dataIndex: 'scholarStatus',
  //   key: 'scholarStatus',
  // },
  {
    title: 'Scholar Wallet',
    dataIndex: 'scholarWallet',
    key: 'scholarWallet',
  },
  {
    title: 'Need Payment',
    dataIndex: 'needPayment',
    key: 'needPayment',
  },
  {
    title: 'Paid',
    dataIndex: 'paid',
    key: 'paid',
  },
  {
    title: 'Remaining',
    dataIndex: 'remaining',
    key: 'remaining',
  }
]

export const accountExcelCol = [
  {
    title: 'Account Email',
    dataIndex: 'email',
    key: 'email',
  },
  {
    title: 'Account Wallet',
    dataIndex: 'wallet',
    key: 'wallet',
  },
  {
    title: 'Password',
    dataIndex: 'password',
    key: 'password',
  },
  {
    title: 'Build',
    dataIndex: 'build',
    key: 'build',
  }
]

export const excelCol = [
  {
    title: 'User Name',
    dataIndex: 'username',
    key: 'username',
  },
  {
    title: 'Email',
    dataIndex: 'email',
    key: 'email',
  },
  {
    title: 'Identify ID',
    dataIndex: 'identifyId',
    key: 'identifyId',
  },
  {
    title: 'Personal Wallet',
    dataIndex: 'personalWallet',
    key: 'personalWallet',
  },
  {
    title: 'Phone Number',
    dataIndex: 'phoneNumber',
    key: 'phoneNumber',
  },
  {
    title: 'Password',
    dataIndex: 'password',
    key: 'password',
  }
]

export const errorCol = [
  {
    title: 'Field Error',
    dataIndex: 'field',
  }, {
    title: 'Row Index',
    dataIndex: 'row',
  },{
    title: 'Value Error',
    dataIndex: 'value',
  }
]

export const schemaErrorCol = [
  {
    title: 'Column Name',
    dataIndex: 'column',
  }, {
    title: 'Error Type',
    dataIndex: 'error',
  },{
    title: 'Row Index',
    dataIndex: 'row',
  }
]

export enum STATUS_ENUM {
  ACTIVE = "active",
  DE_ACTIVE = "in-active",
  ARCHIVED = "archived"
}

export const API_URL = process.env.REACT_APP_API_URL
export const emailRegex = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i)