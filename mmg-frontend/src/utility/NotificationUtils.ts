import {notification} from "antd"

export function openNotification(message: any, desc: any, type: any) {
  switch (type) {
  case "error":
    notification.error({
      message: message,
      description: desc,
      onClick: () => {
        console.log('Notification Clicked!')
      }
    })
    break
  case "success":
    notification.success({
      message: message,
      description: desc,
      onClick: () => {
        console.log('Notification Clicked!')
      }
    })
    break
  }


}