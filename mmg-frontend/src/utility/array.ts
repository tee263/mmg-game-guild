import {Key} from "react"

export function getUniqueIds(arr:Key[]) {
  return arr.filter((item, i, ar) => ar.indexOf(item) === i)
}