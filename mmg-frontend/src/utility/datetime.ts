import format from 'date-fns/format'
import addDays from 'date-fns/addDays'

export function getDate(datetime:string|number|Date = 1) {
  return new Date(datetime)
    .toISOString()
    .split('T')[0]
}

export function getDateNow() {
  return new Date()
}

export function getYYYYMMDDFrom(date: number | Date) {
  return format(date, 'yyyy-MM-dd')
}

export function getDDMMYYYYFrom(date: number | Date) {
  return format(date, 'dd-MM-yyyy')
}

export function getListOfDateInRange(start: string | number | Date, end: string | number | Date) {
  let point = new Date(start)
  const results = []
  while (point <= new Date(end)) {
    results.push(getYYYYMMDDFrom(point))
    point = addDays(point, 1)
  }
  return results
}

