
export const addressShortener = (address:string | undefined) => {
  if (!address || address.length === 0) return ''
  return address.slice(0, 5) + '...' + address.slice(address.length - 5, address.length)
}
